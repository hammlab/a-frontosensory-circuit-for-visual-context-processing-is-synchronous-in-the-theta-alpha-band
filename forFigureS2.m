clcl
clear all
rednumber=0; %redundant to analyse. if 0, use all.
t1=250; t2=799; t3=800; t4=1350;%times of analysis; 
for fileload=1;
  
    files1={};%
    files2={};%;
     

    two_obvs_per_mouse=1;

end
addpath /Users/jhamm1/Documents/dataanalysis_new/fieldtrip-20230428
ft_defaults
load dataTEMPLATE_for_fieldtrip 


%control
for fila=1:size(files1,1);
    data=dataTEMPLATE_for_fieldtrip; 
    load(files1{fila});
    
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0); ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
    trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%
  %  erp1=erp1(trials1>2,:);erp2=erp2(trials1>2,:);
     erp1=erp1(or(or(trials1==1,trials1==2),or(trials1==3,trials1==4)),:);erp2=erp2(or(or(trials1==1,trials1==2),or(trials1==3,trials1==4)),:);    
    clear tmp times
    for tr=1:size(erp1,1);
        tmp{1,tr}=vertcat(erp1(tr,t1:t4),erp2(tr,t1:t4));        
        times{1,tr}=(0:(t4-t1))/1000;        
    end
    data.trial=tmp; data.time=times; data.fsample=size(times{1,tr},2); 
    clear aa; aa{1,1}='v1';aa{2,1}='ACa';
    data.label=aa;


    cfg         = [];
     cfg.order   = 10; %can vary this
    cfg.toolbox = 'bsmart';
    mdata       = ft_mvaranalysis(cfg, data);
    %%The resulting variable mdata contains a description of the data in terms
    % of a multivariate autoregressive model. For each time-lag up to the model
    % order (cfg.order), a 3x3 matrix of coefficients is outputted.
    % The noisecov-field contains covariance matrix of the model’s residuals.
%     dd=mdata.coeffs;
%     for ep=1:3;
%         dd(:,:,ep)=(dd(:,:,ep)-dd(:,:,ep));
%     end
%     mdata.coeffs=dd; 


    cfg        = [];
    cfg.method = 'mvar';
    mfreq      = ft_freqanalysis(cfg, mdata);
    %The resulting mfreq data structure contains the pairwise transfer
    % function between the 3 channels for 101 frequencies.

%     cfg           = [];
%     cfg.method    = 'mtmfft';
%     cfg.taper     = 'dpss';
%     cfg.output    = 'fourier';
%     %cfg.foilim = [1 100];
%     cfg.tapsmofrq = 2; %%smoothing
%     freq          = ft_freqanalysis(cfg, data);
    %%The resulting freq structure contains the spectral estimate for 3 tapers
    %%in each of the 500 trials (hence 1500 estimates), for each of the 3
    % channels and for 101 frequencies.
% 
%      cfg           = [];
%      cfg.method    = 'superlet';
%      cfg.foi = freq1:freq2
%      cfg.toi = 0:.004:1.000
%      cfg.width = 1
%      cfg.gwidth = 3
%      cfg.taper     = 'hanning';
%      cfg.output    = 'fourier';
%      cfg.tapsmofrq = 2; %%smoothing
%      freq          = ft_freqanalysis(cfg, data);



    cfg           = [];
    cfg.method    = 'granger';
    granger       = ft_connectivityanalysis(cfg, mfreq);%(cfg, freq);%%

%      cfg           = [];
%      cfg.parameter = 'grangerspctrm';
%      cfg.zlim      = [0 1];
%     ft_connectivityplot(cfg, granger);

    grangerspec_p2v_control(fila,:)=squeeze(granger.grangerspctrm(2,1,:));
    GCs_p2v_control(:,fila)=granger.grangerspctrm(2,1,10);%rch;

    grangerspec_v2p_control(fila,:)=squeeze(granger.grangerspctrm(1,2,:));
    GCs_v2p_control(:,fila)=granger.grangerspctrm(1,2,10);%rch;
    disp(fila)
end



%figure; plot(mean(grangerspec_p2v_control(:,freq1:frneq2),1),'b'); hold on
%plot(mean(grangerspec_v2p_control(:,freq1:freq2),1),'k');

%figure; plot(mean(grangerspec_p2v_control(:,freq1:freq2),1)-mean(grangerspec_v2p_control(:,freq1:freq2),1));

%rdnt
for fila=1:size(files1,1);
    data=dataTEMPLATE_for_fieldtrip; 
    load(files2{fila});
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:); 
    trials2=trials1;

    trials_numrdnt=trials1; cnt=0;
    for t=1:size(trials1,2);
        if trials1(t)==1;
            trials_numrdnt(1,t)=trials1(t)+cnt;
            cnt=1+cnt;
        else
            cnt=0;
            trials_numrdnt(1,t)=99;
        end
    end
    for t=1:size(trials_numrdnt,2);
        if trials_numrdnt(1,t)>98;
            trials_numrdnt(1,t)=99;
        elseif trials_numrdnt(1,t)==1;
             trials_numrdnt(1,t)=99;
        end
    end
   % trials_numrdnt(1:40)=99;
    if rednumber>0;
        erp1=erp1(and(trials_numrdnt>rednumber,trials_numrdnt<99),:);erp2=erp2(and(trials_numrdnt>rednumber,trials_numrdnt<99),:);
    else
        erp1=erp1(trials_numrdnt<99,:);erp2=erp2(trials_numrdnt<99,:);
    end
    
    clear tmp times
    for tr=1:size(erp1,1);
        tmp{1,tr}=vertcat(erp1(tr,t1:t4),erp2(tr,t1:t4));        
        times{1,tr}=(0:(t4-t1))/1000;        
    end
    data.trial=tmp; data.time=times; data.fsample=size(times{1,tr},2); 
    clear aa; aa{1,1}='v1';aa{2,1}='ACa';
    data.label=aa;


    cfg         = [];
     cfg.order   = 10; %can vary this
    cfg.toolbox = 'bsmart';
    mdata       = ft_mvaranalysis(cfg, data);
    %%The resulting variable mdata contains a description of the data in terms
    % of a multivariate autoregressive model. For each time-lag up to the model
    % order (cfg.order), a 3x3 matrix of coefficients is outputted.
    % The noisecov-field contains covariance matrix of the model’s residuals.
%     dd=mdata.coeffs;
%     for ep=1:3;
%         dd(:,:,ep)=(dd(:,:,ep)-dd(:,:,ep));
%     end
%     mdata.coeffs=dd; 

    cfg        = [];
    cfg.method = 'mvar';
    mfreq      = ft_freqanalysis(cfg, mdata);
    %The resulting mfreq data structure contains the pairwise transfer
    % function between the 2 channels for 101 frequencies.

%     cfg           = [];
%     cfg.method    = 'mtmfft';
%     cfg.taper     = 'dpss';
%     cfg.output    = 'fourier';
%     %cfg.foilim = [1 100];
%     cfg.tapsmofrq = 2; %%smoothing
%     freq          = ft_freqanalysis(cfg, data);
    %%The resulting freq structure contains the spectral estimate for 3 tapers
    %%in each of the 500 trials (hence 1500 estimates), for each of the 3
    % channels and for 101 frequencies.
% 
%      cfg           = [];
%      cfg.method    = 'superlet';
%      cfg.foi = freq1:freq2
%      cfg.toi = 0:.004:1.000
%      cfg.width = 1
%      cfg.gwidth = 3
%      cfg.taper     = 'hanning';
%      cfg.output    = 'fourier';
%      cfg.tapsmofrq = 2; %%smoothing
%      freq          = ft_freqanalysis(cfg, data);



    cfg           = [];
    cfg.method    = 'granger';
    granger       = ft_connectivityanalysis(cfg, mfreq);%(cfg, freq);%

     cfg           = [];
     cfg.parameter = 'grangerspctrm';
     cfg.zlim      = [0 1];
    ft_connectivityplot(cfg, granger);

  grangerspec_p2v_rdnt(fila,:)=squeeze(granger.grangerspctrm(2,1,:));
    GCs_p2v_rdnt(:,fila)=granger.grangerspctrm(2,1,10);%rch;

    grangerspec_v2p_rdnt(fila,:)=squeeze(granger.grangerspctrm(1,2,:));
    GCs_v2p_rdnt(:,fila)=granger.grangerspctrm(1,2,10);%rch;
    disp(fila)
end


%figure; plot(mean(grangerspec_p2v_rdnt(:,freq1:freq2),1),'b'); hold on
%plot(mean(grangerspec_v2p_rdnt(:,freq1:freq2),1),'k');

%figure; plot(mean(grangerspec_p2v_rdnt(:,freq1:freq2),1)-mean(grangerspec_v2p_rdnt(:,freq1:freq2),1));

%dev
for fila=1:size(files1,1);
    data=dataTEMPLATE_for_fieldtrip; 
    load(files2{fila});
    
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0); ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
    trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%
     cnt=0;   
    for t=1:size(trials1,2);
        if and(trials1(t)==2,cnt<10);
            cnt=1+cnt;
        elseif and(trials1(t)==2,cnt==10);
            trials1(t)=99;
        end
    end

    erp1=erp1(trials1==2,:);erp2=erp2(trials1==2,:);
    clear tmp times
    for tr=1:size(erp1,1);
        tmp{1,tr}=vertcat(erp1(tr,t1:t4),erp2(tr,t1:t4));        
        times{1,tr}=(0:(t4-t1))/1000;        
    end
    data.trial=tmp; data.time=times; data.fsample=size(times{1,tr},2); 
    clear aa; aa{1,1}='v1';aa{2,1}='ACa';
    data.label=aa;


    cfg         = [];
     cfg.order   = 10; %can vary this
    cfg.toolbox = 'bsmart';
    mdata       = ft_mvaranalysis(cfg, data);
    %%The resulting variable mdata contains a description of the data in terms
    % of a multivariate autoregressive model. For each time-lag up to the model
    % order (cfg.order), a 3x3 matrix of coefficients is outputted.
    % The noisecov-field contains covariance matrix of the model’s residuals.
%     dd=mdata.coeffs;
%     for ep=1:3;
%         dd(:,:,ep)=(dd(:,:,ep)-dd(:,:,ep));
%     end
%     mdata.coeffs=dd; 

    cfg        = [];
    cfg.method = 'mvar';
    mfreq      = ft_freqanalysis(cfg, mdata);
    %The resulting mfreq data structure contains the pairwise transfer
    % function between the 3 channels for 101 frequencies.

%     cfg           = [];
%     cfg.method    = 'mtmfft';
%     cfg.taper     = 'dpss';
%     cfg.output    = 'fourier';
%     %cfg.foilim = [1 100];
%     cfg.tapsmofrq = 2; %%smoothing
%     freq          = ft_freqanalysis(cfg, data);
    %%The resulting freq structure contains the spectral estimate for 3 tapers
    %%in each of the 500 trials (hence 1500 estimates), for each of the 3
    % channels and for 101 frequencies.
% 
%      cfg           = [];
%      cfg.method    = 'superlet';
%      cfg.foi = freq1:freq2
%      cfg.toi = 0:.004:1.000
%      cfg.width = 1
%      cfg.gwidth = 3
%      cfg.taper     = 'hanning';
%      cfg.output    = 'fourier';
%      cfg.tapsmofrq = 2; %%smoothing
%      freq          = ft_freqanalysis(cfg, data);



    cfg           = [];
    cfg.method    = 'granger';
    granger       = ft_connectivityanalysis(cfg, mfreq);%(cfg, freq);%

%      cfg           = [];
%      cfg.parameter = 'grangerspctrm';
%      cfg.zlim      = [0 1];
%     ft_connectivityplot(cfg, granger);

    grangerspec_p2v_dev(fila,:)=squeeze(granger.grangerspctrm(2,1,:));
    GCs_p2v_dev(:,fila)=granger.grangerspctrm(2,1,10);%rch;

    grangerspec_v2p_dev(fila,:)=squeeze(granger.grangerspctrm(1,2,:));
    GCs_v2p_dev(:,fila)=granger.grangerspctrm(1,2,10);%rch;
    disp(fila) 
end


%figure; plot(mean(grangerspec_p2v_dev(:,freq1:freq2),1),'b'); hold on
%plot(mean(grangerspec_v2p_dev(:,freq1:freq2),1),'k');

grangerspec_p2v(:,:,1)=grangerspec_p2v_control;
grangerspec_v2p(:,:,1)=grangerspec_v2p_control;
grangerspec_p2v(:,:,2)=grangerspec_p2v_rdnt;
grangerspec_v2p(:,:,2)=grangerspec_v2p_rdnt;
grangerspec_p2v(:,:,3)=grangerspec_p2v_dev;
grangerspec_v2p(:,:,3)=grangerspec_v2p_dev;
freq1=1; freq2=110;
    figure; clear avs
for bin=1:3;
    subplot(3,2,((bin-1)*2)+1);shadedErrorBar(freq1:freq2,mean(grangerspec_p2v(:,freq1:freq2,bin),1),std(grangerspec_p2v(:,freq1:freq2,bin)-grangerspec_v2p(:,freq1:freq2,bin),1,1)./sqrt(13),'lineProps','b');
    shadedErrorBar(freq1:freq2,mean(grangerspec_v2p(:,freq1:freq2,bin),1),std(grangerspec_p2v(:,freq1:freq2,bin)-grangerspec_v2p(:,freq1:freq2,bin),1,1)./sqrt(13),'lineProps','k');

    subplot(3,2,((bin-1)*2)+2); shadedErrorBar(freq1:freq2,mean(grangerspec_p2v(:,freq1:freq2,bin)-grangerspec_v2p(:,freq1:freq2,bin),1),std(grangerspec_p2v(:,freq1:freq2,bin)-grangerspec_v2p(:,freq1:freq2,bin),0,1)./sqrt(13),'lineProps','b');
    for suba=1:fila;    
        [val freqa]=max(mean(grangerspec_p2v(suba,3:15,:),3)-mean(grangerspec_v2p(suba,3:15,:),3));
        avs(suba,1,bin)=mean(grangerspec_p2v(suba,freqa+2,bin),2);
        avs(suba,2,bin)=mean(grangerspec_v2p(suba,freqa+2,bin),2);
        disp(freqa+2)
    end
end



tmpa1=[];   
for b=1:size(avs,3);
    for d=1:size(avs,2);
         for j=1:(fila/2);
             tmpa1(j,d,b)=mean(avs((((j-1)*2)+1):(j*2),d,b));
         end
    end
end
avs=tmpa1;
tmp1=vertcat(avs(:,1,1),avs(:,2,1),avs(:,1,2),avs(:,2,2),avs(:,1,3),avs(:,2,3),avs(:,1,1),avs(:,2,1));
tmp2=vertcat((avs(:,1,1)*0)+1.05,(avs(:,1,1)*0)+1.95,(avs(:,1,1)*0)+3.05,(avs(:,1,1)*0)+3.95,(avs(:,1,1)*0)+5.05,(avs(:,1,1)*0)+5.95,(avs(:,1,1)*0)+7.05,(avs(:,1,1)*0)+7.95);
tx1=(avs(:,1,1)+avs(:,2,1))/2;tx2=(avs(:,1,2)+avs(:,2,2))/2;tx3=(avs(:,1,3)+avs(:,2,3))/2;tx4=(avs(:,1,1)+avs(:,2,1))/2;
figure; errorbar_groups([mean(avs(:,1,1)) mean(avs(:,2,1)) ;mean(avs(:,1,2)) mean(avs(:,2,2)) ; mean(avs(:,1,3)) mean(avs(:,2,3)) ; mean(avs(:,1,1)) mean(avs(:,2,1))]',[std(avs(:,1,1)-tx1)./sqrt(fila) std(avs(:,2,1)-tx1)./sqrt(fila); std(avs(:,1,2)-tx2)./sqrt(fila) std(avs(:,2,2)-tx2)./sqrt(fila); std(avs(:,1,3)-tx3)./sqrt(fila) std(avs(:,2,3)-tx3)./sqrt(fila); std(avs(:,1,1)-tx4)./sqrt(fila) std(avs(:,1,2)-tx4)./sqrt(fila)]', 'bar_colors',[ 0 0 1; .5 .5 1],'bar_names',{'control';'rdnt';'dev';'ignore';}); title('grangers'); hold on
scatter(tmp2,tmp1);
for c=1:4
    
    a= tmp2( ((c-1)*((size(tmp1,1)./4)))+1  :(c*(size(tmp1,1)./4)));
    b= tmp1( ((c-1)*((size(tmp1,1)./4)))+1 :(c*(size(tmp1,1)./4)));
    subs=(size(a,1)./2);
    for s=1:subs;
        line(horzcat(a(s),a(s+subs)),horzcat(b(s),b(s+subs)));
    end

end


[h,p,ci,stats]=ttest(avs(:,1,1),avs(:,2,1)); stats
p
[h,p,ci,stats]=ttest(avs(:,1,2),avs(:,2,2)); stats
p
[h,p,ci,stats]=ttest(avs(:,1,3),avs(:,2,3)); stats
p
%NOTE dev, rdnt, and control are not real here. they are just variable
%names that carried over from past scripts, bc i'm lazy!

anova_rm({[avs(:,1,1) avs(:,2,1)] [avs(:,1,2) avs(:,2,2)] [avs(:,1,3) avs(:,2,3)]});%


clear all
files={''};
alldata=[]; alldataSTD=[]; cells=0; framerate=28; 
stdz=1; 
baseper=round(framerate*1.2):((framerate*2)-3); 
%actper2=((framerate*4)+5):(framerate*7);%for calc STD
actper2=1:(framerate*2);%for calc STD
ends=[91 91 76 84 85 86 86]; for stim=1:7; actper{stim}=ends(stim):((framerate*4)+1); end
for file=1:size(files,1);
    try
    load(strcat('CA_workspace_',files{file,1}));
    catch
    load(strcat('CA_workspace_optorhythm',files{file,1}));
    end
    vecdat=vectrials{1,1}; vecstim=vectrials{1,2}; clear dat1
    stimvis1=horzcat(0,0,stimvis); stimvis2=horzcat(0,stimvis,0); stimvis3=horzcat(stimvis,0,0); stimvis4=stimvis1+stimvis2+stimvis3; stimvis=stimvis4(2:(end-1));
    for cc=1:size(data,1);
        dd=data(cc,stimvis==0);
        vecdat(cc,:,:)=vecdat(cc,:,:)./prctile(dd,20);
      
    end
    for cnd=1:7;
        aaa=mean(vecdat(:,:,vecstim==cnd),3);
        bbb=mean(vecdat(:,:,vecstim==cnd),3);
        if stdz==0;
            alldata(cells+1:cells+size(vecdat,1),:,cnd)=aaa;
            alldataSTD(cells+1:cells+size(vecdat,1),:,cnd)=bbb;
        else
            for c=1:size(aaa,1);
                ss=std(aaa(c,baseper)); 
                mm=mean(aaa(c,baseper));
                aaa(c,:)=(aaa(c,:)-mm);
                bbb(c,:)=(bbb(c,:)-mm)./ss;
            end
            alldata(cells+1:cells+size(vecdat,1),:,cnd)=aaa;
            alldataSTD(cells+1:cells+size(vecdat,1),:,cnd)=bbb;
        end
                
    end
    cells=cells+size(vecdat,1);
    
end
titles={'weak';'full';'2 hz';'6 hz';'10 hz';'20 hz';'40 hz'};

for trimcells=0; %only use cells with some steady response to stiml
    if trimcells==1;
        cut=1.67;
        alltmp=[]; alltmp2=[]; c1=0;
        for c=1:size(alldataSTD,1);
            if sum(abs(mean(alldataSTD(c,max(ends):(framerate*4+1),2:7),2))>cut)>0;
                c1=1+c1;
                alltmp(c1,:,:)=alldata(c,:,:);
                alltmp2(c1,:,:)=alldataSTD(c,:,:);
            end
        end
        alldata=alltmp;
        alldataSTD=alltmp2;
    end
end
  
ttrim=[1:54 87:281]
for manyplotthing=1;
    figure;
    for cnd=1:7;
        y1=(alldata(:,ttrim,cnd));
        %y1=abs(alldata(:,ttrim,cnd));% for ooptocontrool plot supplementoL


        err=std(y1,0,1)./sqrt(size(y1,1));
        y1=mean(y1,1);y1=y1-mean(y1(1,baseper));
        subplot(1,7,cnd); shadedErrorBar(timeaxis(1,1:size(ttrim,2)),y1,err,'lineProps',col1); set(gcf,'Color','w');
        if cnd==1;
            ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            title(titles{cnd}, 'FontSize', 18, 'FontWeight', 'bold');
        else
            title(titles{cnd}, 'FontSize', 18, 'FontWeight', 'bold'); yticks([]);set(gca,'FontSize',16,'FontWeight','bold');
        end
        xlim([1 4000]);
    end
    for fixaxes=1
        
        ymin1=1; ymax1=-1;
        for cnd=2:7
            subplot(1,7,cnd); yy=ylim;
            if yy(1)<ymin1; ymin1=yy(1); end
            if yy(2)>ymax1; ymax1=yy(2); end
            
        end
        for cnd=1:7
            subplot(1,7,cnd); ylim([ymin1 ymax1]); xlim([-1000 4000]);ylim([-.005 .101])
        end
        make_eps_saveable
    end
end

for rastera=1;
    
    BOOSTscalefactor=55; %between 0 and 100. higher is brighter
    figure;
     cnd=3:7;
        y1=mean(alldata(:,:,cnd),3);
        
        [b,ix]=sort(mean(y1(:,actper{cnd(1)}),2));
    
    for cnd=1:7;
        y1=alldata(:,ttrim,cnd);
        for c=1:size(y1,1);
           % y1(c,:)=(y1(c,:)./std(y1(c,(framerate*3):(framerate*5))))-mean(y1(c,framerate:((framerate*2)-5)));
%             y1(c,:)=y1(c,:)-min(y1(c,framerate:((framerate*2)-4)));
        end
        subplot(1,7,cnd); imagesc(timeaxis(1,1:size(ttrim,2)),1:size(y1,1),y1(ix,:));set(gca,'FontSize',14,'FontWeight','bold'); %colormap('Gray'); 
        xlim([1 3000]);
        title(titles{cnd});
        camin(cnd,1)=min(min(y1(:,100:150)))-1; camax(cnd,1)=max(max(y1(:,100:150)));
        if cnd==1; xlabel('ms','FontSize',14,'FontWeight','bold');
            ylabel('neurons','FontSize',14,'FontWeight','bold');
        end
    end
    for settingcolorscale=1;
        scalefactor=(BOOSTscalefactor/100); if scalefactor==1; scalefactor=.99; end
        tmpmax=0; tmpmin=0;
        for z1=1:7
            subplot(1,7,z1); caxis([camin(cnd) camax(cnd)])
            caxis([-.1 .1])
            cb=caxis; if cb(1)<tmpmin; tmpmin=cb(1); end
            if cb(2)>tmpmax; tmpmax=cb(2); end
        end
        for z1=1:7;
            subplot(1,7,z1);
            %cb=[tmpmin tmpmax];
            %caxis(cb);
            %caxis(cb*(1-scalefactor))
            xlim([-1000 4000]); 
        end
    end
        make_eps_saveable
    
end

for PCAkmeans=1;
    numberclusters=3;
    clear dattmp  dat
    
    for cnd=1:7;
        y1=alldataSTD(:,:,cnd);
        for c=1:size(y1,1);
            dattmp(c,cnd)=mean(y1(c,actper{cnd}));
        end
    end
    dat=dattmp;
    for c=1:size(dattmp,1);
        dat(c,:)=dattmp(c,:)./std(y1(c,:));
    end
    numcomps=6;
    [coeff,score,latent,tsquared,explained,mu] = pca(dat);W=coeff(:,1:numcomps)'; %variables as columns
    coeff=rotatefactors(coeff(:,1:numcomps));%'Method','Promax');
    W=coeff';
   % figure; plot(explained); title('scree');
    
    %figure; plot(W'); title('factor weights');
    score=zeros(numcomps,size(dat,1));
    for adjustweight=0; %make maxes
        if adjustweight==1;
            W1=W-W; 
            for ww=1:numcomps;
                for cndd=1:size(W,2);
                    if W(ww,cndd)==max(W(ww,:)); W1(ww,cndd)=1; end
                end
            end
            W=W1;
        end
    end
    for t=1:size(dat,1);
        for c=1:numcomps;
           score(c,t)=sum(W(c,:).*dat(t,:))./sum((W(c,:)));
        end
    end
    [IDX,Ca,sumd,D] = kmeans(dat(:,[5 7]),numberclusters);
    clear cols
    colas={[0 0 0] [1 0 0] [0 0 1] [.5 .5 .5] [1 0 1]}; for c=1:size(IDX,1); cols(c,:)=colas{IDX(c)}; end;
   % figure; scatter(score(1,:),score(2,:),50,cols,'fill');
      %figure; scatter3(score(1,:),score(2,:),score(3,:),50,cols,'fill');   
    figure; scatter(mean(dat(:,5),2),dat(:,7),50,cols,'fill'); xlabel('theta-alpha'); ylabel('gamma'); 
   
    for rastera=1;
        
        BOOSTscalefactor=50; %between 0 and 100. higher is brighter
        figure;
        y1=score(1,:)-score(2,:);
        y1=IDX;
        [b,ix]=sort(y1);
        
        for cnd=1:7;
            y1=alldataSTD(:,:,cnd);
            for c=1:size(y1,1);
                y1(c,:)=(y1(c,:)./std(y1(c,1:framerate)))-mean(y1(c,framerate:((framerate*2)-5)));
                y1(c,:)=y1(c,:)-min(y1(c,framerate:((framerate*2)-4)));
            end
            subplot(1,7,cnd); imagesc(timeaxis,1:size(y1,1),y1(ix,:)); colormap('Gray'); set(gca,'FontSize',14,'FontWeight','bold');
            xlim([1500 5000]);
            title(titles{cnd});
            camin(cnd,1)=min(min(y1(:,100:150)))-1; camax(cnd,1)=max(max(y1(:,100:150)));
            if cnd==1; xlabel('sec','FontSize',14,'FontWeight','bold');
                ylabel('neurons','FontSize',14,'FontWeight','bold');
            end
        end
        for settingcolorscale=1;
            scalefactor=(BOOSTscalefactor/100); if scalefactor==1; scalefactor=.99; end
            tmpmax=0; tmpmin=0;
            for z1=1:7
                subplot(1,7,z1); caxis([camin(cnd) camax(cnd)])
                cb=caxis; if cb(1)<tmpmin; tmpmin=cb(1); end
                if cb(2)>tmpmax; tmpmax=cb(2); end
            end
            for z1=1:7;
                subplot(1,7,z1);
                cb=[tmpmin tmpmax];
                caxis(cb*(1-scalefactor))
                xlim([-500 5000]);
            end
        end
        
        
        
        
    end
    
    for clus=1:max(IDX)
    figure;
    for cnd=1:7;
        y1=(alldata(IDX==clus,:,cnd));

        err=std(y1,0,1)./sqrt(size(y1,1));
        y1=mean(y1,1);y1=y1-mean(y1(1,baseper));
        subplot(1,7,cnd); shadedErrorBar(timeaxis,y1,err,'lineProps','k'); set(gcf,'Color','w');
        if cnd==1;
            ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            title(titles{cnd}, 'FontSize', 18, 'FontWeight', 'bold');
        else
            title(titles{cnd}, 'FontSize', 18, 'FontWeight', 'bold'); yticks([]);set(gca,'FontSize',16,'FontWeight','bold');
        end
        xlim([1100 5000]);
    end
    for fixaxes=1
        
        ymin1=1; ymax1=-1;
        for cnd=1:7
            subplot(1,7,cnd); yy=ylim;
            if yy(1)<ymin1; ymin1=yy(1); end
            if yy(2)>ymax1; ymax1=yy(2); end
            
        end
        for cnd=1:7
            subplot(1,7,cnd); ylim([ymin1 ymax1]); xlim([-500 5000]);
        end
        
    end
    disp(strcat('cluster:',num2str(clus),'. n=',num2str(sum(IDX==clus)),'--',num2str(round(mean(IDX==clus)*1000)/10),'%'))
    end
    cut1=1.67; 
     figure; hist(mean(alldataSTD(:,actper{4},4),2),round(size(alldataSTD,1)/3)); title(strcat('6 Hz response. deactivated:',num2str(round(mean(mean(alldataSTD(:,actper{4},4),2)<-cut1)*1000)/10),'. activated:',num2str(round(mean(mean(alldataSTD(:,actper{4},4),2)>cut1)*1000)/10)));
           
    figure; hist(mean(alldataSTD(:,actper{5},5),2),round(size(alldataSTD,1)/3)); title(strcat('10 Hz response. deactivated:',num2str(round(mean(mean(alldataSTD(:,actper{5},5),2)<-cut1)*1000)/10),'. activated:',num2str(round(mean(mean(alldataSTD(:,actper{5},5),2)>cut1)*1000)/10)));
            
    figure; hist(mean(alldataSTD(:,actper{7},7),2),round(size(alldataSTD,1)/3)); title(strcat('40 Hz response. deactivated:',num2str(round(mean(mean(alldataSTD(:,actper{7},7),2)<-cut1)*1000)/10),'. activated:',num2str(round(mean(mean(alldataSTD(:,actper{7},7),2)>cut1)*1000)/10)));

end

optostims=dattmp;
optostimsSTD=dat;
save(strcat(files{1},'optoresults'),'optostims','optostimsSTD','alldata');



for regular_Scatta=1;
    clear dattmp  dat datFORstd
    
    for cnd=1:7;
        y1=alldata(:,:,cnd);
        for c=1:size(y1,1);
            dattmp(c,cnd)=mean(y1(c,actper{cnd}));            
        end
    end
    for c=1:size(alldata,1);
        dd=[];
        for cnd=1:7;
            dd=horzcat(dd,(y1(c,[actper2])));
        end
        datFORstd(c,:)=dd;
    end
    dat=dattmp;
    for c=1:size(dattmp,1);
        dat(c,:)=dattmp(c,:);%./std(datFORstd(c,:));
    end
    figure; scatter(mean(dat(:,4:5),2),dat(:,7),50,col1,'fill'); 
    ylabel('gamma-stim');
    xlabel('theta/alpha-stim');
    hold on;
    plot(min(dat(:)):max(dat(:)),min(dat(:)):max(dat(:)));
    [p, table] = anova_rm({dat});
    [p, table] = anova_rm({datpyr datsst datvip});
end


 for z1=1:7;
            subplot(1,7,z1);
            cb=[-2.5 7];
            caxis(cb)
            xlim([-500 5000]); 
        end







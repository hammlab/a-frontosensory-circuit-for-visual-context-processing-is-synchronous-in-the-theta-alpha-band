clcl
rednumber=0; %redundant to analyse. if 0, use all.
lag=10; %lag of phase
computelagfromtheta=0; %compute the lag based on peak freq
freq1a=3; freq2a=15; %%theta bounds;freq1a=3; freq2a=15;% %beta bounds;  
calcpeakfreq=1; %if computing peak frequency for each mouse separately
freq3=40; freq4=60; %gamma bounds (actual, divided by 2. so 25 is really 50). 40 and 60
t1=400; t2=799; t3=800; t4=1100;%
for fileload=1;
    files1={};%
    files2={};%;
   
    two_obvs_per_mouse=1;

end

%find peak
for calcpeakfreq=calcpeakfreq;
    if calcpeakfreq==1;
        for fila=1:size(files1,1);
            load(files1{fila},'tfs1','tfs2','basetime1','time2');
            clear coher
            for calculatecoherencebetweens=1;

                for fa=freq1a:freq2a;
                    lags=0; cnt=0;
                    for t=1:100;
                        for tr=1:size(tfs1,3);
                            l1=tfs1(fa,t,tr)./abs(tfs1(fa,t,tr));
                            l2=tfs2(fa,t,tr)./abs(tfs2(fa,t,tr));
                            lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                            cnt=cnt+1;
                        end
                    end
                    coher(fa,1)=abs(lags/cnt);
                end
            end
            tmp=mean(coher,2);
            fr=max(tmp);
            for fa=freq1a:freq2a;
                if coher(fa,1)==fr;
                    peakf(fila,1)=fa;
                end
            end

%             if peakf(fila,1)==freq1a;
%                  peakf(fila,1)=9;
%              end
%              if peakf(fila,1)==freq2a;
%                  peakf(fila,1)=9;
%              end
            freq1(fila)=peakf(fila,1)-1; freq2(fila)=peakf(fila,1)+1;
        end
    else
        for fila=1:size(files1,1);
            freq1(fila)=freq1a; freq2(fila)=freq2a; peakf(fila)=round((freq1a+freq2a)/2);
        end
    end
end

%control
for fila=1:size(files1,1);
    load(files1{fila});
    if computelagfromtheta==1;
        lag=floor((1000/peakf(fila))/8);
    end

    %d2 = designfilt('lowpassiir',  'FilterOrder',8,'PassbandFrequency',40, 'PassbandRipple',.2,'StopbandAttenuation', 65,'SampleRate',1000);
    d4 = designfilt('bandpassiir','FilterOrder',2, 'HalfPowerFrequency1',freq1(fila),'HalfPowerFrequency2',freq2(fila), 'SampleRate',1000);
%d4=designfilt('bandpassfir','FilterOrder',4,'CutoffFrequency1',freq1(fila),'CutoffFrequency2',freq2(fila),'SampleRate',1000);
    %fvtool(d4)
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0); ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
    trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%
    erp1=erp1(or(or(trials1==1,trials1==2),or(trials1==3,trials1==4)),:);erp2=erp2(or(or(trials1==1,trials1==2),or(trials1==3,trials1==4)),:);

    %pfc phase to V1 amplitude
    X1=[];X2=[];X1a=[]; YYamp=[];
    for tr=1:size(erp1,1)
        tmp=erp1(tr,:);
        han=hanning(500);
        for t=251:1100;
            f=fft(han'.*tmp(t-250:t+249));
            a=abs(f);
            data1(t)=mean(a(freq3:freq4)); %these are the gamma freqs
        end

        X1(:,tr)=data1([t1:t2 t3:t4]);
        X1a(:,tr)=data1([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp2(tr,:)))./abs(hilbert(filter(d4,erp2(tr,:))));
        X2(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp2(tr,:)))';
        YYamp(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
    end
    XX=X1(:); YY=X2(:); XXa=X1a(:); YYamp=YYamp(:);
    pr=prctile(XX,99);
    YY=YY(XX<pr);XX=XX(XX<pr);XXa=XXa(XX<pr); YYamp=YYamp(XX<pr);
    XX=XX./pr;XXa=XXa/pr;
    %YYtmp=abs(YYamp); pr1=prctile(YYtmp,15); pr2=prctile(YYtmp,85);  YY=YY(and(YYtmp>pr1,YYtmp<pr2)); XX=XX(and(YYtmp>pr1,YYtmp<pr2)); XXa=XXa(and(YYtmp>pr1,YYtmp<pr2));
    ZZ=YY.*XX; % phase amp coupling
    YYa=real(YY); YYb=imag(YY);
    model1=fitlm(XXa,XX);
    model2=fitlm(horzcat(XXa,YYa,YYb),XX);
    rch=model2.Rsquared.ordinary-model1.Rsquared.ordinary;
    F=(rch./2)/((1-rch)/size(YY,1));
    PAs_p2v_control(:,fila)=abs(mean(ZZ));
    GCs_p2v_control(:,fila)=F;%rch;


    %v1 phase to PFC amplitude
    X1=[];X2=[];X1a=[]; YYamp=[];
    for tr=1:size(erp1,1)
        tmp=erp2(tr,:);
        han=hanning(500);
        for t=251:1100;
            f=fft(han'.*tmp(t-250:t+249));
            a=abs(f);
            data1(t)=mean(a(freq3:freq4)); %these are the gamma freqs
        end

        X1(:,tr)=data1([t1:t2 t3:t4]);
        X1a(:,tr)=data1([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp1(tr,:)))./abs(hilbert(filter(d4,erp1(tr,:))));
        X2(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp1(tr,:)))';
        YYamp(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
    end
    XX=X1(:); YY=X2(:); XXa=X1a(:); YYamp=YYamp(:);
    pr=prctile(XX,99);
    YY=YY(XX<pr);XX=XX(XX<pr);XXa=XXa(XX<pr); YYamp=YYamp(XX<pr);
    XX=XX./pr;XXa=XXa/pr;
    %YYtmp=abs(YYamp); pr1=prctile(YYtmp,15); pr2=prctile(YYtmp,85);  YY=YY(and(YYtmp>pr1,YYtmp<pr2)); XX=XX(and(YYtmp>pr1,YYtmp<pr2)); XXa=XXa(and(YYtmp>pr1,YYtmp<pr2));
    ZZ=YY.*XX; % phase amp coupling
    YYa=real(YY); YYb=imag(YY);
    model1=fitlm(XXa,XX);
    model2=fitlm(horzcat(XXa,YYa,YYb),XX);
    rch=model2.Rsquared.ordinary-model1.Rsquared.ordinary;
    F=(rch./2)/((1-rch)/size(YY,1));
    PAs_v2p_control(:,fila)=abs(mean(ZZ));
    GCs_v2p_control(:,fila)=F;%rch;
    disp(fila)
end

for calcpeakfreq=calcpeakfreq;
    if calcpeakfreq==1;
        for fila=1:size(files2,1);
            load(files2{fila},'tfs1','tfs2','basetime1','time2');
            clear coher
            for calculatecoherencebetweens=1;

                for fa=freq1a:freq2a;
                    lags=0; cnt=0;
                    for t=1:100;
                        for tr=1:size(tfs1,3);
                            l1=tfs1(fa,t,tr)./abs(tfs1(fa,t,tr));
                            l2=tfs2(fa,t,tr)./abs(tfs2(fa,t,tr));
                            lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                            cnt=cnt+1;
                        end
                    end
                    coher(fa,1)=abs(lags/cnt);
                end
            end
            tmp=mean(coher,2);
            fr=max(tmp);
            for fa=freq1a:freq2a;
                if coher(fa,1)==fr;
                    peakf(fila,1)=fa;
                end
            end
%             if peakf(fila,1)==freq2a;
%                  peakf(fila,1)=9;
%             end
%             if peakf(fila,1)==freq1a;
%                  peakf(fila,1)=9;
%              end
            freq1(fila)=peakf(fila,1)-1; freq2(fila)=peakf(fila,1)+1;
        end
    else
        for fila=1:size(files1,1);
            freq1(fila)=freq1a; freq2(fila)=freq2a; peakf(fila)=round((freq1a+freq2a)/2);
        end
    end
end

%rdnt
for fila=1:size(files1,1);
    load(files2{fila});
    if computelagfromtheta==1;
        lag=floor((1000/peakf(fila))/8);
    end
  d4 = designfilt('bandpassiir','FilterOrder',2, 'HalfPowerFrequency1',freq1(fila),'HalfPowerFrequency2',freq2(fila), 'SampleRate',1000);

    %d2 = designfilt('lowpassiir',  'FilterOrder',8,'PassbandFrequency',40, 'PassbandRipple',.2,'StopbandAttenuation', 65,'SampleRate',1000);
    %fvtool(d2)
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0); ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
    trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%

    trials_numrdnt=trials1; cnt=0;
    for t=1:size(trials1,2);
        if trials1(t)==1;
            trials_numrdnt(1,t)=trials1(t)+cnt;
            cnt=1+cnt;
        else
            cnt=0;
            trials_numrdnt(1,t)=99;
        end
    end
    for t=1:size(trials_numrdnt,2);
        if trials_numrdnt(1,t)>98;
            trials_numrdnt(1,t)=99;
        elseif trials_numrdnt(1,t)==1;
            % trials_numrdnt(1,t)=99;
        end
    end
    if rednumber>0;
        erp1=erp1(and(trials_numrdnt>rednumber,trials_numrdnt<99),:);erp2=erp2(and(trials_numrdnt>rednumber,trials_numrdnt<99),:);
    else
        erp1=erp1(trials_numrdnt<99,:);erp2=erp2(trials_numrdnt<99,:);
    end

    %pfc phase to V1 amplitude
    X1=[];X2=[];X1a=[]; YYamp=[];
    for tr=1:size(erp1,1)
        tmp=erp1(tr,:);
        han=hanning(500);
        for t=251:1100;
            f=fft(han'.*tmp(t-250:t+249));
            a=abs(f);
            data1(t)=mean(a(freq3:freq4)); %these are the gamma freqs
        end

        X1(:,tr)=data1([t1:t2 t3:t4]);
        X1a(:,tr)=data1([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp2(tr,:)))./abs(hilbert(filter(d4,erp2(tr,:))));
        X2(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp2(tr,:)))';
        YYamp(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
    end
    XX=X1(:); YY=X2(:); XXa=X1a(:); YYamp=YYamp(:);
    pr=prctile(XX,99);
    YY=YY(XX<pr);XX=XX(XX<pr);XXa=XXa(XX<pr); YYamp=YYamp(XX<pr);
    XX=XX./pr;XXa=XXa/pr;
    %   YYtmp=abs(YYamp); pr1=prctile(YYtmp,15); pr2=prctile(YYtmp,85);  YY=YY(and(YYtmp>pr1,YYtmp<pr2)); XX=XX(and(YYtmp>pr1,YYtmp<pr2)); XXa=XXa(and(YYtmp>pr1,YYtmp<pr2));
    ZZ=YY.*XX; % phase amp coupling
    YYa=real(YY); YYb=imag(YY);
    model1=fitlm(XXa,XX);
    model2=fitlm(horzcat(XXa,YYa,YYb),XX);
    rch=model2.Rsquared.ordinary-model1.Rsquared.ordinary;
    F=(rch./2)/((1-rch)/size(YY,1));
    PAs_p2v_rdnt(:,fila)=abs(mean(ZZ));
    GCs_p2v_rdnt(:,fila)=F;%rch;


    %v1 phase to PFC amplitude
    X1=[];X2=[];X1a=[]; YYamp=[];
    for tr=1:size(erp2,1)
        tmp=erp2(tr,:);
        han=hanning(500);
        for t=251:1100;
            f=fft(han'.*tmp(t-250:t+249));
            a=abs(f);
            data1(t)=mean(a(freq3:freq4)); %these are the gamma freqs
        end

        X1(:,tr)=data1([t1:t2 t3:t4]);
        X1a(:,tr)=data1([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp1(tr,:)))./abs(hilbert(filter(d4,erp1(tr,:))));
        X2(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp1(tr,:)))';
        YYamp(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
    end
    XX=X1(:); YY=X2(:); XXa=X1a(:); YYamp=YYamp(:);
    pr=prctile(XX,99);
    YY=YY(XX<pr);XX=XX(XX<pr);XXa=XXa(XX<pr); YYamp=YYamp(XX<pr);
    XX=XX./pr;XXa=XXa/pr;
    % YYtmp=abs(YYamp); pr1=prctile(YYtmp,15); pr2=prctile(YYtmp,85);  YY=YY(and(YYtmp>pr1,YYtmp<pr2)); XX=XX(and(YYtmp>pr1,YYtmp<pr2)); XXa=XXa(and(YYtmp>pr1,YYtmp<pr2));
    ZZ=YY.*XX; % phase amp coupling
    YYa=real(YY); YYb=imag(YY);
    model1=fitlm(XXa,XX);
    model2=fitlm(horzcat(XXa,YYa,YYb),XX);
    rch=model2.Rsquared.ordinary-model1.Rsquared.ordinary;
    F=(rch./2)/((1-rch)/size(YY,1));
    PAs_v2p_rdnt(:,fila)=abs(mean(ZZ));
    GCs_v2p_rdnt(:,fila)=F;%rch;
    disp(fila)
end

%dev
for fila=1:size(files1,1);
    load(files2{fila});
    if computelagfromtheta==1;
        lag=floor((1000/peakf(fila))/8);
    end
    d4 = designfilt('bandpassiir','FilterOrder',2, 'HalfPowerFrequency1',freq1(fila),'HalfPowerFrequency2',freq2(fila), 'SampleRate',1000);

    %d2 = designfilt('lowpassiir',  'FilterOrder',8,'PassbandFrequency',40, 'PassbandRipple',.2,'StopbandAttenuation', 65,'SampleRate',1000);
    %fvtool(d2)
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0); ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
    trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%
    cnt=0;   
    for t=1:size(trials1,2);
        if and(trials1(t)==2,cnt<10);
            cnt=1+cnt;
        elseif and(trials1(t)==2,cnt==10);
            trials1(t)=99;
        end
    end


    erp1=erp1(trials1==2,:);erp2=erp2(trials1==2,:);


    %pfc phase to V1 amplitude
    X1=[];X2=[];X1a=[]; YYamp=[];
    for tr=1:size(erp1,1)
        tmp=erp1(tr,:);
        han=hanning(500);
        for t=251:1100;
            f=fft(han'.*tmp(t-250:t+249));
            a=abs(f);
            data1(t)=mean(a(freq3:freq4)); %these are the gamma freqs
        end

        X1(:,tr)=data1([t1:t2 t3:t4]);
        X1a(:,tr)=data1([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp2(tr,:)))./abs(hilbert(filter(d4,erp2(tr,:))));
        X2(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp2(tr,:)))';
        YYamp(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
    end
    XX=X1(:); YY=X2(:); XXa=X1a(:); YYamp=YYamp(:);
    pr=prctile(XX,99);
    YY=YY(XX<pr);XX=XX(XX<pr);XXa=XXa(XX<pr); YYamp=YYamp(XX<pr);
    XX=XX./pr;XXa=XXa/pr;
    %YYtmp=abs(YYamp); pr1=prctile(YYtmp,15); pr2=prctile(YYtmp,85);  YY=YY(and(YYtmp>pr1,YYtmp<pr2)); XX=XX(and(YYtmp>pr1,YYtmp<pr2)); XXa=XXa(and(YYtmp>pr1,YYtmp<pr2));
    ZZ=YY.*XX; % phase amp coupling
    YYa=real(YY); YYb=imag(YY);
    model1=fitlm(XXa,XX);
    model2=fitlm(horzcat(XXa,YYa,YYb),XX);
    rch=model2.Rsquared.ordinary-model1.Rsquared.ordinary;
    F=(rch./2)/((1-rch)/size(YY,1));
    PAs_p2v_dev(:,fila)=abs(mean(ZZ));
    GCs_p2v_dev(:,fila)=F;%rch;


    %v1 phase to PFC amplitude
    X1=[];X2=[];X1a=[]; YYamp=[];
    for tr=1:size(erp2,1)
        tmp=erp2(tr,:);
        han=hanning(500);
        for t=251:1100;
            f=fft(han'.*tmp(t-250:t+249));
            a=abs(f);
            data1(t)=mean(a(freq3:freq4)); %these are the gamma freqs
        end

        X1(:,tr)=data1([t1:t2 t3:t4]);
        X1a(:,tr)=data1([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp1(tr,:)))./abs(hilbert(filter(d4,erp1(tr,:))));
        X2(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp1(tr,:)))';
        YYamp(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
    end
    XX=X1(:); YY=X2(:); XXa=X1a(:); YYamp=YYamp(:);
    pr=prctile(XX,99);
    YY=YY(XX<pr);XX=XX(XX<pr);XXa=XXa(XX<pr); YYamp=YYamp(XX<pr);
    XX=XX./pr;XXa=XXa/pr;
    % YYtmp=abs(YYamp); pr1=prctile(YYtmp,15); pr2=prctile(YYtmp,85);  YY=YY(and(YYtmp>pr1,YYtmp<pr2)); XX=XX(and(YYtmp>pr1,YYtmp<pr2)); XXa=XXa(and(YYtmp>pr1,YYtmp<pr2));
    ZZ=YY.*XX; % phase amp coupling
    YYa=real(YY); YYb=imag(YY);
    model1=fitlm(XXa,XX);
    model2=fitlm(horzcat(XXa,YYa,YYb),XX);
    rch=model2.Rsquared.ordinary-model1.Rsquared.ordinary;
    F=(rch./2)/((1-rch)/size(YY,1));
    PAs_v2p_dev(:,fila)=abs(mean(ZZ));
    GCs_v2p_dev(:,fila)=F;%rch;
    disp(fila)
end

%rdnt 2
for fila=1:size(files2,1);
      load(files2{fila});
    if computelagfromtheta==1;
        lag=floor((1000/peakf(fila))/8);
    end
    d4 = designfilt('bandpassiir','FilterOrder',2, 'HalfPowerFrequency1',freq1(fila),'HalfPowerFrequency2',freq2(fila), 'SampleRate',1000);

    %d2 = designfilt('lowpassiir',  'FilterOrder',8,'PassbandFrequency',40, 'PassbandRipple',.2,'StopbandAttenuation', 65,'SampleRate',1000);
    %fvtool(d2)
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0); ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
    trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%

    trials_numrdnt=trials1; cnt=0;
    for t=1:size(trials1,2);
        if trials1(t)==1;
            trials_numrdnt(1,t)=trials1(t)+cnt;
            cnt=1+cnt;
        else
            cnt=0;
            trials_numrdnt(1,t)=99;
        end
    end
    for t=1:size(trials_numrdnt,2);
        if trials_numrdnt(1,t)>98;
            trials_numrdnt(1,t)=99;
        elseif trials_numrdnt(1,t)==1;
            % trials_numrdnt(1,t)=99;
        end
    end
    if rednumber>0;
        erp1=erp1(and(trials_numrdnt<rednumber,trials_numrdnt<99),:);erp2=erp2(and(trials_numrdnt<rednumber,trials_numrdnt<99),:);
    else
        erp1=erp1(trials_numrdnt<99,:);erp2=erp2(trials_numrdnt<99,:);
    end

    %pfc phase to V1 amplitude
    X1=[];X2=[];X1a=[]; YYamp=[];
    for tr=1:size(erp1,1)
        tmp=erp1(tr,:);
        han=hanning(500);
        for t=251:1100;
            f=fft(han'.*tmp(t-250:t+249));
            a=abs(f);
            data1(t)=mean(a(freq3:freq4)); %these are the gamma freqs
        end

        X1(:,tr)=data1([t1:t2 t3:t4]);
        X1a(:,tr)=data1([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp2(tr,:)))./abs(hilbert(filter(d4,erp2(tr,:))));
        X2(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp2(tr,:)))';
        YYamp(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
    end
    XX=X1(:); YY=X2(:); XXa=X1a(:); YYamp=YYamp(:);
    pr=prctile(XX,99);
    YY=YY(XX<pr);XX=XX(XX<pr);XXa=XXa(XX<pr); YYamp=YYamp(XX<pr);
    XX=XX./pr;XXa=XXa/pr;
    %   YYtmp=abs(YYamp); pr1=prctile(YYtmp,15); pr2=prctile(YYtmp,85);  YY=YY(and(YYtmp>pr1,YYtmp<pr2)); XX=XX(and(YYtmp>pr1,YYtmp<pr2)); XXa=XXa(and(YYtmp>pr1,YYtmp<pr2));
    ZZ=YY.*XX; % phase amp coupling
    YYa=real(YY); YYb=imag(YY);
    model1=fitlm(XXa,XX);
    model2=fitlm(horzcat(XXa,YYa,YYb),XX);
    rch=model2.Rsquared.ordinary-model1.Rsquared.ordinary;
    F=(rch./2)/((1-rch)/size(YY,1));
    PAs_p2v_rdnt2(:,fila)=abs(mean(ZZ));
    GCs_p2v_rdnt2(:,fila)=F;%rch;


    %v1 phase to PFC amplitude
    X1=[];X2=[];X1a=[]; YYamp=[];
    for tr=1:size(erp2,1)
        tmp=erp2(tr,:);
        han=hanning(500);
        for t=251:1100;
            f=fft(han'.*tmp(t-250:t+249));
            a=abs(f);
            data1(t)=mean(a(freq3:freq4)); %these are the gamma freqs
        end

        X1(:,tr)=data1([t1:t2 t3:t4]);
        X1a(:,tr)=data1([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp1(tr,:)))./abs(hilbert(filter(d4,erp1(tr,:))));
        X2(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        tmp=hilbert(filter(d4,erp1(tr,:)))';
        YYamp(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
    end
    XX=X1(:); YY=X2(:); XXa=X1a(:); YYamp=YYamp(:);
    pr=prctile(XX,99);
    YY=YY(XX<pr);XX=XX(XX<pr);XXa=XXa(XX<pr); YYamp=YYamp(XX<pr);
    XX=XX./pr;XXa=XXa/pr;
    % YYtmp=abs(YYamp); pr1=prctile(YYtmp,15); pr2=prctile(YYtmp,85);  YY=YY(and(YYtmp>pr1,YYtmp<pr2)); XX=XX(and(YYtmp>pr1,YYtmp<pr2)); XXa=XXa(and(YYtmp>pr1,YYtmp<pr2));
    ZZ=YY.*XX; % phase amp coupling
    YYa=real(YY); YYb=imag(YY);
    model1=fitlm(XXa,XX);
    model2=fitlm(horzcat(XXa,YYa,YYb),XX);
    rch=model2.Rsquared.ordinary-model1.Rsquared.ordinary;
    F=(rch./2)/((1-rch)/size(YY,1));
    PAs_v2p_rdnt2(:,fila)=abs(mean(ZZ));
    GCs_v2p_rdnt2(:,fila)=F;%rch;
    disp(fila)
end

%GCs_v2p_rdnt2=GCs_v2p_control; GCs_p2v_rdnt2=GCs_p2v_control; 

for two_obvs_per_mouse=two_obvs_per_mouse;
    if two_obvs_per_mouse==1;
        clear tmp1 tmp2 tmp3 tmp4 tmp5 tmp6 tmp7 tmp8
        for j=1:(fila/2);
             tmp1(1,j)=mean(GCs_p2v_control(1,(((j-1)*2)+1):(j*2)));
             tmp2(1,j)=mean(GCs_v2p_control(1,(((j-1)*2)+1):(j*2)));
             tmp3(1,j)=mean(GCs_p2v_rdnt(1,(((j-1)*2)+1):(j*2)));
             tmp4(1,j)=mean(GCs_v2p_rdnt(1,(((j-1)*2)+1):(j*2)));
             tmp5(1,j)=mean(GCs_p2v_dev(1,(((j-1)*2)+1):(j*2)));
             tmp6(1,j)=mean(GCs_v2p_dev(1,(((j-1)*2)+1):(j*2)));
             tmp7(1,j)=mean(GCs_p2v_rdnt2(1,(((j-1)*2)+1):(j*2)));
             tmp8(1,j)=mean(GCs_v2p_rdnt2(1,(((j-1)*2)+1):(j*2)));
        end
        GCs_p2v_control=tmp1';
        GCs_v2p_control=tmp2';
        GCs_p2v_rdnt=tmp3';
        GCs_v2p_rdnt=tmp4';
        GCs_p2v_dev=tmp5';
        GCs_v2p_dev=tmp6';
        GCs_p2v_rdnt2=tmp7';
        GCs_v2p_rdnt2=tmp8';
    end
end

%errorbar_groups([mean(PAs_p2v_control) mean(PAs_v2p_control) ;mean(PAs_p2v_rdnt) mean(PAs_v2p_rdnt) ; mean(PAs_p2v_dev) mean(PAs_v2p_dev)]',[std(PAs_p2v_control)./sqrt(fila) std(PAs_v2p_control)./sqrt(fila); std(PAs_p2v_rdnt)./sqrt(fila) std(PAs_v2p_rdnt)./sqrt(fila); std(PAs_p2v_dev)./sqrt(fila) std(PAs_v2p_dev)./sqrt(fila)]', 'bar_colors',[.5 .5 .5; 0 0 1; 1 0 0]); title('phase amp locking');
% errorbar_groups([mean(GCs_p2v_control) mean(GCs_v2p_control) ;mean(GCs_p2v_rdnt) mean(GCs_v2p_rdnt) ; mean(GCs_p2v_dev) mean(GCs_v2p_dev)]',[std(GCs_p2v_control)./sqrt(fila) std(GCs_v2p_control)./sqrt(fila); std(GCs_p2v_rdnt)./sqrt(fila) std(GCs_v2p_rdnt)./sqrt(fila); std(GCs_p2v_dev)./sqrt(fila) std(GCs_v2p_dev)./sqrt(fila)]', 'bar_colors',[.5 .5 .5; 0 0 1; 1 0 0]); title('grangers');

%errorbar_groups([mean(PAs_p2v_control) mean(PAs_v2p_control) ;mean(PAs_p2v_rdnt) mean(PAs_v2p_rdnt) ; mean(PAs_p2v_rdnt2) mean(PAs_v2p_rdnt2)]',[std(PAs_p2v_control)./sqrt(fila) std(PAs_v2p_control)./sqrt(fila); std(PAs_p2v_rdnt)./sqrt(fila) std(PAs_v2p_rdnt)./sqrt(fila); std(PAs_p2v_rdnt2)./sqrt(fila) std(PAs_v2p_rdnt2)./sqrt(fila)]', 'bar_colors',[.5 .5 .5; 0 0 1; 1 0 0]); title('phase amp locking');

tx1=(GCs_p2v_control+GCs_v2p_control)/2;tx2=(GCs_p2v_rdnt+GCs_v2p_rdnt)/2;tx3=(GCs_p2v_dev+GCs_v2p_dev)/2;tx4=(GCs_p2v_rdnt2+GCs_v2p_rdnt2)/2;
errorbar_groups([mean(GCs_p2v_control) mean(GCs_v2p_control) ;mean(GCs_p2v_rdnt) mean(GCs_v2p_rdnt) ; mean(GCs_p2v_dev) mean(GCs_v2p_dev) ; mean(GCs_p2v_rdnt2) mean(GCs_v2p_rdnt2)]',[std(GCs_p2v_control-tx1)./sqrt(fila) std(GCs_v2p_control-tx1)./sqrt(fila); std(GCs_p2v_rdnt-tx2)./sqrt(fila) std(GCs_v2p_rdnt-tx2)./sqrt(fila); std(GCs_p2v_dev-tx3)./sqrt(fila) std(GCs_v2p_dev-tx3)./sqrt(fila); std(GCs_p2v_rdnt2-tx4)./sqrt(fila) std(GCs_v2p_rdnt2-tx4)./sqrt(fila)]', 'bar_colors',[ 0 0 1; .5 .5 1],'bar_names',{'control';'redundants';'deviants';'rdnt2';}); title('grangers');
figure; scatter(GCs_v2p_rdnt,GCs_p2v_rdnt); hold on; plot(0:max(horzcat(GCs_v2p_rdnt,GCs_p2v_rdnt)),0:max(horzcat(GCs_v2p_rdnt,GCs_p2v_rdnt)));

lGCs_p2v_control=log(GCs_p2v_control);
lGCs_v2p_control=log(GCs_v2p_control);
lGCs_p2v_rdnt=log(GCs_p2v_rdnt);
lGCs_v2p_rdnt=log(GCs_v2p_rdnt);
lGCs_p2v_dev=log(GCs_p2v_dev);
lGCs_v2p_dev=log(GCs_v2p_dev);
lGCs_p2v_rdnt2=log(GCs_p2v_rdnt2);
lGCs_v2p_rdnt2=log(GCs_v2p_rdnt2);
tmp1=vertcat(lGCs_p2v_control,lGCs_v2p_control,lGCs_p2v_rdnt,lGCs_v2p_rdnt,lGCs_p2v_dev,lGCs_v2p_dev,lGCs_p2v_rdnt2,lGCs_v2p_rdnt2);
tmp2=vertcat((lGCs_p2v_control*0)+1.05,(lGCs_v2p_control*0)+1.95,(lGCs_p2v_rdnt*0)+3.05,(lGCs_v2p_rdnt*0)+3.95,(lGCs_p2v_dev*0)+5.05,(lGCs_v2p_dev*0)+5.95,(lGCs_p2v_rdnt2*0)+7.05,(lGCs_v2p_rdnt2*0)+7.95);
tx1=(lGCs_p2v_control+lGCs_v2p_control)/2;tx2=(lGCs_p2v_rdnt+lGCs_v2p_rdnt)/2;tx3=(lGCs_p2v_dev+lGCs_v2p_dev)/2;tx4=(lGCs_p2v_rdnt2+lGCs_v2p_rdnt2)/2;
figure; errorbar_groups([mean(lGCs_p2v_control) mean(lGCs_v2p_control) ;mean(lGCs_p2v_rdnt) mean(lGCs_v2p_rdnt) ; mean(lGCs_p2v_dev) mean(lGCs_v2p_dev) ; mean(lGCs_p2v_rdnt2) mean(lGCs_v2p_rdnt2)]',[std(lGCs_p2v_control-tx1)./sqrt(fila) std(lGCs_v2p_control-tx1)./sqrt(fila); std(lGCs_p2v_rdnt-tx2)./sqrt(fila) std(lGCs_v2p_rdnt-tx2)./sqrt(fila); std(lGCs_p2v_dev-tx3)./sqrt(fila) std(lGCs_v2p_dev-tx3)./sqrt(fila); std(lGCs_p2v_rdnt2-tx4)./sqrt(fila) std(lGCs_v2p_rdnt2-tx4)./sqrt(fila)]', 'bar_colors',[ 0 0 1; .5 .5 1],'bar_names',{'control';'rdnt>4';'deviants';'rdnt<4';}); title('grangers'); hold on
scatter(tmp2,tmp1);
for c=1:4
    
    a= tmp2( ((c-1)*((size(tmp1,1)./4)))+1  :(c*(size(tmp1,1)./4)));
    b= tmp1( ((c-1)*((size(tmp1,1)./4)))+1 :(c*(size(tmp1,1)./4)));
    subs=(size(a,1)./2);
    for s=1:subs;
        line(horzcat(a(s),a(s+subs)),horzcat(b(s),b(s+subs)));
    end

end

%figure; scatter(lGCs_v2p_rdnt,lGCs_p2v_rdnt); hold on; plot(0:max(horzcat(lGCs_v2p_rdnt,lGCs_p2v_rdnt)),0:max(horzcat(lGCs_v2p_rdnt,lGCs_p2v_rdnt)));
%figure; scatter(lGCs_v2p_rdnt2,lGCs_p2v_rdnt2); hold on; plot(0:max(horzcat(lGCs_v2p_rdnt,lGCs_p2v_rdnt2)),0:max(horzcat(lGCs_v2p_rdnt,lGCs_p2v_rdnt2)));

[h,p,ci,stats]=ttest(lGCs_p2v_control,lGCs_v2p_control); stats
p
[h,p,ci,stats]=ttest(lGCs_p2v_rdnt,lGCs_v2p_rdnt); stats
p
[h,p,ci,stats]=ttest(lGCs_p2v_dev,lGCs_v2p_dev); stats
p
%

anova_rm({[lGCs_p2v_control lGCs_p2v_rdnt lGCs_p2v_dev lGCs_p2v_rdnt2] [lGCs_v2p_control lGCs_v2p_rdnt lGCs_v2p_dev lGCs_v2p_rdnt2]});%
anova_rm({[lGCs_p2v_control lGCs_v2p_control] [lGCs_p2v_rdnt lGCs_v2p_rdnt] [lGCs_p2v_dev lGCs_v2p_dev]});% [lGCs_p2v_rdnt2' lGCs_v2p_rdnt2']}); %[lGCs_p2v_rdnt2' lGCs_v2p_rdnt2']});%
%anova_rm(horzcat([lGCs_p2v_control-lGCs_v2p_control]',[lGCs_p2v_rdnt-lGCs_v2p_rdnt]',[lGCs_p2v_dev-lGCs_v2p_dev]'))
(mean([lGCs_p2v_rdnt])-mean([lGCs_p2v_control]))./std((lGCs_p2v_control+lGCs_p2v_rdnt)/2)
[h,p,ci,stats]=ttest(lGCs_p2v_rdnt,lGCs_v2p_rdnt); stats
p

a=horzcat([lGCs_p2v_control lGCs_p2v_rdnt lGCs_p2v_dev lGCs_p2v_rdnt2],[lGCs_v2p_control lGCs_v2p_rdnt lGCs_v2p_dev lGCs_v2p_rdnt2])




[h,p,ci,stats]=ttest(lGCs_p2v_rdnt,lGCs_v2p_rdnt); stats
p
[h,p,ci,stats]=ttest(lGCs_p2v_rdnt2,lGCs_v2p_rdnt2); stats
p
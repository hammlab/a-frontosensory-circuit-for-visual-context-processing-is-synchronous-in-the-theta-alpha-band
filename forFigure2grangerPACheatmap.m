clcl
rednumber=0; %redundant to analyse. if 0, use all. for fig, i used 0
lag=10; %lag of phase
computelagfromtheta=0; %compute the lag based on peak freq

t1=400; t2=799; t3=800; t4=1100;%times of analysis% 
for fileload=1;

     files1={};%
    files2={};%;

    two_obvs_per_mouse=1;

end

%find peak
for calcpeakfreq=1;
    for fila=1:size(files2,1);
        load(files2{fila},'tfs1','tfs2','basetime1','time2');
        clear coher
        for calculatecoherencebetweens=1;

            for fa=3:15;
                lags=0; cnt=0;
                for t=1:100;
                    for tr=1:size(tfs1,3);
                        l1=tfs1(fa,t,tr)./abs(tfs1(fa,t,tr));
                        l2=tfs2(fa,t,tr)./abs(tfs2(fa,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                end
                coher(fa,1)=abs(lags/cnt);
            end
        end
        tmp=mean(coher,2);
        fr=max(tmp);
        for fa=1:15;
            if coher(fa,1)==fr;
                peakf(fila,1)=fa;
            end
        end
        if peakf(fila,1)==3;
            peakf(fila,1)=7;
        elseif peakf(fila,1)==15;
            peakf(fila,1)=9;
        end
        freq1(fila)=peakf(fila,1)-1; freq2(fila)=peakf(fila,1)+1;
    end
end
save('freq1.mat','freq1');

tic
%rdnt
for fila=1:size(files1,1);
    load(files2{fila});
    

    %d2 = designfilt('lowpassiir',  'FilterOrder',8,'PassbandFrequency',40, 'PassbandRipple',.2,'StopbandAttenuation', 65,'SampleRate',1000);
    %fvtool(d2)
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0); ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
    trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%

    trials_numrdnt=trials1; cnt=0;
    for t=1:size(trials1,2);
        if trials1(t)==1;
            trials_numrdnt(1,t)=trials1(t)+cnt;
            cnt=1+cnt;
        elseif trials1(t)==2;
            trials_numrdnt(t)=8;
            cnt=0;
            %trials_numrdnt(1,t)=99;
        else 
            cnt=0;
            trials_numrdnt(1,t)=99;
        end
    end
    for t=1:size(trials_numrdnt,2);
        if trials_numrdnt(1,t)>98;
            trials_numrdnt(1,t)=99;
        elseif trials_numrdnt(1,t)==1;
            % trials_numrdnt(1,t)=99;
        end
    end
    if rednumber>0;
        erp1=erp1(trials_numrdnt==rednumber,:);erp2=erp2(trials_numrdnt==rednumber,:);
    else
        erp1=erp1(trials_numrdnt<99,:);erp2=erp2(trials_numrdnt<99,:);
    end
    erp1a=erp1; erp2a=erp2; 
    %%% load control
      load(files1{fila});
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);    
    %erp1=erp1(trials1>2,:);erp2=erp2(trials1>2,:);

    erp1=vertcat(erp1,erp1a); erp2=vertcat(erp2,erp2a);

    
    pow1=[];pow1lag=[]; 
    for tr=1:size(erp1,1)
        tmp=erp1(tr,:);
        han=hanning(500);
        for t=251:1100;
            f=fft(han'.*tmp(t-250:t+249));
            a=abs(f);
            data1(t,:)=(a(10:58)); %these are the gamma freqs
        end

        pow1(:,:,tr)=data1([t1:t2 t3:t4],:);
        pow1lag(:,:,tr)=data1([(t1-lag):(t2-lag) (t3-lag):(t4-lag)],:);
    end
    
    for freq1=2:40;
        if computelagfromtheta==1;
            lag=floor((1000/freq1)/8);
        end
        d4 = designfilt('bandpassiir','FilterOrder',2, 'HalfPowerFrequency1',freq1-1,'HalfPowerFrequency2',freq1+1, 'SampleRate',1000);
        YYamp=[];X2=[];
        for tr=1:size(erp1,1);
            tmp=hilbert(filter(d4,erp2(tr,:)))./abs(hilbert(filter(d4,erp2(tr,:))));
            X2(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
            tmp=hilbert(filter(d4,erp2(tr,:)))';
            YYamp(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        end
        %pfc phase to V1 amplitude
        for freq2=1:size(pow1,2);
            X1=[];X1a=[]; 
            X1=squeeze(pow1(:,freq2,:));
            X1a=squeeze(pow1lag(:,freq2,:));   
            XX=X1(:); YY=X2(:); XXa=X1a(:); YYamp=YYamp(:);
            pr=prctile(XX,99);
            YY=YY(XX<pr);XX=XX(XX<pr);XXa=XXa(XX<pr); YYamp=YYamp(XX<pr);
            XX=XX./pr;XXa=XXa/pr;
            %   YYtmp=abs(YYamp); pr1=prctile(YYtmp,15); pr2=prctile(YYtmp,85);  YY=YY(and(YYtmp>pr1,YYtmp<pr2)); XX=XX(and(YYtmp>pr1,YYtmp<pr2)); XXa=XXa(and(YYtmp>pr1,YYtmp<pr2));
            ZZ=YY.*XX; % phase amp coupling  
            PAs_p2v(freq1,freq2,fila)=abs(mean(ZZ));

            YYa=real(YY); YYb=imag(YY);
            model1=fitlm(XXa,XX);
            model2=fitlm(horzcat(XXa,YYa,YYb),XX);
            rch=model2.Rsquared.ordinary-model1.Rsquared.ordinary;
            F=(rch./2)/((1-rch)/size(YY,1));
            GCs_p2v(freq1,freq2,fila)=F;
        end
    end
    disp(fila)
    toc
end

save('GCs_p2v_7.mat','GCs_p2v')
save('PAs_p2v_7.mat','PAs_p2v')

tic
%rdnt
for fila=1:size(files1,1);
    load(files2{fila});
    
    erp1a=erp1;
    erp2a=erp2;
    erp1=erp2a;
    erp2=erp1a;
    %d2 = designfilt('lowpassiir',  'FilterOrder',8,'PassbandFrequency',40, 'PassbandRipple',.2,'StopbandAttenuation', 65,'SampleRate',1000);
    %fvtool(d2)
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0); ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
    trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%

    trials_numrdnt=trials1; cnt=0;
    for t=1:size(trials1,2);
        if trials1(t)==1;
            trials_numrdnt(1,t)=trials1(t)+cnt;
            cnt=1+cnt;
        elseif trials1(t)==2;
            trials_numrdnt(t)=8;
            cnt=0;
            %trials_numrdnt(1,t)=99;
        else 
            cnt=0;
            trials_numrdnt(1,t)=99;
        end
    end
    for t=1:size(trials_numrdnt,2);
        if trials_numrdnt(1,t)>98;
            trials_numrdnt(1,t)=99;
        elseif trials_numrdnt(1,t)==1;
            % trials_numrdnt(1,t)=99;
        end
    end
    if rednumber>0;
        erp1=erp1(trials_numrdnt==rednumber,:);erp2=erp2(trials_numrdnt==rednumber,:);
    else
        erp1=erp1(trials_numrdnt<99,:);erp2=erp2(trials_numrdnt<99,:);
    end

    erp1a=erp1; erp2a=erp2; 
    %%% load control
      load(files1{fila});
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);    
   %erp1=erp1(trials1>2,:);erp2=erp2(trials1>2,:);

    erp1=vertcat(erp1,erp1a); erp2=vertcat(erp2,erp2a);
    
    pow1=[];pow1lag=[]; 
    for tr=1:size(erp1,1)
        tmp=erp1(tr,:);
        han=hanning(500);
        for t=251:1100;
            f=fft(han'.*tmp(t-250:t+249));
            a=abs(f);
            data1(t,:)=(a(10:58)); %these are the gamma freqs
        end

        pow1(:,:,tr)=data1([t1:t2 t3:t4],:);
        pow1lag(:,:,tr)=data1([(t1-lag):(t2-lag) (t3-lag):(t4-lag)],:);
    end
    
    for freq1=2:40;
        if computelagfromtheta==1;
            lag=floor((1000/freq1)/8);
        end
        d4 = designfilt('bandpassiir','FilterOrder',2, 'HalfPowerFrequency1',freq1-1,'HalfPowerFrequency2',freq1+1, 'SampleRate',1000);
        YYamp=[];X2=[];
        for tr=1:size(erp1,1);
            tmp=hilbert(filter(d4,erp2(tr,:)))./abs(hilbert(filter(d4,erp2(tr,:))));
            X2(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
            tmp=hilbert(filter(d4,erp2(tr,:)))';
            YYamp(:,tr)=tmp([(t1-lag):(t2-lag) (t3-lag):(t4-lag)]);
        end
        %pfc phase to V1 amplitude
        for freq2=1:size(pow1,2);
            X1=[];X1a=[]; 
            X1=squeeze(pow1(:,freq2,:));
            X1a=squeeze(pow1lag(:,freq2,:));   
            XX=X1(:); YY=X2(:); XXa=X1a(:); YYamp=YYamp(:);
            pr=prctile(XX,99);
            YY=YY(XX<pr);XX=XX(XX<pr);XXa=XXa(XX<pr); YYamp=YYamp(XX<pr);
            XX=XX./pr;XXa=XXa/pr;
            %   YYtmp=abs(YYamp); pr1=prctile(YYtmp,15); pr2=prctile(YYtmp,85);  YY=YY(and(YYtmp>pr1,YYtmp<pr2)); XX=XX(and(YYtmp>pr1,YYtmp<pr2)); XXa=XXa(and(YYtmp>pr1,YYtmp<pr2));
            ZZ=YY.*XX; % phase amp coupling  
            PAs_v2p(freq1,freq2,fila)=abs(mean(ZZ));

            YYa=real(YY); YYb=imag(YY);
            model1=fitlm(XXa,XX);
            model2=fitlm(horzcat(XXa,YYa,YYb),XX);
            rch=model2.Rsquared.ordinary-model1.Rsquared.ordinary;
            F=(rch./2)/((1-rch)/size(YY,1));
            GCs_v2p(freq1,freq2,fila)=F;
        end
    end
    disp(fila)
    toc
end
save('GCs_v2p_7.mat','GCs_v2p')
save('PAs_v2p_7.mat','PAs_v2p')
%clcl; load('GCs_v2p_6');load('PAs_v2p_6');load('GCs_p2v_6');load('PAs_p2v_6');load('freq1');


clear sm_v2p sm_p2v
for smoothin=2;
    for fil=1:size(GCs_p2v,3);
        tmp1=GCs_p2v(:,:,fil);
        clear sm1 sm2
        for t=1:size(tmp1,2);
            x=2:40;
            sm1(:,t)=smoothdata(interp1(x,tmp1(2:40,t),2:.2:40,'linear'),'movmedian',30);
        end
        for l=2:size(sm1,1);
            x=1:49;
            sm2(l,:)=smoothdata(interp1(x,sm1(l,:),1:.2:49,'linear'),'movmedian',30);
        end
        sm_p2v(:,:,fil)=abs(sm2(2:end,:));
    end

    for fil=1:size(GCs_v2p,3);
        tmp1=GCs_v2p(:,:,fil);
        clear sm1 sm2
        for t=1:size(tmp1,2);
            x=2:40;
            sm1(:,t)=smoothdata(interp1(x,tmp1(2:40,t),2:.2:40,'linear'),'movmedian',30);
        end
        for l=2:size(sm1,1);
            x=1:49;
            sm2(l,:)=smoothdata(interp1(x,sm1(l,:),1:.2:49,'linear'),'movmedian',30);
        end
        sm_v2p(:,:,fil)=abs(sm2(2:end,:));
    end
end
tmpcor=sm_p2v; %makin a non-neg baseline
for t=1:size(sm_p2v,1);
    for f=1:size(sm_p2v,2);
        for s=1:size(sm_p2v,3);
            if log(sm_p2v(t,f,s))<0;
                tmpcor(t,f,s)=1.01;
            end
        end
    end
end
mnLOG2=mean(((log(sm_p2v)-log(sm_v2p))./(log(tmpcor))),3);%((log(sm_p2v)+log(sm_v2p)))),3);
mnLOG3=(log(mean(sm_p2v,3))-log(mean(sm_v2p,3)))./(log(mean(sm_p2v,3))+log(mean(sm_v2p,3)));
mnPER2=mean((((sm_p2v)./(sm_v2p))),3);%./((sm_p2v)+(sm_v2p))),3);
mnPER1=mean((((sm_p2v)-(sm_v2p))./((sm_p2v)+(sm_v2p))),3)*2;
 
for fil=1:size(sm_p2v,3);
    tmp=mean((((sm_p2v(:,:,fil))-(sm_v2p(:,:,fil)))./((sm_p2v(:,:,fil))+(sm_v2p(:,:,fil)))),3)*2;
    %tmp=mean((((sm_p2v(:,:,fil))./(sm_v2p(:,:,fil)))),3);
    freqa=126;
    mnPERall(:,fil)=mean(tmp(:,freqa:end),2);%35hz=40; 40hz=51; 40hz=51; 50hz=76; 65hz=114; 70hz=126; 90hz=176;
    mnPER1a(:,fil)=mean(sm_p2v(:,freqa:end,fil),2);%./((mean(sm_p2v(:,freqa:end,fil),2)+mean(sm_v2p(:,freqa:end,fil),2))/2);
    mnPER1b(:,fil)=mean(sm_v2p(:,freqa:end,fil),2);%./((mean(sm_p2v(:,freqa:end,fil),2)+mean(sm_v2p(:,freqa:end,fil),2))/2);  
end
fs1=2.2:.2:40;
fs2=20:.4:116;
figure; shadedErrorBar(fs1,mean(mnPERall,2),std(mnPERall,1,2)./sqrt(7));
 figure; shadedErrorBar(fs1,mean(mnPER1a,2),std((mnPER1a)/2,1,2)./sqrt(6),'lineProps','b'); hold on
 shadedErrorBar(fs1,mean(mnPER1b,2),std((mnPER1b)/2,1,2)./sqrt(6),'lineProps','k'); set(gca,'yscale','log');
 

 figure; imagesc(fs1,fs2,mnPER2'); axis xy;  axis([2.5 40 30 116]); caxis([.8 2.5]);     colormap jet 
 figure; contourf(fs1,fs2,mnPER1',40, 'linecolor', 'none'); axis xy; colormap jet; set(gca,'yscale','log');
caxis([0 .7]); ylim([38 116]);


 
%% plot by peak freq
p2v_diff=[];p2v_peak=[];v2p_peak=[];
for fil=1:size(freq1,2);
    f=(freq1(fil)-2)*5;% f=45;
    p2v_diff(fil,:)=(log(sm_p2v(f,:,fil))-log(sm_v2p(f,:,fil)));

    p2v_peak(fil,:)=mean((sm_p2v(f-1:f+1,:,fil)),1);%./((sm_v2p(f-1:f+1,:,fil)+(sm_v2p(f-1:f+1,:,fil)))/2),1);
    v2p_peak(fil,:)=mean((sm_v2p(f-1:f+1,:,fil)),1);%./((sm_p2v(f-1:f+1,:,fil)+(sm_v2p(f-1:f+1,:,fil)))/2),1);
end
for f=1:size(p2v_peak,2);
    [h,p,ci,stats]=ttest(p2v_peak(:,f),v2p_peak(:,f));
    ps(f)=p;
end
figure; shadedErrorBar(fs2,mean(p2v_peak,1),std((p2v_peak-v2p_peak)/2,1,1)./sqrt(6),'lineProps','b'); hold on
 shadedErrorBar(fs2,mean(v2p_peak,1),std((p2v_peak-v2p_peak)/2,1,1)./sqrt(6),'lineProps','k');
 figure; plot(fs2,ps);

for reorganize_around_peak=1;

p2vRE=zeros(180,size(sm_v2p,2),size(sm_v2p,3));
v2pRE=p2vRE;
 for reorg=1;
     for fil=1:size(freq1,2);
         f=(freq1(fil)-2)*5; %f=30;
         p2vRE(:,:,fil)=sm_p2v(f-19:f+160,:,fil);
         v2pRE(:,:,fil)=sm_v2p(f-19:f+160,:,fil);
     end
 end
 tmpcor=p2vRE; %makin a non-neg baseline
for t=1:size(p2vRE,1);
    for f=1:size(p2vRE,2);
        for s=1:size(p2vRE,3);
            if log(p2vRE(t,f,s))<0;
                tmpcor(t,f,s)=1.01;
            end
        end
    end
end

mnLOG2=mean(((log(p2vRE)-log(v2pRE))./(log(tmpcor))),3);%((log(p2vRE)+log(v2pRE)))),3);

mnPER2=mean((((p2vRE)./(v2pRE))),3);%./((p2vRE)+(v2pRE))),3);
mnPER3=mean((((p2vRE)-(v2pRE))./((v2pRE)+(p2vRE))),3);
fs1=-3.8:.2:32;
fs2=20:.4:116;

 
 figure; imagesc(fs1,fs2,mnLOG2'); axis xy; caxis([0 .3]); colormap jet
 figure; imagesc(fs1,fs2,mnPER2'); axis xy; caxis([.8 2.5]); colormap jet 
 figure; imagesc(fs1,fs2,mnPER3'); axis xy; caxis([-.2 .2]); colormap jet 
     
    axis([-3 32 20 115]);
end

for fil=1:size(sm_p2v,3);
    tmp=mean((((p2vRE(:,:,fil))-(v2pRE(:,:,fil)))./((p2vRE(:,:,fil))+(v2pRE(:,:,fil)))),3)*2;
    mnPERall_RE(:,fil)=mean(tmp(:,135:end),2);%35hz=40; 40hz=51; 40hz=51; 50hz=76; 65hz=114; 70hz=126; 90hz=176;

  
end

fs2=20:.4:116;
figure; shadedErrorBar(fs1,mean(mnPERall_RE,2),std(mnPERall_RE,0,2)./sqrt(7));


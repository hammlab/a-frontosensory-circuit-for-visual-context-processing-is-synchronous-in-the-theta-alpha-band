
clcl
%%% decoding analysis from the paper
for file_load=1;
%
files={''}; %%put _28hz at the end! 
end

limtrials1=10; %limit the number of trials analysed for each stimulus type(control/redundandt/deviant).
baseBEGIN=1; baseEND=12; %what you call the baseline
BEGINo=14; ENDo=28; %these are the analysis windows. note: stimulus starts at 14 and finishes at 28
onestim=0; %if 0, then plot responses to both stimuli (e.g. 45 and 135 deg). if 1, then plot only to first orientation. if 2, then plot to second
dobeforeafter=0; %if this is "1", then it compares the first trials to the last for the test. if it's zero, it does even/odd
stdfromaverage=0; %normalize based on average, rather than background Df. did not use bc some of the baseline period was included in analysis

%%%%%%%this compiles all the files into three matrices
for buildingmatrices=1;
  
    count=0; counttest=0;
    effectsall=[];  %this is the main varable
    effectsallTR=[];  %this is the main varable
    actcont=[];
    countall=0;
end
for file=1:size(files,1);
   
        load(strcat('CA_workspace_SORTED_',files{file}));
   
    %figure; plot(stimvistypeMAIN); title(num2str(file));
    disp(files{file});
    for remove_first_trial_of_each_type=1;
        if remove_first_trial_of_each_type==1; 
            if limtrials1>0;
                limtrials=limtrials1;
                for ss1=1:2;
                    vecstim=vectrials{ss1,2};
                    tmps1=[];
                    for tr1=1:size(vecstim,2);
                        
                        if and(sum(vecstim(tr1)==tmps1)==0,vecstim(tr1)~=15)
                            tmps1=horzcat(tmps1,vecstim(tr1));
                           %vecstim(tr1)=99;
                        end
                        
                    end
                    for additionally_remove_first_few_controls=1;
                        tmps1=[]; gh=size(vecstim,2)+1;
                        disp(sum(vecstim==(20+(ss1-1)))); 
                        for tr1=1:size(vecstim,2);
                            if and(vecstim(gh-tr1)>15,vecstim(gh-tr1)<22)
                                if sum(tmps1==1)<(limtrials)
                                    tmps1=horzcat(tmps1,1);
                                else
                                    vecstim(gh-tr1)=99;
                                end
                            end
                        end
                    end
                    vectrials{ss1,2}=vecstim;
                end
            else
                for ss1=1:2;
                    vecstim=vectrials{ss1,2};
                    tmps1=[];
                    for tr1=1:size(vecstim,2);
                        
                        if and(sum(vecstim(tr1)==tmps1)==0,vecstim(tr1)~=15)
                            tmps1=horzcat(tmps1,vecstim(tr1));
                            vecstim(tr1)=99;
                        end
                        
                    end
                    vectrials{ss1,2}=vecstim;
                end
            end
        end
    end
    
    
    for analysis=1
        
        samp=1/framerate;
        timeaxis=-(samp*(framerate/2)):samp:(samp*framerate); timeaxis=timeaxis+.088;
        
        for stim=1:2;
            limtrials=limtrials1;
            vecdat=vectrials{stim,1};
            disp(size(vecdat,1));
            mousetmp=zeros(size(vecdat,1),1)+file; 
            vecstim=vectrials{stim,2};
            for renamevecstim=1;
                if renamevecstim==1; 
                    vecstim1=vecstim;
                    for t=1:size(vecstim,2);
                        if or(or(vecstim(1,t)==1,vecstim(1,t)==1),vecstim(1,t)==1)
                            vecstim1(1,t)=1;
                        elseif or(or(vecstim(1,t)==2,vecstim(1,t)==2),vecstim(1,t)==3)
                            vecstim1(1,t)=2;
                            elseif or(or(vecstim(1,t)==4,vecstim(1,t)==4),vecstim(1,t)==5)
                            vecstim1(1,t)=3;
                            elseif or(or(vecstim(1,t)==6,vecstim(1,t)==7),vecstim(1,t)==8)
                            vecstim1(1,t)=4;
                        end
                    end
                    vecstim=vecstim1;
                end
            end

            if limtrials1>0; 
            for checkingfortoo_few_trials=1;
                ss=[1 2 3 4 15 phzstd(stim)];
                for gss=1:6; 
                    sss(gss)=sum(vecstim==ss(gss));
                end
                if min(sss)<limtrials1;
                   limtrials=min(sss); 
                end
                disp(limtrials); 
            end
            
            effects=[]; effectsTR=[];
            for z1=1:8
                tmp1=vecdat(:,:,vecstim==z1);
                if size(tmp1,3)>(limtrials-1)
                    effects(:,:,z1+1)=mean(tmp1(:,:,1:limtrials),3);
                else
                    effects(:,:,z1+1)=mean(tmp1(:,:,1:end),3);
                end
            end
            for z1=1:4
                tmp1=vecdat(:,:,vecstim==z1);
                effectsTR(:,:,:,z1+1)=(tmp1(:,:,1:limtrials));                
            end

            tmp1=vecdat(:,:,vecstim==15);
            if size(tmp1,3)>(limtrials-1)
                effects(:,:,10)=mean(tmp1(:,:,1:limtrials),3);
            else
                effects(:,:,10)=mean(tmp1(:,:,1:end),3);
            end
            tmp1=vecdat(:,:,vecstim==15);
            effectsTR(:,:,:,6)=(tmp1(:,:,1:limtrials));

            tmp1=vecdat(:,:,vecstim==phzstd(stim));
            
                if size(tmp1,3)>(limtrials-1)
                    effects(:,:,1)=mean(tmp1(:,:,1:limtrials),3);
                     else
                    effects(:,:,1)=mean(tmp1(:,:,1:end),3);
                end
                tmp1=vecdat(:,:,vecstim==phzstd(stim));
                effectsTR(:,:,:,1)=(tmp1(:,:,1:limtrials));
            
                
            
            end
                
            
            for c1=1:size(effects,1) %baseline correction
                basos=[1 2 2 2 2 2 2 2 2 3; 1 3 3 3 3 3 3 3 3 2];
                at=squeeze(effects(c1,baseBEGIN:baseEND,:));
                
                for z1=1:size(effects,3)
                    mnn=min(effects(c1,baseBEGIN:baseEND,z1),[],2); 
                    tmpbasos=dfofanalyse(c1,phases==basos(stim,z1));
                    tmpbasos=tmpbasos(tmpbasos>0); tmpbasos=tmpbasos(tmpbasos<prctile(tmpbasos,92));
                    stt=std(tmpbasos);
                    if stdfromaverage==1; stt=std(at(:)); end
                    
                    if stt<.001; stt=std(effects(c1,ENDo:end,z1)); end
                    if stt<.001; stt=10000; end
                    for t1=1:size(effects,2)
                        effects(c1,t1,z1)=(effects(c1,t1,z1)-mnn)./stt;
                    end
                    if z1<7
                        for tri=1:size(effectsTR,3);
                            effectsTR(c1,:,tri,z1)=effectsTR(c1,:,tri,z1)-min(effectsTR(c1,1:12,tri,z1))./stt;
                        end
                    end
                    mnns(file,stim,c1,z1)=mnn;
                    stts(file,stim,c1,z1)=stt;
                end
                
            end
            
                  
            
                effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),2:10,stim)=effects(:,1:end,2:10);
                effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),1,stim)=effects(:,1:end,1);
                effectsallTR((1+countall):(countall+size(effects,1)),1:size(effects,2),:,2:6,stim)=effectsTR(:,1:end,:,2:6);
                effectsallTR((1+countall):(countall+size(effects,1)),1:size(effects,2),:,1,stim)=effectsTR(:,1:end,:,1);
          
            
        end
        
        countall=countall+size(effects,1);
    end
   
     
end
%close all
titles={'Control';' ';' ';' ';'redundants';' ';' ';' ';' ';'DEVIANT'};
redundantnumberA=4; % this is the redundant in the chain that you will plot/analyse; if it's 0, then average over all redundants
include_cutoff=-100; %number of standard deviations above baseline for inclusion
exclude_outlier=100; %get rid of the cell if any responses are this big

%if you want to just plot averages accross all trials, make stringent test
%%% equal to 0. if you want to do a hard test of clusters,make it 1... use
%%% 1 only if you are actually testing teh reality of a subcluster
for excluddin_outlier_cells=1; 
        simpleeffects=[];c1=1; c2=1; cols=[]; newindices=[];notindices=[];
        for stim=1:2
            for c=1:size(effectsall,1)
                if redundantnumberA==0; 
                	r=mean(mean(effectsall(c,BEGINo:ENDo,2:9,stim)));
                else
                    r=mean(effectsall(c,BEGINo:ENDo,redundantnumberA+1,stim));
                end
                cn=mean(effectsall(c,BEGINo:ENDo,1,stim));
                d=mean(effectsall(c,BEGINo:ENDo,10,stim));
               
                if and(or(or(r>include_cutoff,cn>include_cutoff),d>include_cutoff),and(and(r<exclude_outlier,cn<exclude_outlier),d<exclude_outlier))
                    simpleeffects(c1,1)=(cn);
                    simpleeffects(c1,2)=(r);
                    simpleeffects(c1,3)=(d);
                    newindices(c1,1)=c;
                    newindices(c1,2)=stim;
                    c1=1+c1;
                else
                    notindices(c2,1)=c;
                    notindices(c2,2)=stim;
                    c2=1+c2;
                    
                end
                
            end
        end
        for makingcolors=1;
            for c=1:(c1-1)
                s=simpleeffects(c,1)./max(simpleeffects(c,:));
                cols(c,2)=s;
                s=simpleeffects(c,3)./max(simpleeffects(c,:));
                cols(c,1)=s;
                s=simpleeffects(c,2)./max(simpleeffects(c,:));
                cols(c,3)=s;
            end
            for c=1:(c1-1);
                a=cols(c,:)-min(cols(c,:));
                cols(c,:)=cols(c,:)./max(cols(c,:));
            end
        end
        

end

redundantnumber=4; %if you want to plot (but not "select by") a redudnant later in the stream, use this line
%%%%for selecting all or subset of cells for subesequent plots
%%%for selecting all or subset of cells for subesequent plots
plotanti=0; %if =1, then plot everything outside of the selected box
selections=1; %if the cells you want to select are scattered more than a single box can capture, make this 2 or 3 and select multiple regions.
for plotandidentify=1;
    %close all force
    xcut=std((.5*simpleeffects(:,1)+.5*simpleeffects(:,3))-simpleeffects(:,2))*4; ycut=std(simpleeffects(:,3)-simpleeffects(:,1))*4;
    xmn=mean((.5*simpleeffects(:,1)+.5*simpleeffects(:,3))-simpleeffects(:,2)); ymn=mean(simpleeffects(:,3)-simpleeffects(:,1));
    
    pind=[];
    for selecto=1:selections;
        figure; scatter((.5*simpleeffects(:,1)+.5*simpleeffects(:,3))-simpleeffects(:,2),simpleeffects(:,3)-simpleeffects(:,1),50,cols,'fill');
       
        if selecto>1;
            hold on;
            scatter([x11(1) x11(1) x11(2) x11(2)],[y11(1) y11(2) y11(1) y11(2)]);
        end
        ylabel('Deviance Detection','FontSize',12,'FontWeight','bold'); xlabel('Stim specific adaptation','FontSize',12,'FontWeight','bold');
        title('pick region to plot; upper left, then lower right');
        [x,y]=ginput(2); close
        if selecto==1;
            x11=x; y11=y;
        end
        if selecto==2;
            x22=x; y22=y;
        end
        
        for c=1:size(simpleeffects,1);
            if and(((.5*simpleeffects(c,1)+.5*simpleeffects(c,3))-simpleeffects(c,2))>x(1),((.5*simpleeffects(c,1)+.5*simpleeffects(c,3))-simpleeffects(c,2))<x(2));
                if and((simpleeffects(c,3)-simpleeffects(c,1))<y(1),(simpleeffects(c,3)-simpleeffects(c,1))>y(2))
                    if sum(c==pind)==0;
                        pind=vertcat(pind,c);
                    end
                end
            end
        end
    end
    
    
    numcells=size(pind,1)
    inc=zeros(1,size(effectsallTR,1));
    for t=1:(size(effectsallTR,1)*2);
        if sum(pind==t)>0;
            inc(t)=1;
        end
    end
    a=size(inc,2)/2;
    inc1=((inc(1,1:a)+inc(1,(a+1):end))==2)*1;

end


%%%max norm
for c=1:size(effectsallTR,1);
    atmp=effectsallTR(c,:,:,:,:); 
    effectsallTR(c,:,:,:,:)=effectsallTR(c,:,:,:,:)./max(atmp(:));
end

%%%analyse
effectsallTR1(:,:,:,1,:)=effectsallTR(inc1==1,:,:,1,:);effectsallTR1(:,:,:,2,:)=mean(effectsallTR(inc1==1,:,:,3,:),4);effectsallTR1(:,:,:,3,:)=effectsallTR(inc1==1,:,:,6,:);
time1=BEGINo-5; time2=ENDo+5;%time2=BEGINo+3;%
%time1=B; time2=43;
comps=6; 
close all force
for doPCA=1;
    for first_remove_outlier_trials=1;
        dattmp=max(effectsallTR1(:,time1:time2,:,:,:),[],2);
        dat=[];
        for c=1:size(dattmp,1);
            atmp=dattmp(c,:,:,:,:);
            dat(c,:)=atmp(:)';
        end
        for t=1:size(dat,2);
            % dat(:,t)=(dat(:,t)-mean(dat(:,t)))./std(dat(:,t));
        end
        [coeff,score,latent,tsquared,explained,mu] = pca(dat'); %coeff=rotatefactors(coeff(:,1:2));%,'Method','Orthomax');%);%,'Method','Promax');%
        W=coeff';

        scorestmp=dat'*W';
        %close all force
        newtmp=(scorestmp(:,1)-scorestmp(:,1));
        for selecto=1;
            figure; scatter(scorestmp(:,1),scorestmp(:,2),50,'fill');

            title('pick region to plot; upper left, then lower right');
            [x,y]=ginput(2); close
             x11=x; y11=y;
            

            for c=1:size(scorestmp,1);
                if and(and(scorestmp(c,1)>x(1),scorestmp(c,1)<x(2)),and(scorestmp(c,2)>y(2),scorestmp(c,2)<y(1)));
                    newtmp(c,1)=1;
                end
            end
        end
        b=reshape(newtmp,[1,1,size(effectsallTR1,3),size(effectsallTR1,4),size(effectsallTR1,5)]);
         effectsallTR2=[];
        for c=1:size(effectsallTR1,1);
            for t=1:size(effectsallTR1,2);                
                atmp=effectsallTR1(c,t,:,:,:);
                effectsallTR2(c,t,:,:,:)=atmp(b(:,:,:,:,:)==1);
            end
        end
        includy=b; 
    end

    %%main PCA
    for then_do_main_PCA=1; 
          dattmp=(effectsallTR2(:,time1:time2,:));
        dat=[];
        for c=1:size(dattmp,1);
            atmp=dattmp(c,:,:);
            dat(c,:)=atmp(:)';
        end
        for t=1:size(dat,2);
            dat(:,t)=(dat(:,t)-mean(dat(:,t)))./std(dat(:,t));
        end
        [coeff,score,latent,tsquared,explained,mu] = pca(dat'); coeff=rotatefactors(coeff(:,1:comps));%,'Method','Promax');%);%,'Method','Orthomax');%);%,'Method','Promax');%
        W=coeff';
        figure(1); plot(explained(1:20),'k'); hold on; %close
            

    end

    close all
    %%then plot conditions
    datdiscrim=[]; stimvector=[];  stimtypevector=[];
    for stim=1:2;
        dattype=[];
        for stimtype=1:3;
             dattmp=mean(effectsallTR1(:,time1+4:BEGINo+2,:,stimtype,stim),2);
           %  dattmp=(effectsallTR1(:,time1-4:BEGINo+3,:,stimtype,stim)); %% for plotting!  
            %dattmp=mean(effectsallTR1(:,BEGINo:ENDo,:,stimtype,stim),2);
            % dattmp=(effectsallTR1(:,time1:time2,:,stimtype,stim)); %% for plotting!     
            for c=1:size(dattmp,1);
                atmp=dattmp(c,:,:,:,:);
                dattype(c,stimtype,:)=atmp(:)';
            end
        end


        colas={'k','b','b','b','b','r'};
        for stimtype=1:3;

            dattmp=squeeze(dattype(:,stimtype,:));
            inc=includy(1,1,:,stimtype,stim); inc=inc(:);
            for t=1:size(dattmp,2);
                dattmp(:,t)=(dattmp(:,t)-mean(dattmp(:,t)))./std(dattmp(:,t));
            end
            scores=dattmp'*W';
          %  scores=scores(inc==1,1:comps);
            cols1=zeros(size(scores,1),3);
            if stim==1;
                for c=1:size(cols1);
                    if stimtype==1;%(simpleeffectsTEST(c,3)-mean(simpleeffectsTEST(c,1),2))>1;%
                        cols1(c,:)=[.3 .3 .3];
                    elseif stimtype==3;
                        cols1(c,:)=[1 0 0];
                    else
                        cols1(c,:)=[0 0 1];
                    end
                end
                figure(stimtype+1); scatter3(scores(:,1),scores(:,3),scores(:,4),80,cols1,'filled');hold on
                axis([-10 10 -10 10 -10 10])
            else
                for c=1:size(cols1);
                    if stimtype==1;%(simpleeffectsTEST(c,3)-mean(simpleeffectsTEST(c,1),2))>1;%
                        cols1(c,:)=[.7 .7 .7];
                    elseif stimtype==3;
                        cols1(c,:)=[1 .7 .7];
                    else
                        cols1(c,:)=[.7 .7 1];
                    end
                end
                figure(stimtype+1); scatter3(scores(:,1),scores(:,3),scores(:,4),80,cols1,'filled');hold on
             axis([-10 10 -10 10 -10 10])
            end
            
            datdiscrim=vertcat(datdiscrim,scores);
            stimvector=vertcat(stimvector,(scores(:,1)-scores(:,1)+stim));
            stimtypevector=vertcat(stimtypevector,(scores(:,1)-scores(:,1)+stimtype));
            
%        
%             if stim==1;
%                 for c=1:size(cols1);
%                     if stimtype==1;%(simpleeffectsTEST(c,3)-mean(simpleeffectsTEST(c,1),2))>1;%
%                         cols1(c,:)=[.3 .3 .3];
%                     elseif stimtype==3;
%                         cols1(c,:)=[1 0 0];
%                     else
%                         cols1(c,:)=[0 0 1];
%                     end
%                 end
%                 figure(stimtype+4); scatter(scores(:,1),scores(:,3),80,cols1,'filled');hold on
%                %axis([-10 10 -10 10 -10 10])
%             else
%                 for c=1:size(cols1);
%                     if stimtype==1;%(simpleeffectsTEST(c,3)-mean(simpleeffectsTEST(c,1),2))>1;%
%                         cols1(c,:)=[.7 .7 .7];
%                     elseif stimtype==3;
%                         cols1(c,:)=[1 .7 .7];
%                     else
%                         cols1(c,:)=[.7 .7 1];
%                     end
%                 end
%                 figure(stimtype+4); scatter(scores(:,1),scores(:,3),80,cols1,'filled');hold on
%              %axis([-10 10 -10 10 -10 10])
%             end
            
           
        end
    end

    alldat=horzcat(stimvector,stimtypevector,datdiscrim);
end

 view(131,16)


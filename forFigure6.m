
clcl
clear all
%this is just for storing teh text. copy these lists and paste them into
%under where it says " if cond==1" and "else"... around 157
rednumber=4; % the number redundant to analyse
limtrials=10; %liman it trial is in all conditions to this. if 0, dont do it
limtodev=0;

dopowstandardize=0; %this is for standardizing the data for assessing baseline power. it takes time, so only do it when youre going to analyse power.
docontrol=0; % 1 = cno only. 0=dreadds were present;use the first cnt/vmm pre CNO, and then the second post CNO. for control, use teh second preCNO. this makes sense, bc the tehta responses were better then.
for cond=1:2
    for fileload=1; %load the files in here!
        matchy=1; ori1=5; ori2=6;
        if cond==1;

            for fileload=1; %control pre
                files1={}; %


                files2={};%;


            end
            for fileload=1; %dreadd pre
                if docontrol==0;
                    files1={''}; %control runs
                    files2={''};%vmmn runs

                end

            end
        else
            for fileload=1; %control post
                files3={''
                    }; %'

                files4={''
                    };%;

            end

            for fileload=1; %dreadd post

                if docontrol==0;
                    files3={''}; %control runs
                    files4={''};%vmmn runs

                end
            end

        end
    end
    if cond==2; files1a=files3; files2a=files4; files3=files1; files4=files2; files1=files1a; files2=files2a; end;
    %% do control
    for makematrices=1;
        erspsV1_control=[];
        itcsV1_control=[];
        erspsPFC_control=[];
        itcsPFC_control=[];
        powers_control=[];
        cohers_control=[];
        cohers_allC=[];
        cohers_control_alltimes=[];
        cohers_redundant_alltimes=[];
        cohers_deviant_alltimes=[];
        phases=[];
        lagalls=[];
        coherTIME_cntrl=zeros(100,350,size(files1,1));
        coherTIME_cntrl_smooth=zeros(100,350,size(files1,1));
        time3=88;
        phaseampsCNTpfc=zeros(100,14,5,size(files1,1));
        phaseampsCNTv1=zeros(100,14,5,size(files1,1));
        ERP_V1_control=[];
        ERP_PFC_control=[];
    end
    %control
    for fila=1:size(files1,1);
        if limtodev==1;
            load(files2{fila},'trials1','rej');trials1=trials1(:,rej==0); limtrials=sum(trials1==2); clear trials1 rej
        end

        load(files1{fila});
        trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0); ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
        trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%

        if limtrials>0;
            counta=0;
            for t=0:(size(trials1,2)-1);
                if (counta>(limtrials))
                    trials1(1,size(trials1,2)-t)=0;
                elseif or(trials1(1,size(trials1,2)-t)==5,trials1(1,size(trials1,2)-t)==6)
                    counta=1+counta;
                end
            end
        end


        load(files3{fila},'erp1','erp2','rej');
        erp3=erp1(rej==0,:); erp4=erp2(rej==0,:);
        load(files1{fila},'erp1','erp2','rej');
        erp1=erp1(rej==0,:); erp2=erp2(rej==0,:);

        powtmp=zeros(1,40);
        if dopowstandardize==1; a=vertcat(erp1(:,251:500),erp3(:,251:500)); st=std(a(:)); else; st=1; end
        etmp=erp1./st;
        for tr=1:size(erp1,1)
            %         han=hanning(1000)';
            %         a=fft(erp1(tr,175:1174).*han);
            %         powtmp=powtmp+abs(a(1,1:200));
            a=fft(etmp(tr,251:500).*hanning(250)');
            powtmp=powtmp+abs(a(1,1:40));
        end
        powers_control(:,fila,1)=powtmp./tr;

        powtmp=zeros(1,40);
        if dopowstandardize==1; a=vertcat(erp2(:,251:500),erp4(:,251:500)); st=std(a(:)); else; st=1; end
        etmp=erp2./st;
        for tr=1:size(erp2,1);
            %         han=hanning(1000)';
            %         a=fft(erp2(tr,175:1174).*han);
            %         powtmp=powtmp+abs(a(1,1:200));
            a=fft(etmp(tr,251:500).*hanning(250)');
            powtmp=powtmp+abs(a(1,1:40));
        end
        powers_control(:,fila,2)=powtmp./tr;


        if fila==1; erpsV1=mean(erp1); erpsPFC=mean(erp2); else
            erpsV1=erpsV1+mean(erp1);   erpsPFC=erpsPFC+mean(erp2);
        end

        tr=size(trials1,2); cut=round(tr/2);
        % trials1(1,1:50)=zeros(1,50);%
        %trials1(1,1:125)=zeros(1,125); %exclude first half of trials

        %     ersp=mean(abs(tfs1(:,:,trials1<5)).^2,3);
        %     for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end
        aa=ersp1(:,:,or(trials1==5,trials1==6));
        erspsV1_control(:,:,fila)=mean(aa(:,:,:),3);

        %     ersp=mean(abs(tfs2(:,:,trials1<5)).^2,3);
        %     for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end
        aa=ersp2(:,:,or(trials1==5,trials1==6));
        erspsPFC_control(:,:,fila)=mean(aa(:,:,:),3);

        tr=sum(or(trials1==5,trials1==6));
        tfs=tfs1(:,:,or(trials1==5,trials1==6));
        itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
        itcsV1_control(:,:,fila)=itc;

        tfs=tfs2(:,:,or(trials1==5,trials1==6));
        itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
        itcsPFC_control(:,:,fila)=itc;

        tfs1a=tfs1;tfs2a=tfs2;
        %tfs1=tfs1(:,:,or(trials1==5,trials1==6));
        %tfs2=tfs2(:,:,or(trials1==5,trials1==6));

        ERP_V1_control(:,fila)=mean(erp1(and(trials1>4,trials1<7),:),1);
        ERP_PFC_control(:,fila)=mean(erp2(and(trials1>4,trials1<7),:),1);

        %trialwise coherence
        clear coher
        for calculatecoherencebetweens=1;

            for f=1:size(f_sG1,2);
                for t=1:100;
                    lags=0; cnt=0;
                    for tr=1:size(tfs1,3);
                        l1=tfs1(f,t,tr)./abs(tfs1(f,t,tr));
                        l2=tfs2(f,t,tr)./abs(tfs2(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                    coher(f,t)=abs(lags/cnt);%-sqrt(-(1/cnt)*log(.5));
                end
            end
        end
        cohers_control(:,:,fila)=coher;%-cohersham;
        cohershams(:,:,fila)=cohersham;

        % tfs1=tfs1a(:,:,and(trials1>4,trials1<7));
        % tfs2=tfs2a(:,:,and(trials1>4,trials1<7));

        tfs1a=tfs1(:,:,or(trials1==ori1,trials1==ori2));
        tfs2a=tfs2(:,:,or(trials1==ori1,trials1==ori2));

        %coherence across all timepoints; do limited trials!
        clear coher
        for calculatecoherencebetweens=1
            for f=1:size(f_sG1,2)
                lags=0; cnt=0;
                for t=basetime1:time3
                    for tr=1:size(tfs1a,3)
                        l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                        l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                end
                coher(f)=abs(lags/cnt);%-sqrt(-(1/cnt)*log(.5));
            end
        end
        cohers_control_alltimes(:,fila)=coher;%-cohersham;

        %trialwise coherence... but all trials
        clear coher
        for calculatecoherencebetweens=1;
            %tfs1=tfs1a; tfs2=tfs2a;
            for f=1:size(f_sG1,2);
                for t=1:100;
                    lags=0; cnt=0;
                    for tr=1:size(tfs1,3);
                        l1=tfs1(f,t,tr)./abs(tfs1(f,t,tr));
                        l2=tfs2(f,t,tr)./abs(tfs2(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                    coher(f,t)=abs(lags/cnt);%-sqrt(-(1/cnt)*log(.5));
                end
            end
        end
        cohers_allC(:,:,fila)=coher;%-cohersham;
        cohershams(:,:,fila)=cohersham;






        trnums(1,fila)=sum(trials1(or(trials1==1,trials1==2))>0);
        trnums2(1,fila)=sum(trials1(or(trials1==1,trials1==2))>0);

        tfs1=tfs1b(:,:,and(trials2>0,trials2<100));
        tfs2=tfs2b(:,:,and(trials2>0,trials2<100));

        for calculatecoherenceTIMElines=1;

            for f=1:size(f_sG1,2);
                for tr=1:size(tfs1,3);
                    cnt=0; lags=0;
                    for t=2:99;
                        l1=tfs1(f,t,tr)./abs(tfs1(f,t,tr));
                        l2=tfs2(f,t,tr)./abs(tfs2(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                    coherTIME_cntrl(f,tr,fila)=abs(lags/cnt);%-sqrt(-(1/cnt)*log(.5));
                end
                %coherTIME_cntrl(f,:,fila)=coherTIME_cntrl(f,:,fila)-mean(coherTIME_cntrl(f,1:5,fila));
                coherTIME_cntrl_smooth(f,:,fila)=smoothdata(coherTIME_cntrl(f,:,fila),10)';
                % coherTIME_cntrl_smooth(f,:,fila)=coherTIME_cntrl_smooth(f,:,fila)-mean(coherTIME_cntrl_smooth(f,1,fila));

            end
        end
    end


    powers{1,cond}=powers_control;
    coherences{1,cond}=cohers_control;
    coherences{4,cond}=cohers_allC;
    coherences_alltime{1,cond}=cohers_control_alltimes;
    cohersTIME{1,cond}=coherTIME_cntrl_smooth;
    %pfcdrive{1,cond}=cohers_control_pfcdrive;
    %v1drive{1,cond}=cohers_control_V1drive;
    itcsV1{1,cond}=itcsV1_control;
    itcsPFC{1,cond}=itcsPFC_control;
    erspsV1{1,cond}=erspsV1_control;
    erspsPFC{1,cond}=erspsPFC_control;
    ERPsV1{1,cond}=ERP_V1_control;
    ERPsPFC{1,cond}=ERP_PFC_control;


    % figure; compass((mean(lagalls)/abs(mean(lagalls)))*size(lagalls,1)/10); hold on
    %  rose(angle(lagalls))
    %% do mismatch
    for makematrices=1;
        erspsV1_redundant=[];
        itcsV1_redundant=[];
        erspsPFC_redundant=[];
        itcsPFC_redundant=[];
        cohers_redundant=[];
        powers_odball=[];
        erspsV1_deviant=[];
        itcsV1_deviant=[];
        erspsPFC_deviant=[];
        itcsPFC_deviant=[];
        cohers_deviant=[];
        cohers_OB=[];
        cohers_redundant_alltimes=[];
        cohers_deviant_alltimes=[];
        coherTIME_ob=zeros(100,350,size(files2,1));
        coherTIME_ob_smooth=zeros(100,350,size(files2,1));
        lagalls=[];
        time3=88;
        phaseampsOBpfc=zeros(100,14,5,size(files2,1));
        phaseampsOBv1=zeros(100,14,5,size(files2,1));
        ERP_V1_redundant=[];
        ERP_PFC_redundant=[];
        ERP_V1_deviant=[];
        ERP_PFC_deviant=[];
    end

    for fila=1:size(files2,1);
        load(files2{fila});
        trials_numrdnt=trials1; cnt=0;
        for t=1:size(trials1,2);
            if trials1(t)==1;
                trials_numrdnt(1,t)=trials1(t)+cnt;
                cnt=1+cnt;
            else
                cnt=0;
                trials_numrdnt(1,t)=99;
            end
        end

        trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0);ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
        trials_numrdnt2=trials_numrdnt(:,rej==0);
        trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%

        %for limiting trials by manual definitiion
        if limtrials>0; %deviant
            counta=0;
            for t=1:size(trials1,2);
                if (counta>(limtrials))
                    trials1(1,t)=0;
                elseif trials1(1,t)==2
                    counta=1+counta;
                end
            end
        end

        %for limiting trials by manual definitiion
        if limtrials>0; %rdnt
            counta=0;
            for t=1:size(trials_numrdnt2,2);
                if (counta>(limtrials))
                    trials_numrdnt2(1,t)=0;
                elseif trials_numrdnt2(1,t)==rednumber
                    counta=1+counta;
                end
            end
        end


        load(files4{fila},'erp1','erp2','rej');
        erp3=erp1(rej==0,:); erp4=erp2(rej==0,:);
        load(files2{fila},'erp1','erp2','rej');
        erp1=erp1(rej==0,:); erp2=erp2(rej==0,:);

        powtmp=zeros(1,40);
        if dopowstandardize==1; a=vertcat(erp1(:,251:500),erp3(:,251:500)); st=std(a(:)); else; st=1; end
        etmp=erp1./st;
        for tr=1:size(erp1,1);
            %         han=hanning(1000)';
            %         a=fft(erp1(tr,175:1174).*han);
            %         powtmp=powtmp+abs(a(1,1:200));
            a=fft(etmp(tr,251:500).*hanning(250)');
            powtmp=powtmp+abs(a(1,1:40));
        end
        powers_oddball(:,fila,1)=powtmp./tr;

        powtmp=zeros(1,40);
        if dopowstandardize==1; a=vertcat(erp2(:,251:500),erp4(:,251:500)); st=std(a(:)); else; st=1; end
        etmp=erp2./st;
        for tr=1:size(erp2,1);
            %         han=hanning(1000)';
            %         a=fft(erp2(tr,175:1174).*han);
            %         powtmp=powtmp+abs(a(1,1:200));
            a=fft(etmp(tr,251:500).*hanning(250)');
            powtmp=powtmp+abs(a(1,1:40));
        end
        powers_oddball(:,fila,2)=powtmp./tr;



        %ersp=mean(abs(tfs1(:,:,trials1>1)).^2,3);
        %for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end
        erspsV1_deviant(:,:,fila)=mean(ersp1(:,:,trials1>1),3);

        %ersp=mean(abs(tfs2(:,:,trials1>1)).^2,3);
        %for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end
        erspsPFC_deviant(:,:,fila)=mean(ersp2(:,:,trials1>1),3);


        tr=sum(trials1>1);
        tfs=tfs1(:,:,trials1>1);
        itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
        itcsV1_deviant(:,:,fila)=itc;

        tfs=tfs2(:,:,trials1>1);
        itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
        itcsPFC_deviant(:,:,fila)=itc;

        ERP_V1_deviant(:,fila)=mean(erp1(trials1>1,:),1);
        ERP_PFC_deviant(:,fila)=mean(erp2(trials1>1,:),1);

        %ersp=mean(abs(tfs1(:,:,trials1==1)).^2,3);
        %for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end
        aa=ersp1(:,:,trials_numrdnt2==rednumber);
        erspsV1_redundant(:,:,fila)=mean(aa(:,:,:),3);

        %ersp=mean(abs(tfs2(:,:,trials1==1)).^2,3);
        % for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end
        aa=ersp2(:,:,trials_numrdnt2==rednumber);
        erspsPFC_redundant(:,:,fila)=mean(aa(:,:,:),3);

        ERP_V1_redundant(:,fila)=mean(erp1(trials_numrdnt2==rednumber,:),1);
        ERP_PFC_redundant(:,fila)=mean(erp2(trials_numrdnt2==rednumber,:),1);


        tr=sum(trials_numrdnt2==rednumber);
        tfs=tfs1(:,:,trials_numrdnt2==rednumber);
        itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
        itcsV1_redundant(:,:,fila)=itc;

        tfs=tfs2(:,:,trials_numrdnt2==rednumber);
        itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
        itcsPFC_redundant(:,:,fila)=itc;

        for deviant_coher=1;
            tfs1a=tfs1(:,:,trials1>1); %trials2 for non-limited.
            tfs2a=tfs2(:,:,trials1>1); %trials2 for non-limited.

            clear coher
            for calculatecoherencebetweens=1;

                for f=1:size(f_sG1,2);
                    for t=1:100;
                        lags=0; cnt=0;
                        for tr=1:size(tfs1a,3);
                            l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                            l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                            lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                            cnt=cnt+1;
                        end
                        coher(f,t)=abs(lags/cnt);%-sqrt(-(1/cnt)*log(.5));
                    end
                end
            end
            cohers_deviant(:,:,fila)=coher;%-cohersham;
            cohershams(:,:,fila)=cohersham;
            %basecorrect
            for f=1:size(cohers_deviant,1);
                %         bb=mean(cohers(f,basetime1:basetime2,fila));
                %         for t=1:size(cohers,2);
                %             cohers(f,t,fila)=cohers(f,t,fila)-bb;
                %         end
            end

            %coherence across all timepoints
            clear coher
            for calculatecoherencebetweens=1
                for f=1:size(f_sG1,2)
                    lags=0; cnt=0;
                    for t=basetime1:time3
                        for tr=1:size(tfs1a,3)
                            l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                            l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                            lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                            cnt=cnt+1;
                        end
                    end
                    coher(f)=abs(lags/cnt);%-sqrt(-(1/cnt)*log(.5));
                end
            end
            cohers_deviant_alltimes(:,fila)=coher;%-cohersham;

        end

        for redndt_coher=1;
            tfs1a=tfs1(:,:,trials_numrdnt2==rednumber);
            tfs2a=tfs2(:,:,trials_numrdnt2==rednumber);
            %tfs1a=tfs1(:,:,trials1==1);
            %tfs2a=tfs2(:,:,trials1==1);

            clear coher
            for calculatecoherencebetweens=1;

                for f=1:size(f_sG1,2);
                    for t=1:100;
                        lags=0; cnt=0;
                        for tr=1:size(tfs1a,3);
                            l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                            l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                            lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                            cnt=cnt+1;
                        end
                        coher(f,t)=abs(lags/cnt);%-sqrt(-(1/cnt)*log(.5));
                    end
                end
            end
            cohers_redundant(:,:,fila)=coher;%-cohersham;
            cohershams(:,:,fila)=cohersham;
            %basecorrect
            for f=1:size(cohers_redundant,1);
                %         bb=mean(cohers(f,basetime1:basetime2,fila));
                %         for t=1:size(cohers,2);
                %             cohers(f,t,fila)=cohers(f,t,fila)-bb;
                %         end
            end

            %coherence across all timepoints
            clear coher
            for calculatecoherencebetweens=1
                for f=1:size(f_sG1,2)
                    lags=0; cnt=0;
                    for t=basetime1:time3
                        for tr=1:size(tfs1a,3)
                            l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                            l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                            lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                            cnt=cnt+1;
                        end
                    end
                    coher(f)=abs(lags/cnt);%-sqrt(-(1/cnt)*log(.5));
                end
            end
            cohers_redundant_alltimes(:,fila)=coher;%-cohersham;

        end
        trnums(1,fila)=sum(trials1==1);

        for OB_coher=1;
            tfs1a=tfs1(:,:,trials1<2);
            tfs2a=tfs2(:,:,trials1<2);

            clear coher
            for calculatecoherencebetweens=1;

                for f=1:size(f_sG1,2);
                    for t=1:100;
                        lags=0; cnt=0;
                        for tr=1:size(tfs1a,3);
                            l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                            l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                            lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                            cnt=cnt+1;
                        end
                        coher(f,t)=abs(lags/cnt);%-sqrt(-(1/cnt)*log(.5));
                    end
                end
            end
            cohers_OB(:,:,fila)=coher;%-cohersham;
            cohershams(:,:,fila)=cohersham;
            %basecorrect
            for f=1:size(cohers_redundant,1);
                %         bb=mean(cohers(f,basetime1:basetime2,fila));
                %         for t=1:size(cohers,2);
                %             cohers(f,t,fila)=cohers(f,t,fila)-bb;
                %         end
            end

            for calculatephaselags=1;
                freqq=7;
                lags=0; cnt=1;
                for t=basetime1:time3;
                    for tr=1:size(tfs1,3);
                        l1=tfs1(freqq,t,tr)./abs(tfs1(freqq,t,tr));
                        l2=tfs2(freqq,t,tr)./abs(tfs2(freqq,t,tr));
                        lags(cnt)=(1*exp(1i*((angle(l2)-angle(l1)))));
                        cnt=cnt+1;
                    end
                end
                lagalls=vertcat(lagalls,lags');
            end

        end


        tfs1=tfs1b(:,:,and(trials2>0,trials2<2));
        tfs2=tfs2b(:,:,and(trials2>0,trials2<2));

        for calculatecoherenceTIMElines=1;

            for f=1:size(f_sG1,2);
                for tr=1:size(tfs1,3);
                    cnt=0; lags=0;
                    for t=2:99;
                        l1=tfs1(f,t,tr)./abs(tfs1(f,t,tr));
                        l2=tfs2(f,t,tr)./abs(tfs2(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                    coherTIME_ob(f,tr,fila)=abs(lags/cnt);%-sqrt(-(1/cnt)*log(.5));
                end
                %coherTIME_ob(f,:,fila)=coherTIME_ob(f,:,fila)-mean(coherTIME_ob(f,1:5,fila));
                coherTIME_ob_smooth(f,:,fila)=smoothdata(coherTIME_ob(f,:,fila),10)';
                %coherTIME_ob_smooth(f,:,fila)=coherTIME_ob_smooth(f,:,fila)-mean(coherTIME_ob_smooth(f,1,fila));

            end
        end
    end

    powers{2,cond}=powers_oddball;
    coherences{2,cond}=cohers_redundant;
    coherences_alltime{2,cond}=cohers_redundant_alltimes;
    cohersTIME{2,cond}=coherTIME_ob_smooth;
    itcsV1{2,cond}=itcsV1_redundant;
    itcsPFC{2,cond}=itcsPFC_redundant;
    erspsV1{2,cond}=erspsV1_redundant;
    erspsPFC{2,cond}=erspsPFC_redundant;
    ERPsV1{2,cond}=ERP_V1_redundant;
    ERPsPFC{2,cond}=ERP_PFC_redundant;

    powers{3,cond}=powers_oddball;
    coherences{3,cond}=cohers_deviant;
    coherences_alltime{3,cond}=cohers_deviant_alltimes;
    coherences{5,cond}=cohers_OB;
    cohersTIME{3,cond}=coherTIME_ob_smooth;
    itcsV1{3,cond}=itcsV1_deviant;
    itcsPFC{3,cond}=itcsPFC_deviant;
    erspsV1{3,cond}=erspsV1_deviant;
    erspsPFC{3,cond}=erspsPFC_deviant;
    ERPsV1{3,cond}=ERP_V1_deviant;
    ERPsPFC{3,cond}=ERP_PFC_deviant;

    % pfcdrive{2,cond}=cohers_OB_pfcdrive;
    % v1drive{2,cond}=cohers_OB_V1drive;

end

condnames={'pre CNO';'post CNO'};
timeA=basetime0; %timeA=1;
timeB=time2; %timeB=size(t_sG1,2);
clear obs


%% all timepoints avg power
%% v1 power
figure;
for cond=1;
    tmp1=powers{1,1};
    tmp2=powers{1,2};
    tmp1=tmp1(:,:,1);
    tmp2=tmp2(:,:,1);

    for sub=1:size(tmp1,2);
        for f=1:size(tmp1,1);
            tmp1(f,sub)=log(tmp1(f,sub).^2);%*(f-1);
            tmp2(f,sub)=log(tmp2(f,sub).^2);%*(f-1);
        end
    end

    tmpX=(tmp1+tmp2)/2;
    for sub=1:size(tmp1,2);
        for f=1:size(tmp1,1);
            %tmp1(f,sub)=tmp1(f,sub)-(mean(mean(tmpX(1:50,sub))));
            % tmp2(f,sub)=tmp2(f,sub)-(mean(mean(tmpX(1:50,sub))));
            % tmp3(f,sub)=tmp3(f,sub)-(mean(mean(tmpX(1:50,sub))));
        end
    end

    ctMN=mean(tmp1,2);
    obMN=mean(tmp2,2);

    tmpX=(tmp1+tmp2)/2; %tmpX=0;
    ctST=std(tmp1-tmpX,0,2)/sqrt(size(tmp1,2));
    obST=std(tmp2-tmpX,0,2)/sqrt(size(tmp2,2));

    a=timeA;
    b=timeB;

    shadedErrorBar(0:4:156,mean((ctMN(:,1)),2),mean((ctST(:,1)),2),'lineProps','k');  xlim([1 120]);hold on
    shadedErrorBar(0:4:156,mean((obMN(:,1)),2),mean((obST(:,1)),2),'lineProps','b'); xlim([1 120]);hold on

    title(strcat(condnames{cond},': V1 power: all timempoints'));
    xlabel('frequency (Hz)')
    yy(cond,:)=ylim;
    yyy(1)=min(yy(:)); yyy(2)=max(yy(:));
end
for t=1:40;
    [h,p,ci,stats]=ttest(tmp1(t,:),tmp2(t,:));
    pp(t)=p;
end
figure; plot(0:4:156,pp); title('ttest for v1 power');
%% pfc power
figure;
for cond=1;
    tmp1=powers{1,1};
    tmp2=powers{1,2};
    tmp1=tmp1(:,:,2);
    tmp2=tmp2(:,:,2);

    for sub=1:size(tmp1,2);
        for f=1:size(tmp1,1);
            tmp1(f,sub)=log(tmp1(f,sub).^2);%*(f-1);
            tmp2(f,sub)=log(tmp2(f,sub).^2);%*(f-1);
        end
    end

    tmpX=(tmp1+tmp2)/3;
    for sub=1:size(tmp1,2);
        for f=1:size(tmp1,1);
            %  tmp1(f,sub)=tmp1(f,sub)-(mean(mean(tmpX(1:50,sub))));
            %  tmp2(f,sub)=tmp2(f,sub)-(mean(mean(tmpX(1:50,sub))));
            % tmp3(f,sub)=tmp3(f,sub)-(mean(mean(tmpX(1:50,sub))));
        end
    end

    ctMN=mean(tmp1,2);
    obMN=mean(tmp2,2);

    tmpX=(tmp1+tmp2)/2; tmpX=0;
    ctST=std(tmp1-tmpX,0,2)/sqrt(size(tmp1,2));
    obST=std(tmp2-tmpX,0,2)/sqrt(size(tmp2,2));

    a=timeA;
    b=timeB;

    shadedErrorBar(0:4:156,mean((ctMN(:,1)),2),mean((ctST(:,1)),2),'lineProps','k');  xlim([1 120]);hold on
    shadedErrorBar(0:4:156,mean((obMN(:,1)),2),mean((obST(:,1)),2),'lineProps','b'); xlim([1 120]);hold on

    title(strcat(condnames{cond},': PFC power: all timempoints'));
    xlabel('frequency (Hz)')
    yy(cond,:)=ylim;
    yyy(1)=min(yy(:)); yyy(2)=max(yy(:));
end


%% all trials coherence
timeA=time1;
timeB=time3; clear obs
timeAp=basetime0;
timeBp=basetime2;

for cond=1:2

    for coherences_across_all_timepoints_per_condition=1;
        cohers_control_alltimes=coherences_alltime{1,cond};cohers_redundant_alltimes=coherences_alltime{2,cond};cohers_deviant_alltimes=coherences_alltime{3,cond};
        ctMN=mean(cohers_control_alltimes,2);
        rdMN=mean(cohers_redundant_alltimes,2);
        dvMN=mean(cohers_deviant_alltimes,2);
        %    bbMN=mean(cohers_nostim_alltimes,2);

        tmp1=cohers_control_alltimes;
        tmp2=cohers_redundant_alltimes;
        tmp3=cohers_deviant_alltimes;
        %  bb1=cohers_nostim_alltimes;

        tmpX=(tmp1+tmp2+tmp3)/3;
        ctST=std(tmp1-tmpX,0,2)/sqrt(size(tmp1,2));
        rdST=std(tmp2-tmpX,0,2)/sqrt(size(tmp2,2));
        dvST=std(tmp3-tmpX,0,2)/sqrt(size(tmp3,2));
        %  bbST=std(bb1-tmpX,0,2)/sqrt(size(bb1,2));

        figure;
        shadedErrorBar(f_sG1,dvMN,dvST,'lineProps','r'); xlim([1 55]);hold on
        shadedErrorBar(f_sG1,ctMN,ctST,'lineProps','k');  xlim([1 55]);hold on
        shadedErrorBar(f_sG1,rdMN,rdST,'lineProps','b');  xlim([1 55]);hold on
        %  shadedErrorBar(f_sG1,bbMN,bbST,'lineProps','g');  xlim([1 55]);hold on
        title('V1 phase-locking to PFC: all timepoints, trial types');
        xlabel('frequency (Hz)')
        yy(1,:)=ylim;
        yyy(1)=min(yy(:)); yyy(2)=max(yy(:));
        legend('deviant','control','redundant')
    end
end

%% stim evoked coherence, unseparated trials
clear obs
figure;
for cond=1:2;
    tmp1=coherences{4,cond};
    tmp2=coherences{5,cond};
    ctMN=mean(tmp1(:,:,:),3);
    obMN=mean(tmp2(:,:,:),3);

    tmpX=(tmp1+tmp2)/2;
    ctST=std(tmp1-tmpX,0,3)/sqrt(size(tmp1,3));
    obST=std(tmp2-tmpX,0,3)/sqrt(size(tmp2,3));

    a=basetime1;
    b=time3;
    subplot(1,2,cond);shadedErrorBar(f_sG1,mean((obMN(:,a:b)),2),mean((obST(:,a:b)),2),'lineProps','r'); xlim([1 55]);hold on
    subplot(1,2,cond);shadedErrorBar(f_sG1,mean((ctMN(:,a:b)),2),mean((ctST(:,a:b)),2),'lineProps','k');  xlim([1 55]);
    title(strcat(condnames{cond},': V1 phase-locking to PFC: run vs run'));
    xlabel('frequency (Hz)')
    yy(cond,:)=ylim;
    yyy(1)=min(yy(:)); yyy(2)=max(yy(:));
    obs(:,1,cond)=mean((obMN(:,a:b)+ctMN(:,a:b)),2)/2;obs(:,2,cond)=mean((obST(:,a:b)+ctST(:,a:b)),2)/2;
end
for cond=1:2;
    subplot(1,2,cond); ylim(yyy);
end
close
%figure; shadedErrorBar(f_sG1,(mean((obMN(:,a:b)),2)+mean((ctMN(:,a:b)),2))/2,mean((ctST(:,a:b)),2),'lineProps','k'); axis xy; title('avg PFC-V1 coherence');  xlim([  1 90]);
figure; shadedErrorBar(f_sG1,obs(:,1,1)',obs(:,2,1)','lineProps','k'); xlim([1 55]);hold on
shadedErrorBar(f_sG1,obs(:,1,2)',obs(:,2,2)','lineProps','b'); xlim([1 55]);hold on
title('Pre vs post CNO during oddball');

%effect=(mean(obs(5:9,1,1))-mean(obs(5:9,1,2)))./(mean(obs(5:9,2,1))*sqrt(size(tmp2,3)))

for stimlocked_coherences_between_v1_and_pfc=1; %unsorted triials
    tmp1=coherences{4,1};
    tmp2=coherences{5,1};

    tmp5=coherences{4,2};
    tmp6=coherences{5,2};
    grpSig=mean(tmp1,3)-mean(tmp1,3);
    intSig=mean(tmp1,3)-mean(tmp1,3);
    for f=1:50;
        for t=1:100;
            a=squeeze(tmp1(f,t,:));b=squeeze(tmp2(f,t,:));
            d=squeeze(tmp5(f,t,:));e=squeeze(tmp6(f,t,:));
            [p,table] = anova_rm({horzcat(a,b) horzcat(d,e)},'off');
            grpSig(f,t)=1-table{3,6};
            intSig(f,t)=1-table{4,6};
        end
    end
    figure;

    subplot(2,2,1); imagesc(t_sG1,f_sG1,mean(tmp1,3)); axis xy; title('control'); colormap jet;  axis([-300 700  1 55]);cc=caxis; ylabel('pre CNO'); cc(1)=.1;caxis(cc);
    subplot(2,2,2);imagesc(t_sG1,f_sG1,mean(tmp2,3)); axis xy; title('redundant'); colormap jet;   axis([-300 700  1 55]);caxis(cc);
    subplot(2,2,3); imagesc(t_sG1,f_sG1,mean(tmp5,3)); axis xy; title('control'); colormap jet;  axis([-300 700 1 55]);caxis(cc); ylabel('post CNO');
    subplot(2,2,4);imagesc(t_sG1,f_sG1,mean(tmp6,3)); axis xy; title('redundant'); colormap jet;   axis([-300 700  1 55]);caxis(cc);
    figure;
    map = [0 0 0;    1 1 1];
    subplot(1,2,1);imagesc(t_sG1,f_sG1,grpSig); axis xy; title('pvalues-group effect'); colormap(map); caxis([.985 1]); axis([-300 700  1 55]);

    subplot(1,2,2);imagesc(t_sG1,f_sG1,intSig); axis xy; title('pvalues-groupBYstimtype interaction'); colormap(map); caxis([.985 1]); axis([-300 700  1 55]);

    % figure; imagesc(t_sG1,f_sG1,(mean(tmp5,3)+mean(tmp6,3)+mean(tmp5,3)+mean(tmp6,3))/4); axis xy; title('stimulus locked coherence'); colormap jet;   axis([-300 700  1 90]);caxis(cc);
end

%% single electrode analyses
for phase_locking_to_stimV1=1;
    tmp1=itcsV1{1,1};
    tmp2=itcsV1{2,1};
    tmp3=itcsV1{3,1};
    %tmp4=itcsV1{4,1};

    tmp5=itcsV1{1,2};
    tmp6=itcsV1{2,2};
    tmp7=itcsV1{3,2};
    grpSig=mean(tmp1,3)-mean(tmp1,3);
    intSig=mean(tmp1,3)-mean(tmp1,3);
    for f=1:50;
        for t=basetime1:time3;
            a=squeeze(tmp1(f,t,:));b=squeeze(tmp2(f,t,:));c=squeeze(tmp3(f,t,:));
            d=squeeze(tmp5(f,t,:));e=squeeze(tmp6(f,t,:));g=squeeze(tmp7(f,t,:));
            [p,table] = anova_rm({horzcat(a,b,c) horzcat(d,e,g)},'off');
            grpSig(f,t)=1-table{3,6};
            intSig(f,t)=1-table{4,6};
        end
    end
    figure;

    subplot(2,3,1); imagesc(t_sG1,f_sG1,mean(tmp1,3)); axis xy; title('control'); colormap jet;  axis([-50 550 1 55]);cc=caxis; ylabel('pre CNO'); cc(1)=.02;caxis(cc);
    subplot(2,3,2);imagesc(t_sG1,f_sG1,mean(tmp2,3)); axis xy; title('redundant'); colormap jet;   axis([-50 550 1 55]);caxis(cc);
    subplot(2,3,3);imagesc(t_sG1,f_sG1,mean(tmp3,3)); axis xy; title('deviant'); colormap jet;  axis([-50 550 1 55]);caxis(cc);
    subplot(2,3,4); imagesc(t_sG1,f_sG1,mean(tmp5,3)); axis xy; title('control'); colormap jet;  axis([-50 550 1 55]);caxis(cc); ylabel('post CNO');
    subplot(2,3,5);imagesc(t_sG1,f_sG1,mean(tmp6,3)); axis xy; title('redundant'); colormap jet;   axis([-50 550 1 55]);caxis(cc);
    subplot(2,3,6);imagesc(t_sG1,f_sG1,mean(tmp7,3)); axis xy; title('deviant'); colormap jet;  axis([-50 550 1 55]);caxis(cc);
    figure;
    map = [0 0 0;    1 1 1];
    subplot(1,2,1);imagesc(t_sG1,f_sG1,grpSig); axis xy; title('pvalues-group effect'); colormap(map); caxis([.985 1]); axis([-50 550 1 55]);

    subplot(1,2,2);imagesc(t_sG1,f_sG1,intSig); axis xy; title('pvalues-groupBYstimtype interaction'); colormap(map); caxis([.985 1]); axis([-50 550 1 55]);
end

for inducedphaselockin_to_stimV1_for_given_freq=1;
    b1=1; b2=11; %frequencies for analysis
    % b1=20; b2=40;
    tmp1=itcsV1{1,1};
    tmp2=itcsV1{2,1};
    tmp3=itcsV1{3,1};
    %tmp4=itcsV1{4,1};

    grpSig=zeros(1,100);
    cpre=zeros(size(tmp1,3),100); rpre=zeros(size(tmp1,3),100); dpre=zeros(size(tmp1,3),100);
    for f=1;
        for t=basetime1:92;
            a=squeeze(mean(tmp1(b1:b2,t,:),1));b=squeeze(mean(tmp2(b1:b2,t,:),1));c=squeeze(mean(tmp3(b1:b2,t,:),1));
            pm1=mean(mean(mean(tmp1(b1:b2,basetime1:basetime2,:))));
            pm2=mean(mean(mean(tmp2(b1:b2,basetime1:basetime2,:))));
            pm3=mean(mean(mean(tmp3(b1:b2,basetime1:basetime2,:))));
            [p,table] = anova_rm({horzcat(a,b,c)},'off');
            grpSig(f,t)=table{2,6};
            cpre(:,t)=a-pm1; rpre(:,t)=b-pm2; dpre(:,t)=c-pm3;
        end
    end
    premean=(cpre+rpre+dpre)/2;
    figure;

    shadedErrorBar(t_sG1,mean(cpre,1),std((cpre-premean),0,1)/sqrt(size(cpre,1)),'lineProps','k'); hold on
    shadedErrorBar(t_sG1,mean(rpre,1),std((rpre-premean),0,1)/sqrt(size(rpre,1)),'lineProps','b'); hold on
    shadedErrorBar(t_sG1,mean(dpre,1),std((dpre-premean),0,1)/sqrt(size(dpre,1)),'lineProps','r');xlim([-100 600]);

    figure; plot(t_sG1,grpSig); title('pvalues');xlim([-100 600]);

    t1=time1; t2=time2;
    a=squeeze(mean(mean(tmp1(b1:b2,t1:t2,:),1),2));b=squeeze(mean(mean(tmp2(b1:b2,t1:t2,:),1),2));c=squeeze(mean(mean(tmp3(b1:b2,t1:t2,:),1),2));
    pm1=mean(mean(mean(tmp1(b1:b2,basetime1:basetime2,:))));
    pm2=mean(mean(mean(tmp2(b1:b2,basetime1:basetime2,:))));
    pm3=mean(mean(mean(tmp3(b1:b2,basetime1:basetime2,:))));
    [p,table] = anova_rm({horzcat(a-pm1,b-pm2,c-pm3)},'on');
    grpSig(f,t)=table{2,6};
    cpre(:,t)=a-pm1; rpre(:,t)=b-pm2; dpre(:,t)=c-pm3;
    a=a-pm1; b=b-pm2; c=c-pm3; x=(a+b+c)/3;
    errorbar_groups([mean(a) mean(b) mean(c)]',[std(a-x)/sqrt(size(a,1)) std(b-x)/sqrt(size(b,1)) std(c-x)/sqrt(size(c,1))]', 'bar_colors',[ .5 .5 .5; .5 .5 1; 1 0 0],'bar_names',{'control';'redundants';'deviants';}); title('ITC'); hold on
    tmpa=vertcat(a,b,c);
    tmpb=tmpa-tmpa; sz=size(a,1); tmpb=horzcat(zeros(1,sz)+1.1,zeros(1,sz)+2,zeros(1,sz)+2.9);
    scatter(tmpb,tmpa,'fill');
end

for inducedAMPLTUDE_to_stimV1=1;
    tmp1=erspsV1{1,1};
    tmp2=erspsV1{2,1};
    tmp3=erspsV1{3,1};
    %tmp4=itcsV1{4,1};

    tmp5=erspsV1{1,2};
    tmp6=erspsV1{2,2};
    tmp7=erspsV1{3,2};

    for baseadjust=1;
        %         for f=1:size(tmp1,1);
        %                 for s=1:size(tmp1,3);
        %                     a=mean(tmp1(f,basetime1:basetime2,s));
        %                     b=mean(tmp2(f,basetime1:basetime2,s));
        %                     c=mean(tmp3(f,basetime1:basetime2,s));
        %                     d=mean(tmp5(f,basetime1:basetime2,s));
        %                     e=mean(tmp6(f,basetime1:basetime2,s));
        %                     g=mean(tmp7(f,basetime1:basetime2,s));
        %                     for t=1:size(tmp1,2);
        %                         tmp1(f,t,s)=tmp1(f,t,s)-a;
        %                         tmp2(f,t,s)=tmp2(f,t,s)-b;
        %                         tmp3(f,t,s)=tmp3(f,t,s)-c;
        %                         tmp5(f,t,s)=tmp5(f,t,s)-d;
        %                         tmp6(f,t,s)=tmp6(f,t,s)-e;
        %                         tmp7(f,t,s)=tmp7(f,t,s)-g;
        %                     end
        %                 end
        %         end
    end
    grpSig=mean(tmp1,3)-mean(tmp1,3);
    intSig=mean(tmp1,3)-mean(tmp1,3);
    for f=1:50;
        for t=basetime1:time3;
            a=squeeze(tmp1(f,t,:));b=squeeze(tmp2(f,t,:));c=squeeze(tmp3(f,t,:));
            d=squeeze(tmp5(f,t,:));e=squeeze(tmp6(f,t,:));g=squeeze(tmp7(f,t,:));
            [p,table] = anova_rm({horzcat(a,b,c) horzcat(d,e,g)},'off');
            grpSig(f,t)=1-table{3,6};
            intSig(f,t)=1-table{4,6};
        end
    end
    figure;

    subplot(2,3,1); imagesc(t_sG1,f_sG1,mean(tmp1,3)); axis xy; title('control'); colormap jet;  axis([-100 730 1 55]);cc=caxis; caxis(cc);ylabel('pre CNO');
    subplot(2,3,2);imagesc(t_sG1,f_sG1,mean(tmp2,3)); axis xy; title('redundant'); colormap jet;   axis([-100 730 1 55]);caxis(cc);
    subplot(2,3,3);imagesc(t_sG1,f_sG1,mean(tmp3,3)); axis xy; title('deviant'); colormap jet;  axis([-100 730 1 55]);caxis(cc);
    subplot(2,3,4); imagesc(t_sG1,f_sG1,mean(tmp5,3)); axis xy; title('control'); colormap jet;  axis([-100 730 1 55]);caxis(cc); ylabel('post CNO');
    subplot(2,3,5);imagesc(t_sG1,f_sG1,mean(tmp6,3)); axis xy; title('redundant'); colormap jet;   axis([-100 730 1 55]);caxis(cc);
    subplot(2,3,6);imagesc(t_sG1,f_sG1,mean(tmp7,3)); axis xy; title('deviant'); colormap jet;  axis([-100 730 1 55]);caxis(cc);
    figure;
    map = [0 0 0;    1 1 1];
    subplot(1,2,1);imagesc(t_sG1,f_sG1,grpSig); axis xy; title('pvalues-group effect'); colormap(map); caxis([.95 1]); axis([-50 650 1 55]);

    subplot(1,2,2);imagesc(t_sG1,f_sG1,intSig); axis xy; title('pvalues-groupBYstimtype interaction'); colormap(map); caxis([.95 1]); axis([-50 650 1 55]);
end

for inducedAMPLTUDE_to_stimV1_for_given_freq=1;
    b1=3; b2=11; %frequencies for analysis
    % b1=20; b2=40;
    tmp1=erspsV1{1,1};
    tmp2=erspsV1{2,1};
    tmp3=erspsV1{3,1};
    %tmp4=itcsV1{4,1};

    tmp5=erspsV1{1,2};
    tmp6=erspsV1{2,2};
    tmp7=erspsV1{3,2};


    for baseadjust=1;
        for f=1:size(tmp1,1);
            for s=1:size(tmp1,3);
                a=mean(tmp1(f,basetime2:time1,s));
                b=mean(tmp2(f,basetime2:time1,s));
                c=mean(tmp3(f,basetime2:time1,s));
                d=mean(tmp5(f,basetime2:time1,s));
                e=mean(tmp6(f,basetime2:time1,s));
                g=mean(tmp7(f,basetime2:time1,s));
                for t=1:size(tmp1,2);
                    tmp1(f,t,s)=tmp1(f,t,s)-a;
                    tmp2(f,t,s)=tmp2(f,t,s)-b;
                    tmp3(f,t,s)=tmp3(f,t,s)-c;
                    tmp5(f,t,s)=tmp5(f,t,s)-d;
                    tmp6(f,t,s)=tmp6(f,t,s)-e;
                    tmp7(f,t,s)=tmp7(f,t,s)-g;
                end
            end
        end
    end
    grpSig=zeros(1,100);
    intSig=zeros(1,100);
    cpre=zeros(size(tmp1,3),100); rpre=zeros(size(tmp1,3),100); dpre=zeros(size(tmp1,3),100);
    cpost=zeros(size(tmp1,3),100); rpost=zeros(size(tmp1,3),100); dpost=zeros(size(tmp1,3),100);
    for f=1;
        for t=basetime1:92;
            a=squeeze(mean(tmp1(b1:b2,t,:),1));b=squeeze(mean(tmp2(b1:b2,t,:),1));c=squeeze(mean(tmp3(b1:b2,t,:),1));
            d=squeeze(mean(tmp5(b1:b2,t,:),1));e=squeeze(mean(tmp6(b1:b2,t,:),1));g=squeeze(mean(tmp7(b1:b2,t,:),1));
            pm=(mean(mean(mean(tmp1(b1:b2,basetime1:basetime2,:))))+mean(mean(mean(tmp2(b1:b2,basetime1:basetime2,:))))+mean(mean(mean(tmp3(b1:b2,basetime1:basetime2,:)))))/3;
            ptm=(mean(mean(mean(tmp5(b1:b2,basetime1:basetime2,:))))+mean(mean(mean(tmp6(b1:b2,basetime1:basetime2,:))))+mean(mean(mean(tmp7(b1:b2,basetime1:basetime2,:)))))/3;
            [p,table] = anova_rm({horzcat(a,b,c) horzcat(d,e,g)},'off');
            grpSig(f,t)=1-table{3,6};
            intSig(f,t)=1-table{4,6};
            cpre(:,t)=a-pm; rpre(:,t)=b-pm; dpre(:,t)=c-pm;  cpost(:,t)=d-ptm; rpost(:,t)=e-ptm; dpost(:,t)=g-ptm;
        end
    end
    premean=(cpre+rpre+dpre)/2;postmean=(cpost+rpost+dpost)/2;
    figure;

    subplot(1,2,1); shadedErrorBar(t_sG1,mean(cpre,1),std((cpre-premean),0,1)/sqrt(size(cpre,1)),'lineProps','k'); hold on
    subplot(1,2,1);shadedErrorBar(t_sG1,mean(rpre,1),std((rpre-premean),0,1)/sqrt(size(rpre,1)),'lineProps','b'); hold on
    subplot(1,2,1);shadedErrorBar(t_sG1,mean(dpre,1),std((dpre-premean),0,1)/sqrt(size(dpre,1)),'lineProps','r');xlim([-100 600]);
    yy(1,:)=ylim;
    yyy(1)=min(yy(:)); yyy(2)=max(yy(:));
    subplot(1,2,2); shadedErrorBar(t_sG1,mean(cpost,1),std((cpost-postmean),0,1)/sqrt(size(cpost,1)),'lineProps','k'); hold on
    subplot(1,2,2);shadedErrorBar(t_sG1,mean(rpost,1),std((rpost-postmean),0,1)/sqrt(size(rpost,1)),'lineProps','b'); hold on
    subplot(1,2,2);shadedErrorBar(t_sG1,mean(dpost,1),std((dpost-postmean),0,1)/sqrt(size(dpost,1)),'lineProps','r');
    yy(2,:)=ylim;
    yyy(1)=min(yy(:)); yyy(2)=max(yy(:));
    xlim([-100 600]);
end
for cond=1:2;
    subplot(1,2,cond); ylim(yyy);
end
time1a=time1+2; time2a=time1+12;
a=mean(dpre(:,time1a:time2a)-cpre(:,time1a:time2a),2);
b=mean(dpost(:,time1a:time2a)-cpost(:,time1a:time2a),2);
effect=(mean(a)-mean(b))./std((a-b)/2)
anova_rm({horzcat(mean(dpre(:,time1a:time2a),2),mean(rpre(:,time1a:time2a),2),mean(cpre(:,time1a:time2a),2)) horzcat(mean(dpost(:,time1a:time2a),2),mean(rpost(:,time1a:time2a),2),mean(cpost(:,time1a:time2a),2))})
dat_pre_post_cno_cnt=horzcat(mean(dpre(:,time1a:time2a),2),mean(rpre(:,time1a:time2a),2),mean(cpre(:,time1a:time2a),2),mean(dpost(:,time1a:time2a),2),mean(rpost(:,time1a:time2a),2),mean(cpost(:,time1a:time2a),2));
%  anova_rm(horzcat(a,b))
%  anova_rm(horzcat(datpre,datpost))





for inducedAMPLTUDE_to_stimV1_for_given_freq=1;
    load erspsV1_exp
    b1=3; b2=11; %frequencies for analysis
    % b1=20; b2=40;
    tmp1=erspsV1_exp{1,1};
    tmp2=erspsV1_exp{2,1};
    tmp3=erspsV1_exp{3,1};
    %tmp4=itcsV1{4,1};

    tmp5=erspsV1_exp{1,2};
    tmp6=erspsV1_exp{2,2};
    tmp7=erspsV1_exp{3,2};


    for baseadjust=1;
        for f=1:size(tmp1,1);
            for s=1:size(tmp1,3);
                a=mean(tmp1(f,basetime2:time1,s));
                b=mean(tmp2(f,basetime2:time1,s));
                c=mean(tmp3(f,basetime2:time1,s));
                d=mean(tmp5(f,basetime2:time1,s));
                e=mean(tmp6(f,basetime2:time1,s));
                g=mean(tmp7(f,basetime2:time1,s));
                for t=1:size(tmp1,2);
                    tmp1(f,t,s)=tmp1(f,t,s)-a;
                    tmp2(f,t,s)=tmp2(f,t,s)-b;
                    tmp3(f,t,s)=tmp3(f,t,s)-c;
                    tmp5(f,t,s)=tmp5(f,t,s)-d;
                    tmp6(f,t,s)=tmp6(f,t,s)-e;
                    tmp7(f,t,s)=tmp7(f,t,s)-g;
                end
            end
        end
    end
    grpSig=zeros(1,100);
    intSig=zeros(1,100);
    cpre=zeros(size(tmp1,3),100); rpre=zeros(size(tmp1,3),100); dpre=zeros(size(tmp1,3),100);
    cpost=zeros(size(tmp1,3),100); rpost=zeros(size(tmp1,3),100); dpost=zeros(size(tmp1,3),100);
    for f=1;
        for t=basetime1:92;
            a=squeeze(mean(tmp1(b1:b2,t,:),1));b=squeeze(mean(tmp2(b1:b2,t,:),1));c=squeeze(mean(tmp3(b1:b2,t,:),1));
            d=squeeze(mean(tmp5(b1:b2,t,:),1));e=squeeze(mean(tmp6(b1:b2,t,:),1));g=squeeze(mean(tmp7(b1:b2,t,:),1));
            pm=(mean(mean(mean(tmp1(b1:b2,basetime1:basetime2,:))))+mean(mean(mean(tmp2(b1:b2,basetime1:basetime2,:))))+mean(mean(mean(tmp3(b1:b2,basetime1:basetime2,:)))))/3;
            ptm=(mean(mean(mean(tmp5(b1:b2,basetime1:basetime2,:))))+mean(mean(mean(tmp6(b1:b2,basetime1:basetime2,:))))+mean(mean(mean(tmp7(b1:b2,basetime1:basetime2,:)))))/3;
            [p,table] = anova_rm({horzcat(a,b,c) horzcat(d,e,g)},'off');
            grpSig(f,t)=1-table{3,6};
            intSig(f,t)=1-table{4,6};
            cpre(:,t)=a-pm; rpre(:,t)=b-pm; dpre(:,t)=c-pm;  cpost(:,t)=d-ptm; rpost(:,t)=e-ptm; dpost(:,t)=g-ptm;
        end
    end
    premean=(cpre+rpre+dpre)/2;postmean=(cpost+rpost+dpost)/2;
    figure;

    subplot(1,2,1); shadedErrorBar(t_sG1,mean(cpre,1),std((cpre-premean),0,1)/sqrt(size(cpre,1)),'lineProps','k'); hold on
    subplot(1,2,1);shadedErrorBar(t_sG1,mean(rpre,1),std((rpre-premean),0,1)/sqrt(size(rpre,1)),'lineProps','b'); hold on
    subplot(1,2,1);shadedErrorBar(t_sG1,mean(dpre,1),std((dpre-premean),0,1)/sqrt(size(dpre,1)),'lineProps','r');xlim([-100 600]);
    yy(1,:)=ylim;
    yyy(1)=min(yy(:)); yyy(2)=max(yy(:));
    subplot(1,2,2); shadedErrorBar(t_sG1,mean(cpost,1),std((cpost-postmean),0,1)/sqrt(size(cpost,1)),'lineProps','k'); hold on
    subplot(1,2,2);shadedErrorBar(t_sG1,mean(rpost,1),std((rpost-postmean),0,1)/sqrt(size(rpost,1)),'lineProps','b'); hold on
    subplot(1,2,2);shadedErrorBar(t_sG1,mean(dpost,1),std((dpost-postmean),0,1)/sqrt(size(dpost,1)),'lineProps','r');
    yy(2,:)=ylim;
    yyy(1)=min(yy(:)); yyy(2)=max(yy(:));
    xlim([-100 600]);
end
for cond=1:2;
    subplot(1,2,cond); ylim(yyy);
end
time1a=time1+2; time2a=time1+12;
a=mean(dpre(:,time1a:time2a)-cpre(:,time1a:time2a),2);
b=mean(dpost(:,time1a:time2a)-cpost(:,time1a:time2a),2);
effect=(mean(a)-mean(b))./std((a-b)/2)
anova_rm({horzcat(mean(dpre(:,time1a:time2a),2),mean(rpre(:,time1a:time2a),2),mean(cpre(:,time1a:time2a),2)) horzcat(mean(dpost(:,time1a:time2a),2),mean(rpost(:,time1a:time2a),2),mean(cpost(:,time1a:time2a),2))})
dat_pre_post_cno_exp=horzcat(mean(dpre(:,time1a:time2a),2),mean(rpre(:,time1a:time2a),2),mean(cpre(:,time1a:time2a),2),mean(dpost(:,time1a:time2a),2),mean(rpost(:,time1a:time2a),2),mean(cpost(:,time1a:time2a),2));
%  anova_rm(horzcat(a,b))
%  anova_rm(horzcat(datpre,datpost))


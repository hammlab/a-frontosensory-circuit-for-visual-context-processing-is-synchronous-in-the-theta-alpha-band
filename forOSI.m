clcl

files={''};%


limtrials=5;
converts=[2 3 5 4 1 8 6 7]; % this is 0degrees to 155 in steps
%converts=[1 2 3 4 5 6 7 8];%unalgered angels
alt_osi=0; %classic compuation. not as good. default is 1-circular variance


for file=1:size(files,1);
   
        load(strcat('CA_workspace_SORTED_',files{file}),'dfofanalyse','stimvistypeMAIN','phases');
  
    dat=dfofanalyse(:,phases==1); 
    stim=stimvistypeMAIN(:,phases==1);
    dat=dat(:,1200:end);
     stim=stim(:,1200:end);
     for dumbfixa=1; %fixes annoying blips that masquerade as trials
        stim1=stim;
        for t=3:size(stim,2);
    
            if stim1(1,t)>0;
                if and(stim1(1,t-2)==0,stim1(1,t+2)==0)
                    stim(1,t-1:t+1)=zeros(1,3); disp(t)
                end
            end
        end
    end
   
    clear trialdata trialstim stimtype osi obest Odiffstim
    tr=1;
    for t=15:size(stim,2)-28;
        if (stim(1,t)-stim(1,t-1))>0
            for c=1:size(dat,1);
                tmp=dat(c,t-14:t+28);%-mean(dat(c,t-10:t-4));
                tmp=tmp.*(tmp>0);
                trialdata(c,:,tr)=tmp;
                trialstim(tr)=max(stim(1,t:t+10));
            end
            tr=1+tr;
        end
    end
    if size(unique(trialstim))~=8;
        disp('whoops')
        disp(file)
    else
        aa=unique(trialstim);
        if and(sum(aa==1)==0,sum(aa==2)==0);
            for tr=1:size(trialstim,2);
                if trialstim(tr)==20;
                    trialstim(tr)=1;
                end
                if trialstim(tr)==21;
                    trialstim(tr)=2;
                end
                stimtype=[5 1]; %in rearranged angles
                
            %    stimtype=[1 2]; %in unaltered angels
            end
        end
        if and(sum(aa==3)==0,sum(aa==4)==0);
            for tr=1:size(trialstim,2);
                if trialstim(tr)==20;
                    trialstim(tr)=3;
                end
                if trialstim(tr)==21;
                    trialstim(tr)=4;
                end
                stimtype=[2 4]; %in rearranged angles
            %    stimtype=[3 4]; %in unaltered angels
            end
        end
        if and(sum(aa==5)==0,sum(aa==6)==0);
            for tr=1:size(trialstim,2);
                if trialstim(tr)==20;
                    trialstim(tr)=5;
                end
                if trialstim(tr)==21;
                    trialstim(tr)=6;
                end
                stimtype=[3 7];  %in rearranged angles
             %   stimtype=[5 6]; %in unaltered angels
            end
            
                disp(strcat('weridstim for',num2str(file)));
                weirdstims(file,1)=1;
        end
         if and(sum(aa==1)==0,sum(aa==3)==0);
            for tr=1:size(trialstim,2);
                if trialstim(tr)==20;
                    trialstim(tr)=1;
                end
                if trialstim(tr)==21;
                    trialstim(tr)=3;
                end
                stimtype=[5 2]; %in rearranged angles
              %  stimtype=[1 3]; %in unaltered angels
            end
        end
                     disp(file)
                     disp(unique(trialstim))
                     clear aaa
                     for c=1:8;
                         aaa(c)=sum(trialstim==c);
                     end
                     disp(aaa)
    end
    trialstimold=trialstim;
    for tr=1:size(trialstim,2); %for renaming the trial numbers
        k=converts==trialstimold(tr); tra=0;
        for ka=1:8;
            if k(ka)==1;
                tra=ka;
            end
        end
        trialstim(tr)=tra;
    end
    
    kb2=0;
    for c=1:size(trialdata,1);
        stimvec=zeros(1,8);
        trvec=zeros(1,8);
        for t=1:size(trialdata,3);
            if trvec(1,trialstim(1,t))<(limtrials+1);
            stimvec(1,trialstim(1,t))=mean(trialdata(c,14:24,t))+stimvec(1,trialstim(1,t));
            trvec(1,trialstim(1,t))=trvec(1,trialstim(1,t))+1;
            end
        end
        stimvec=stimvec./trvec;
        
        
        kb2=kb2+1;
        avrespall(kb2,:)=stimvec;
        angos8=0:pi/8:(pi);
        for stim=1:8;
            veca(1,stim)=stimvec(1,stim).*exp(2i*angos8(stim));
        end
        osi(kb2,1)=abs(sum(veca)./sum(stimvec));
        for alt_osi=alt_osi;
            if alt_osi==1;
            orthos1=[5 6 7 8 1 2 3 4];
            kq=max(abs(veca)); 
            for gp=1:size(veca,2);
                if abs(veca(1,gp))==kq;
                    mino=abs(veca(1,orthos1(gp)));
                end
            end
            osi(kb2,1)=(max(abs(veca))-mino)/max(abs(veca)); %alt osi
            end
        end
        obest(kb2,1)=angle(sum(veca)./sum(stimvec));
        angles=angle(veca);
    end %for calculating osi and pref ori
    for c=1:size(osi,1);
        for stim=1:2;
            pha=obest(c,1);
            Odiffstim(c,stim)=angle(exp(i*((pha-angles(stimtype(stim))))));
        end
    end
    save(strcat('orientation_selectivities_',files{file}),'osi','obest','Odiffstim');
end

clcl

files={''};
oris1=[1; 1; 3; 1; 1; 5; 3; 5; 5; 5; 1; 1; 1];
oris2=[2; 2; 4; 2; 2; 6; 4; 6; 6; 6; 2; 2; 2];
numtrials=10;
donorm=1; %if =0, analyse the raw beh variables.
redundantnumber=4; 
phzflp=[2; 1];
dobasecorr=0; 

for fil=1:size(files,1); 
    clearvars -except files fil donorm numtrials oris1 oris2 mouseavs_ctrl mouseavs_rdnt mouseavs_dev redundantnumber phzflp dobasecorr
    load(strcat('time_aligned_control',files{fil}));
    
    for doctrl=1;
        try load(strcat('time_aligned_control',files{fil},'cntrl_stimbegins'));
        catch
        figure; plot(resamp_stims_ctrl); title('select beginning and end');
        [x,y]=ginput(2); 
        x=round(x); if (x(1)<1); x(1)=1; end; if x(2)>size(resamp_stims_ctrl,2); x(2)=size(resamp_stims_ctrl,2); end;
        save(strcat('time_aligned_control',files{fil},'cntrl_stimbegins'),'x');
        end
        stima=resamp_stims_ctrl(1,x(1):x(2)); 
        beh_vars_norm_ctrl=beh_vars_norm(:,x(1):x(2));
        beh_vars_raw_ctrl=beh_vars_raw(:,x(1):x(2));
        if donorm==1; behtmp=beh_vars_norm_ctrl; else behtmp=beh_vars_raw_ctrl; end
        for c=1:size(behtmp,1);
            behtmp(c,:)=smoothdata(behtmp(c,:),'movmedian',5);
             %behtmp(c,:)=(behtmp(c,:))./max(behtmp(c,:)); 
        end

       if fil==12; 
           figure(fil); plot(behtmp(2:4,1000:2000)');
       end
        stima=round((stima./max(stima))*8);
        clear datcontrol
        tr1=0; tr2=0;
        for t=60:size(stima,2)-90; 
            if and(stima(t-1)==0,stima(t)>0);
                if stima(t+5)==oris1(fil)
                    tr1=1+tr1;
                    tmptmp=behtmp(:,t-59:t+89);
                    for c=1:size(tmptmp,1);
                        %tmptmp(c,:)=smoothdata(tmptmp(c,:),'movmedian',5);
                       if dobasecorr==1; tmptmp(c,:)=tmptmp(c,:)-min(tmptmp(c,1:149)); end;
                    end
                    datcontrol(:,:,tr1,1)=tmptmp;
                end
            end
        end
        for t=60:size(stima,2)-90; 
            if and(stima(t-1)==0,stima(t)>0);
                if stima(t+5)==oris2(fil)
                    tr2=1+tr2;
                    tmptmp=behtmp(:,t-59:t+89);
                    for c=1:size(tmptmp,1);
                        %tmptmp(c,:)=smoothdata(tmptmp(c,:),'movmedian',5);
                       if dobasecorr==1; tmptmp(c,:)=tmptmp(c,:)-min(tmptmp(c,1:149)); end;                        
                    end
                    datcontrol(:,:,tr2,2)=tmptmp;
                end
            end
        end
        if tr1<numtrials; 
            numtrials1=tr1-1;
        else
            numtrials1=numtrials-1; 
        end
        mouseavs_ctrl(:,:,fil,1)=mean(datcontrol(:,:,tr1-numtrials1:tr1,1),3);
        if tr2<numtrials; 
            numtrials1=tr2-1;
        else
            numtrials1=numtrials-1; 
        end
        mouseavs_ctrl(:,:,fil,2)=mean(datcontrol(:,:,tr2-numtrials1:tr2,2),3);
    end
    
    clearvars -except files fil donorm numtrials oris1 oris2 mouseavs_ctrl mouseavs_rdnt mouseavs_dev redundantnumber phzflp dobasecorr
    load(strcat('time_aligned_control',files{fil}));
    for dotest=1;
        try load(strcat('time_aligned_control',files{fil},'test_stimbegins'));
        catch
        figure; plot(resamp_stims_test); title('select beginning, middle, then end');
        [x,y]=ginput(3); 
        x=round(x); if (x(1)<1); x(1)=1; end; if x(3)>size(resamp_stims_test,2); x(3)=size(resamp_stims_test,2); end;
        save(strcat('time_aligned_control',files{fil},'test_stimbegins'),'x');
        end
        x1=x+size(resamp_stims_ctrl,2); 

        for phz=1;
            stima=resamp_stims_test(1,x(1):x(2)); 
            beh_vars_norm_test=beh_vars_norm(:,x1(1):x1(2));
            beh_vars_raw_test=beh_vars_raw(:,x1(1):x1(2));

            if donorm==1; behtmp=beh_vars_norm_test; else behtmp=beh_vars_raw_test; end
            stima=round((stima./max(stima))*2);
            for c=1:size(behtmp,1);
                behtmp(c,:)=smoothdata(behtmp(c,:),'movmedian',5);
                 %behtmp(c,:)=(behtmp(c,:))./max(behtmp(c,:)); 
            end
     
            %% PICK OUT specific redundants
            stimatmp=stima-stima; cnttmp=0;
            for t=60:(size(stima,2)-90);
                if and(stima(1,t-1)==0,stima(1,t)>0)
                    if max(stima(1,t:t+10))==2;
                        cnttmp=0;
                        stimatmp(1,t:t+15)=stima(1,t:t+15);
                    elseif cnttmp~=redundantnumber;
                        cnttmp=1+cnttmp;
                    elseif cnttmp==redundantnumber;
                        cnttmp=1+cnttmp;
                        stimatmp(1,t:t+15)=stima(1,t:t+15);
                    end
                end
            end
            stima=stimatmp;

            
            tr1=0; tr2=0;
            clear datrdnt datdev
            for t=60:size(stima,2)-90; 
                if and(stima(t-1)==0,stima(t)>0);
                    if stima(t+5)==1
                        tr1=1+tr1;
                        tmptmp=behtmp(:,t-59:t+89);
                        for c=1:size(tmptmp,1);
                            %tmptmp(c,:)=smoothdata(tmptmp(c,:),'movmedian',5);
                           if dobasecorr==1; tmptmp(c,:)=tmptmp(c,:)-min(tmptmp(c,1:149)); end;                            
                        end
                        datrdnt(:,:,tr1)=tmptmp;
                    end
                end
            end
            for t=60:size(stima,2)-90; 
                if and(stima(t-1)==0,stima(t)>0);
                    if stima(t+5)==2
                        tr2=1+tr2;
                        tmptmp=behtmp(:,t-59:t+89);
                        for c=1:size(tmptmp,1);
                            %tmptmp(c,:)=smoothdata(tmptmp(c,:),'movmedian',5);
                           if dobasecorr==1; tmptmp(c,:)=tmptmp(c,:)-min(tmptmp(c,1:149)); end;
                        end
                        datdev(:,:,tr2)=tmptmp;
                    end
                end
            end
            if numtrials>tr1;
                numtrials1=tr1;
            else
                numtrials1=numtrials;
            end
            mouseavs_rdnt(:,:,fil,phz)=mean(datrdnt(:,:,2:numtrials1),3);
            if numtrials>tr2;
                numtrials1=tr2;
            else
                numtrials1=numtrials;
            end
            mouseavs_dev(:,:,fil,phzflp(phz))=mean(datdev(:,:,2:numtrials1),3);
        end
    
        for phz=2;
            stima=resamp_stims_test(1,x(2):x(3)); 
            beh_vars_norm_test=beh_vars_norm(:,x1(2):x1(3));
            beh_vars_raw_test=beh_vars_raw(:,x1(2):x1(3));

            if donorm==1; behtmp=beh_vars_norm_test; else behtmp=beh_vars_raw_test; end
            stima=round((stima./max(stima))*2);
            for c=1:size(behtmp,1);
                behtmp(c,:)=smoothdata(behtmp(c,:),'movmedian',5);
                 %behtmp(c,:)=(behtmp(c,:))./max(behtmp(c,:)); 
            end

            %% PICK OUT specific redundants
            stimatmp=stima-stima; cnttmp=0;
            for t=60:(size(stima,2)-90);
                if and(stima(1,t-1)==0,stima(1,t)>0)
                    if max(stima(1,t:t+10))==2;
                        cnttmp=0;
                        stimatmp(1,t:t+15)=stima(1,t:t+15);
                    elseif cnttmp~=redundantnumber;
                        cnttmp=1+cnttmp;
                    elseif cnttmp==redundantnumber;
                        cnttmp=1+cnttmp;
                        stimatmp(1,t:t+15)=stima(1,t:t+15);
                    end
                end
            end
            stima=stimatmp;

            
            tr1=0; tr2=0;
            clear datrdnt datdev
            for t=60:size(stima,2)-90; 
                if and(stima(t-1)==0,stima(t)>0);
                    if stima(t+5)==1
                        tr1=1+tr1;
                        tmptmp=behtmp(:,t-59:t+89);
                        for c=1:size(tmptmp,1);
                            %tmptmp(c,:)=smoothdata(tmptmp(c,:),'movmedian',5);
                           if dobasecorr==1; tmptmp(c,:)=tmptmp(c,:)-min(tmptmp(c,1:149)); end;
                        end
                        datrdnt(:,:,tr1)=tmptmp;
                    end
                end
            end
            for t=60:size(stima,2)-90; 
                if and(stima(t-1)==0,stima(t)>0);
                    if stima(t+5)==2
                        tr2=1+tr2;
                        tmptmp=behtmp(:,t-59:t+89);
                        for c=1:size(tmptmp,1);
                            %tmptmp(c,:)=smoothdata(tmptmp(c,:),'movmedian',5);
                           if dobasecorr==1; tmptmp(c,:)=tmptmp(c,:)-min(tmptmp(c,1:149)); end;
                        end
                        datdev(:,:,tr2)=tmptmp;
                    end
                end
            end
            if numtrials>tr1;
                numtrials1=tr1;
            else
                numtrials1=numtrials;
            end
            mouseavs_rdnt(:,:,fil,phz)=mean(datrdnt(:,:,2:numtrials1),3);
            if numtrials>tr2;
                numtrials1=tr2;
            else
                numtrials1=numtrials;
            end
            mouseavs_dev(:,:,fil,phzflp(phz))=mean(datdev(:,:,2:numtrials1),3);
        end
  
    end
    
    



end
    
fp=33.333/1000;
timeaxis=(-59*fp):fp:((90*fp)-fp);

varnames={'blink','nose','whisk','locomotion'};
for var=1:4;
    figure(var+fil);
    cnt=mean(mean(mouseavs_ctrl(var,:,:,:),3),4); c1(var)=mean(cnt(1,45:59)); if dobasecorr==1; cnt=mean(mean(mouseavs_ctrl(var,:,:,:),3),4)-mean(cnt(1,45:59)); end;
    cntst=std(mean(mouseavs_ctrl(var,:,:,:),4),0,3)./sqrt(fil-1);
    shadedErrorBar(timeaxis,cnt,cntst,'lineProps','k'); hold on

    cnt=mean(mean(mouseavs_rdnt(var,:,:,:),3),4); r1(var)=mean(cnt(1,45:59));if dobasecorr==1;  cnt=mean(mean(mouseavs_rdnt(var,:,:,:),3),4)-mean(cnt(1,45:59)); end;
    cntst=std(mean(mouseavs_rdnt(var,:,:,:),4),0,3)./sqrt(fil-1);
    shadedErrorBar(timeaxis,cnt,cntst,'lineProps','b'); hold on

    cnt=mean(mean(mouseavs_dev(var,:,:,:),3),4); d1(var)=mean(cnt(1,45:59));if dobasecorr==1; cnt=mean(mean(mouseavs_dev(var,:,:,:),3),4)-mean(cnt(1,45:59)); end;
    cntst=std(mean(mouseavs_dev(var,:,:,:),4),0,3)./sqrt(fil-1);
    shadedErrorBar(timeaxis,cnt,cntst,'lineProps','r'); hold on
    title(varnames{var});
end

for var=1:4;
    for t=1:size(mouseavs_ctrl,2);
        if dobasecorr==1; cnt=squeeze(mean(mouseavs_ctrl(var,t,:,:),4))-c1(var);
        rdnt=squeeze(mean(mouseavs_rdnt(var,t,:,:),4))-r1(var);
        dev=squeeze(mean(mouseavs_dev(var,t,:,:),4))-d1(var);
        else cnt=squeeze(mean(mouseavs_ctrl(var,t,:,:),4));
        rdnt=squeeze(mean(mouseavs_rdnt(var,t,:,:),4));
        dev=squeeze(mean(mouseavs_dev(var,t,:,:),4));
        end
        [p,table] = anova_rm(horzcat(cnt,dev,rdnt),'off');
        pvals(var,t)=p(1);
    end
    figure(var+4+fil); plot(timeaxis,pvals(var,:)); title(varnames{var});
end

for var=1:4;
    for t=1;
        if dobasecorr==1; cnt=squeeze(mean(mean(mouseavs_ctrl(var,61:75,:,:),2),4))-c1(var);
        rdnt=squeeze(mean(mean(mouseavs_rdnt(var,61:75,:,:),2),4))-r1(var);
        dev=squeeze(mean(mean(mouseavs_dev(var,61:75,:,:),2),4))-d1(var);
        else cnt=squeeze(mean(mean(mouseavs_ctrl(var,61:75,:,:),2),4));
        rdnt=squeeze(mean(mean(mouseavs_rdnt(var,61:75,:,:),2),4));
        dev=squeeze(mean(mean(mouseavs_dev(var,61:75,:,:),2),4));
        end
        [p,table] = anova_rm(horzcat(cnt,dev,rdnt),'on');       
    end
end


ylim([0 .14]); make_eps_saveable

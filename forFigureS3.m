%%% opto
clear all

for file_load=1;
    %pre
    files = {'';};%

%     % %post
     files = {'';};%
    

end

limtrials1=10; %limit the number of trials analysed for each stimulus type(control/redundandt/deviant).
baseBEGIN=1; baseEND=12; %what you call the baseline
BEGINo=14; ENDo=28; %these are the analysis windows. note: stimulus starts at 14 and finishes at 28
onestim=0; %if 0, then plot responses to both stimuli (e.g. 45 and 135 deg). if 1, then plot only to first orientation. if 2, then plot to second
dobeforeafter=0; %if this is "1", then it compares the first trials to the last for the test. if it's zero, it does even/odd
stdfromaverage=0; %normalize based on average, rather than background Df. did not use this bc optoLED occurs in baseline. 

%%%%%%%this compiles all the files into three matrices
for buildingmatrices=1;
    effectsallBUILD=[]; %even
    effectsallTEST=[]; %odd
    singtrialsall=[];
    count=0; counttest=0;
    effectsall=[];  %this is the main varable
    actcont=[];
    osis=[]; odiffs=[];
    mousevec=[];
    countall=0;
end
for file=1:size(files,1);

    load(strcat('CA_workspace_SORTED_',files{file}));

    for onlydothirdtrials=0; %if you want to eXCLUDE thrid trials, change line below
        if onlydothirdtrials==1;
            try laoad(strcat('CA_workspace_SORTED_',files{file},'onlythird'));
            catch
                stimvistypeall=stimvistypeMAIN-stimvistypeMAIN;
                cnta=0;
                for t=30:size(stimvistypeMAIN,2);
                    if phases(1,t)~=phases(1,t-1);
                        cnta=0;
                    end
                    if and(stimvistypeMAIN(1,t)>0,stimvistypeMAIN(1,t-1)==0);
                        if cnta<2;
                            stimvistypeall(1,t:t+15)=stimvistypeMAIN(t:t+15); %exclude third trials if under "cnta<2". include third trials if under "else"
                            cnta=1+cnta;
                        else
                            cnta=0;
                        end
                    end
                end
                figure; plot(stimvistypeMAIN); hold on;
                plot(stimvistypeall,'r'); xlim([1 3000]); title(files{file})
                for phz=1:3;

                    for vectorize_populations=1;
                        if vectorize_populations==1;
                            if phz>1;
                                phzflp=[1 3 2]; phzstd=[20 21];
                                tmpvis=stimvistypeall-stimvistypeall;
                                for t=1:size(tmpvis,2);
                                    if and(phases(t)==phz,and(stimvistypeall(1,t)>0,stimvistypeall(1,t)<15));
                                        tmpvis(1,t)=stimvistypeall(1,t);
                                    elseif and(phases(t)==phzflp(phz),stimvistypeall(1,t)==15);
                                        tmpvis(1,t)=stimvistypeall(1,t);
                                    elseif and(phases(t)==1,stimvistypeall(1,t)==phzstd(phz-1));
                                        tmpvis(1,t)=stimvistypeall(1,t);
                                    end
                                end

                                vecdat=[];
                                vecstim=[];
                                tr=0;
                                for t=framerate/2:size(dfofanalyse,2)-(framerate);
                                    if (tmpvis(1,t)-tmpvis(1,t-1))>.5;
                                        tr=1+tr;
                                        vecdat(:,:,tr)=dfofanalyse(:,t-(framerate*.5):t+framerate);
                                        vecstim(tr)=(stimvistypeall(1,t));

                                    end
                                end
                                for trtr=1:tr;
                                    if and(vecstim(trtr)>8,vecstim(trtr)<15);
                                        vecstim(trtr)=99;
                                    end
                                end
                                vectrials{phz-1,1}=vecdat;
                                vectrials{phz-1,2}=vecstim;
                            end


                        end
                    end
                end
            end
            save(strcat('CA_workspace_SORTED_',files{file},'onlythird'),'vectrials');
        end
       
    end
    %figure; plot(stimvistypeMAIN); title(num2str(file));
    disp(files{file});

    for analysis=1

        samp=1/framerate;
        timeaxis=-(samp*(framerate/2)):samp:(samp*framerate); timeaxis=timeaxis+.088;

        for stim=1:2;
            limtrials=limtrials1;
            vecdat=vectrials{stim,1};
            mousetmp=zeros(size(vecdat,1),1)+file;
            vecstim=vectrials{stim,2};
            if limtrials1>0;
                for checkingfortoo_few_trials=1;
                    ss=[1 2 15 phzstd(stim)];
                    for gss=1:4;
                        sss(gss)=sum(vecstim==ss(gss));
                    end
                    if min(sss)<limtrials1;
                        limtrials=min(sss);
                    end
                    disp(limtrials);
                end

                effects=[];
                for z1=1:8
                    tmp1=vecdat(:,:,vecstim==z1);
                    if size(tmp1,3)>(limtrials-1)
                        effects(:,:,z1+1)=mean(tmp1(:,:,1:limtrials),3);
                    else
                        effects(:,:,z1+1)=mean(tmp1(:,:,1:end),3);
                    end
                end
                tmp1=vecdat(:,:,vecstim==15);
                if size(tmp1,3)>(limtrials-1)
                    effects(:,:,10)=mean(tmp1(:,:,1:limtrials),3);
                else
                    effects(:,:,10)=mean(tmp1(:,:,1:end),3);
                end
                tmp1=vecdat(:,:,vecstim==phzstd(stim));

                if size(tmp1,3)>(limtrials-1)
                    effects(:,:,1)=mean(tmp1(:,:,1:limtrials),3);
                else
                    effects(:,:,1)=mean(tmp1(:,:,1:end),3);
                end

            else
                vecdat=vectrials{stim,1};
                vecstim=vectrials{stim,2};
                effects=[];
                stimax=[phzstd(stim) 1 2 3 4 5 6 7 8 15];
                for z1=1:10
                    effects(:,:,z1)=mean(vecdat(:,:,vecstim==stimax(z1)),3);
                end
            end


            for c1=1:size(effects,1) %baseline correction
                basos=[1 2 2 2 2 2 2 2 2 3; 1 3 3 3 3 3 3 3 3 2];
                at=squeeze(effects(c1,baseBEGIN:baseEND,:));

                for z1=1:size(effects,3)
                    mnn=min(effects(c1,baseBEGIN:baseEND,z1),[],2);
                    tmpbasos=dfofanalyse(c1,phases==basos(stim,z1));
                    tmpbasos=tmpbasos(tmpbasos>0); tmpbasos=tmpbasos(tmpbasos<prctile(tmpbasos,92));
                    stt=std(tmpbasos);
                    if stdfromaverage==1; stt=std(at(:)); end

                    if stt<.001; stt=std(effects(c1,ENDo:end,z1)); end
                    if stt<.001; stt=10000; end
                    for t1=1:size(effects,2)
                        effects(c1,t1,z1)=(effects(c1,t1,z1)-mnn)./stt;
                    end
                    mnns(file,stim,c1,z1)=mnn;
                    stts(file,stim,c1,z1)=stt;
                end

            end


            for getspontaneousactivity=1
                if stim==1
                    for c1=1:size(effects,1)
                        tmp=[];
                        for ph=1:3

                            tmp2=dfofanalyse(c1,phases==ph);
                            tmp3=stimvisMAIN(1,phases==ph);
                            t1=0; t2=0; %t1 becomes teh beginning of stims. t2 becomes the end.
                            for t=100:size(tmp3,2)-100
                                if and(t1==0,tmp3(1,t)>0)
                                    t1=t;
                                end
                                if and(t2==0,t1>0)
                                    if max(tmp3(1,t-99:t+99))==0
                                        t2=t-99;
                                    end
                                end
                            end
                            if t2==0; t2=size(tmp3,2)-99; end

                            tmp22=tmp2(1,tmp2(1,:)>0); pr=prctile(tmp22,95); tmp2=tmp2/std(tmp22<pr);

                            for alltimepoints=0 %if you want to just analyse interstimulus activity, make this =0
                                if alltimepoints==1
                                    bin1=floor((t2-t1)/10);
                                    for b=1:10;
                                        tmp=horzcat(tmp,mean(tmp2(1,((b-1)*bin1)+t1+1:(b*bin1)+t1)));
                                    end
                                else
                                    tmp2=tmp2(1,t1:t2); tmp3=tmp3(1,t1:t2);
                                    tmp2=tmp2(1,tmp3==0);
                                    bin1=floor(size(tmp2,2)/10);
                                    for b=1:10
                                        tmp=horzcat(tmp,mean(tmp2(1,((b-1)*bin1)+1:(b*bin1))));
                                    end
                                end
                            end

                        end
                        actcont=vertcat(actcont,tmp);
                    end
                end
            end

            %for getting single trial data. make sure to comment out the
            %"exclude first few trials" section above if you want to really see the truth!
            tmpcells=[];
            for cc=1:size(vecdat,1);

                tmp00=dfofanalyse(cc,30:449)./stts(file,stim,cc,1);
                tmp1=squeeze(vecdat(cc,:,vecstim==phzstd(stim)))./stts(file,stim,cc,1);
                tmp0=[]; stim0=[];
                for g=1:limtrials;
                    tmp0=vertcat(tmp0,tmp1(:,g));
                    stim0=horzcat(stim0,zeros(1,13),ones(1,14),zeros(1,16));
                end
                stim0=horzcat(zeros(1,420),stim0); tmp0=vertcat(tmp00',tmp0);
                tmp1=squeeze(vecdat(cc,:,vecstim==1))./stts(file,stim,cc,2); ; tmp2=squeeze(vecdat(cc,:,vecstim==2))./stts(file,stim,cc,3);
                tmp3=squeeze(vecdat(cc,:,vecstim==3))./stts(file,stim,cc,4);  tmp4=squeeze(vecdat(cc,:,vecstim==4))./stts(file,stim,cc,5);
                tmp5=squeeze(vecdat(cc,:,vecstim==5))./stts(file,stim,cc,6);  tmp6=squeeze(vecdat(cc,:,vecstim==6))./stts(file,stim,cc,7);
                tmp7=squeeze(vecdat(cc,:,vecstim==7))./stts(file,stim,cc,8);  tmp8=squeeze(vecdat(cc,:,vecstim==8))./stts(file,stim,cc,9);
                tmp15=squeeze(vecdat(cc,:,vecstim==15))./stts(file,stim,cc,10);  ggg=0;
                for g=1:5;
                    try
                        tmp0=vertcat(tmp0,tmp1(:,g),tmp2(:,g),tmp3(:,g),tmp4(:,g),tmp5(:,g),tmp6(:,g),tmp7(:,g),tmp8(:,g),tmp15(:,g));

                        stim0=horzcat(stim0,zeros(1,13),ones(1,14)-.5,zeros(1,16),zeros(1,13),ones(1,14)-.5,zeros(1,16),zeros(1,13),ones(1,14)-.5,zeros(1,16),zeros(1,13),ones(1,14)-.5,zeros(1,16),zeros(1,13),ones(1,14)-.5,zeros(1,16),zeros(1,13),ones(1,14)-.5,zeros(1,16),zeros(1,13),ones(1,14)-.5,zeros(1,16),zeros(1,13),ones(1,14)-.5,zeros(1,16),zeros(1,13),ones(1,14)+.5,zeros(1,16));
                        ggg=g;
                    end
                end
                if ggg==5;
                    tmpcells=vertcat(tmpcells,tmp0');
                    stim10=stim0;
                end
            end



            if onestim==0;
                effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),2:10,stim)=effects(:,1:end,2:10);
                effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),1,stim)=effects(:,1:end,1);

                singtrialsall((1+countall):(countall+size(effects,1)),1:size(tmpcells,2),stim)=tmpcells;
                mousevec((1+countall):(countall+size(effects,1)),stim)=mousetmp;
            elseif onestim==1;
                if stim==1;
                    effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),2:10,stim)=effects(:,1:end,2:10);
                    effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),1,stim)=effects(:,1:end,1);

                    singtrialsall((1+countall):(countall+size(effects,1)),1:size(tmpcells,2),stim)=tmpcells;
                    mousevec((1+countall):(countall+size(effects,1)),stim)=mousetmp;
                else
                    effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),2:10,stim)=effects(:,1:end,2:10)-effects(:,1:end,2:10);
                    effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),1,stim)=effects(:,1:end,1)-effects(:,1:end,1);
                    singtrialsall((1+countall):(countall+size(effects,1)),1:size(tmpcells,2),stim)=tmpcells-tmpcells;
                    mousevec((1+countall):(countall+size(effects,1)),stim)=mousetmp-mousetmp;
                end
            elseif onestim==2;
                if stim==2;
                    effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),2:10,stim)=effects(:,1:end,2:10);
                    effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),1,stim)=effects(:,1:end,1);
                    singtrialsall((1+countall):(countall+size(effects,1)),1:size(tmpcells,2),stim)=tmpcells;
                    mousevec((1+countall):(countall+size(effects,1)),stim)=mousetmp;
                else
                    effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),2:10,stim)=effects(:,1:end,2:10)-effects(:,1:end,2:10);
                    effectsall((1+countall):(countall+size(effects,1)),1:size(effects,2),1,stim)=effects(:,1:end,1)-effects(:,1:end,1);
                    singtrialsall((1+countall):(countall+size(effects,1)),1:size(tmpcells,2),stim)=tmpcells-tmpcells;
                    mousevec((1+countall):(countall+size(effects,1)),stim)=mousetmp-mousetmp;
                end
            end

        end

        countall=countall+size(effects,1);
    end

    effectsallBUILD=effectsall;
    effectsallTEST=effectsall;
    load(strcat('orientation_selectivities_',files{file}),'osi','obest','Odiffstim');
    osis=vertcat(osis,horzcat(osi,osi));
    odiffs=vertcat(odiffs,Odiffstim);
end
close all
titles={'Control';' ';' ';' ';'redundants';' ';' ';' ';' ';'DEVIANT'};
redundantnumberA=4; % this is the redundant in the chain that you will plot/analyse; if it's 0, then average over all redundants
include_cutoff=1; %number of standard deviations above baseline for inclusion
exclude_outlier=100; %get rid of the cell if any responses are this big
load effectsallTMPopto
effectsallTMP=effectsall;%effectsallTMP=effectsallTMPopto; 

%if you want to just plot averages accross all trials, make stringent test
%%% equal to 0. if you want to do a hard test of clusters,make it 1... use
%%% 1 only if you are actually testing teh reality of a subcluster
for stringentTEST=0;
    if stringentTEST==0;
        %%%%%%%%%figures out relative responses to different stimuli
        simpleeffects=[]; simpleeffectsPRE=[];simpleeffectsPOST=[];c1=1; c2=1; cols=[]; newindices=[];notindices=[];
        for stim=1:2
            for c=1:size(effectsallTMP,1)
                if redundantnumberA==0;
                    r=mean(mean(effectsallTMP(c,BEGINo:ENDo,2:9,stim)));
                else
                    r=mean(effectsallTMP(c,BEGINo:ENDo,redundantnumberA+1,stim));
                end
                cn=mean(effectsallTMP(c,BEGINo:ENDo,1,stim));
                d=mean(effectsallTMP(c,BEGINo:ENDo,10,stim));
                if redundantnumberA==0;
                    rP=mean(mean(effectsallBUILD(c,BEGINo:ENDo,2:9,stim)));
                else
                    rP=mean(effectsallBUILD(c,BEGINo:ENDo,redundantnumberA+1,stim));
                end
                cnP=mean(effectsallBUILD(c,BEGINo:ENDo,1,stim));
                dP=mean(effectsallBUILD(c,BEGINo:ENDo,10,stim));
                if redundantnumberA==0;
                    rT=mean(mean(effectsallTEST(c,BEGINo:ENDo,2:9,stim)));
                else
                    rT=mean(effectsallTEST(c,BEGINo:ENDo,redundantnumberA+1,stim));
                end
                cnT=mean(effectsallTEST(c,BEGINo:ENDo,1,stim));
                dT=mean(effectsallTEST(c,BEGINo:ENDo,10,stim));
                if and(or(or(r>include_cutoff,cn>include_cutoff),d>include_cutoff),and(and(r<exclude_outlier,cn<exclude_outlier),d<exclude_outlier))
                    simpleeffects(c1,1)=(cn);
                    simpleeffects(c1,2)=(r);
                    simpleeffects(c1,3)=(d);
                    simpleeffectsPRE(c1,1)=(cnP);
                    simpleeffectsPRE(c1,2)=(rP);
                    simpleeffectsPRE(c1,3)=(dP);
                    simpleeffectsPOST(c1,1)=(cnT);
                    simpleeffectsPOST(c1,2)=(rT);
                    simpleeffectsPOST(c1,3)=(dT);
                    newindices(c1,1)=c;
                    newindices(c1,2)=stim;
                    c1=1+c1;
                else
                    notindices(c2,1)=c;
                    notindices(c2,2)=stim;
                    c2=1+c2;

                end

            end
        end
        for makingcolors=1;
            for c=1:(c1-1)
                s=simpleeffects(c,1)./max(simpleeffects(c,:));
                cols(c,2)=s;
                s=simpleeffects(c,3)./max(simpleeffects(c,:));
                cols(c,1)=s;
                s=simpleeffects(c,2)./max(simpleeffects(c,:));
                cols(c,3)=s;
            end
            for c=1:(c1-1);
                a=cols(c,:)-min(cols(c,:));
                cols(c,:)=cols(c,:)./max(cols(c,:));
            end
        end

    else
        simpleeffects=[]; simpleeffectsPRE=[];simpleeffectsPOST=[];c1=1; c2=1; cols=[]; newindices=[];notindices=[];
        for stim=1:2
            for c=1:size(effectsallBUILD,1)
                if redundantnumberA==0;
                    r=mean(mean(effectsallBUILD(c,BEGINo:ENDo,2:9,stim)));
                else
                    r=mean(effectsallBUILD(c,BEGINo:ENDo,redundantnumberA+1,stim));
                end
                cn=mean(effectsallBUILD(c,BEGINo:ENDo,1,stim));
                d=mean(effectsallBUILD(c,BEGINo:ENDo,10,stim));
                if redundantnumberA==0;
                    rT=mean(mean(effectsallTEST(c,BEGINo:ENDo,2:9,stim)));
                else
                    rT=mean(effectsallTEST(c,BEGINo:ENDo,redundantnumberA+1,stim));
                end
                cnT=mean(effectsallTEST(c,BEGINo:ENDo,1,stim));
                dT=mean(effectsallTEST(c,BEGINo:ENDo,10,stim));
                if and(or(or(r>include_cutoff,cn>include_cutoff),d>include_cutoff),and(and(r<exclude_outlier,cn<exclude_outlier),d<exclude_outlier))
                    simpleeffects(c1,1)=(cn);
                    simpleeffects(c1,2)=(r);
                    simpleeffects(c1,3)=(d);
                    simpleeffectsPRE(c1,1)=(cn);
                    simpleeffectsPRE(c1,2)=(r);
                    simpleeffectsPRE(c1,3)=(d);
                    simpleeffectsPOST(c1,1)=(cnT);
                    simpleeffectsPOST(c1,2)=(rT);
                    simpleeffectsPOST(c1,3)=(dT);
                    newindices(c1,1)=c;
                    newindices(c1,2)=stim;
                    c1=1+c1;
                else
                    notindices(c2,1)=c;
                    notindices(c2,2)=stim;
                    c2=1+c2;

                end

            end
        end
        for makingcolors=1;
            for c=1:(c1-1)
                s=simpleeffects(c,1)./max(simpleeffects(c,:));
                cols(c,2)=s;
                s=simpleeffects(c,3)./max(simpleeffects(c,:));
                cols(c,1)=s;
                s=simpleeffects(c,2)./max(simpleeffects(c,:));
                cols(c,3)=s;
            end
            for c=1:(c1-1);
                a=cols(c,:)-min(cols(c,:));
                cols(c,:)=cols(c,:)./max(cols(c,:));
            end
        end
    end
end

for countcellstotal=1; % for only focusing on one response per cell
    numcells=size(unique(newindices(:,1)),1);
    disp(strcat('all cells=',num2str(size(effectsall,1)),'; responsive=',num2str(numcells),'; percent=',num2str(numcells/size(effectsall,1))));
    ex1=zeros(size(simpleeffects,1),1);
    for c=1:size(effectsall,1);
        if sum(newindices(:,1)==c)>1;
            clear tmp1 tmp2; cnt1=1;
            for c1=1:size(newindices,1);
                if newindices(c1,1)==c;
                    tmp1(cnt1,1)=mean(simpleeffects(c1,[1 2 3]));
                    tmp2(cnt1,1)=c1;
                    cnt1=1+cnt1;
                end
            end
            if tmp1(1,1)>tmp1(2,1);
                ex1(tmp2(2,1),1)=1;
            else
                ex1(tmp2(1,1),1)=1;
            end
        end
    end
    for onlylookatonePERcell=1;
        if onlylookatonePERcell==1;
            simpleeffectsPRE=simpleeffectsPRE(ex1==0,:);
            simpleeffects=simpleeffects(ex1==0,:);
            simpleeffectsPOST=simpleeffectsPOST(ex1==0,:);
            newindices=newindices(ex1==0,:);
        end
    end
    for makingcolors=1;
        c1=size(simpleeffects,1)+1; clear cols
        for c=1:(c1-1)
            s=simpleeffects(c,1)./max(simpleeffects(c,:));
            cols(c,2)=s;
            s=simpleeffects(c,3)./max(simpleeffects(c,:));
            cols(c,1)=s;
            s=simpleeffects(c,2)./max(simpleeffects(c,:));
            cols(c,3)=s;

        end
        for c=1:(c1-1);
            a=cols(c,:)-min(cols(c,:));
            cols(c,:)=cols(c,:)./max(cols(c,:));
            cols(c,2)=0;cols(c,3)=0;%cols(c,1)=0;
        end
    end

end


redundantnumber=4; %if you want to plot (but not "select by") a redudnant later in the stream, use this line
for plottingeverything=1;
    %%%%for selecting all or subset of cells for subesequent plots
    %%%for selecting all or subset of cells for subesequent plots
    plotanti=0; %if =1, then plot everything outside of the selected box
    selections=1; %if the cells you want to select are scattered more than a single box can capture, make this 2 or 3 and select multiple regions.
    for plotpre=1;
        %close all force
        xcut=std((.5*simpleeffects(:,1)+.5*simpleeffects(:,3))-simpleeffects(:,2))*4; ycut=std(simpleeffects(:,3)-simpleeffects(:,1))*4;
        xmn=mean((.5*simpleeffects(:,1)+.5*simpleeffects(:,3))-simpleeffects(:,2)); ymn=mean(simpleeffects(:,3)-simpleeffects(:,1));

        pind=[];
        for selecto=1:selections;
            figure; scatter((.5*simpleeffects(:,1)+.5*simpleeffects(:,3))-simpleeffects(:,2),simpleeffects(:,3)-simpleeffects(:,1),50,cols,'fill');

            if selecto>1;
                hold on;
                scatter([x11(1) x11(1) x11(2) x11(2)],[y11(1) y11(2) y11(1) y11(2)]);
            end
            ylabel('Deviance Detection','FontSize',12,'FontWeight','bold'); xlabel('Stim specific adaptation','FontSize',12,'FontWeight','bold');
            title('pick region to plot; upper left, then lower right');
            [x,y]=ginput(2); close
            if selecto==1;
                x11=x; y11=y;
            end
            if selecto==2;
                x22=x; y22=y;
            end

            for c=1:size(simpleeffects,1);
                if and(((.5*simpleeffects(c,1)+.5*simpleeffects(c,3))-simpleeffects(c,2))>x(1),((.5*simpleeffects(c,1)+.5*simpleeffects(c,3))-simpleeffects(c,2))<x(2));
                    if and((simpleeffects(c,3)-simpleeffects(c,1))<y(1),(simpleeffects(c,3)-simpleeffects(c,1))>y(2))
                        if sum(c==pind)==0;
                            pind=vertcat(pind,c);
                        end
                    end
                end
            end
        end


        if plotanti==1;
            ppp=[];
            pp=1:size(simpleeffects,1); pp=pp';
            for c=1:size(simpleeffects,1);
                if sum(pp(c)==pind)==0;
                    ppp=vertcat(ppp,c);
                end
            end
            pind=ppp;
        end
        numcells=size(pind,1);
        tmpeff=[];
        for c=1:numcells;
            tmpeff(c,:,:)=effectsallBUILD(newindices(pind(c),1),:,:,(newindices(pind(c),2)));
            for t=1:size(effectsallBUILD,2)
                if redundantnumber>0;
                    mn=mean(mean(effectsallBUILD(newindices(pind(c),1),t,[redundantnumber+1 1 10],newindices(pind(c),2)),3),4);
                    a1(c,t)=mean(effectsallBUILD(newindices(pind(c),1),t,redundantnumber+1,newindices(pind(c),2)),4)-mn;
                else
                    a1tmp=mean(mean(effectsallBUILD(newindices(pind(c),1),t,2:9,newindices(pind(c),2)),3),4);
                    a2tmp=mean(effectsallBUILD(newindices(pind(c),1),t,1,newindices(pind(c),2)),4);
                    a3tmp=mean(effectsallBUILD(newindices(pind(c),1),t,10,newindices(pind(c),2)),4);
                    mn=(a1tmp+a2tmp+a3tmp)/3;
                    a1(c,t)=mean(mean(effectsallBUILD(newindices(pind(c),1),t,2:9,newindices(pind(c),2)),3),4)-mn;
                end
                a2(c,t)=mean(effectsallBUILD(newindices(pind(c),1),t,1,newindices(pind(c),2)),4)-mn;
                a3(c,t)=mean(effectsallBUILD(newindices(pind(c),1),t,10,newindices(pind(c),2)),4)-mn;
            end
        end
        x=timeaxis;
        y1=mean(mean(mean(tmpeff(:,:,redundantnumber+1),3),4),1);
        if redundantnumber==0;y1=mean(mean(mean(tmpeff(:,:,2:9),3),4),1); end
        y1a=y1-min(y1(1,1:BEGINo-1));
        err1=std(a1,0,1)./sqrt(numcells);

        y2=mean(mean(mean(tmpeff(:,:,1),3),4),1);y2a=y2-min(y2(1,1:BEGINo-1));
        err2=std(a2,0,1)./sqrt(numcells);

        y3=mean(mean(mean(tmpeff(:,:,10),3),4),1); y3a=y3-min(y3(1,1:BEGINo-1));
        err3=std(a3,0,1)./sqrt(numcells);


        if numcells>1;
            figure; shadedErrorBar(x,y1a,err1,'lineProps','b'); hold on;shadedErrorBar(x,y2a,err1,'lineProps','k');hold on;shadedErrorBar(x,y3a,err1,'lineProps','r');
            xlim([-.2 1]); title ('Pre trials', 'FontSize', 16, 'FontWeight', 'bold');ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            set(gcf,'Color','w');

            yy=ylim;
            figure;  errorbar_groups([mean(mean(y2a(:,BEGINo:ENDo))) mean(mean(y1a(:,BEGINo:ENDo))) mean(mean(y3a(:,BEGINo:ENDo)))]',[mean(mean(err2(:,BEGINo:ENDo))) mean(mean(err1(:,BEGINo:ENDo))) mean(mean(err3(:,BEGINo:ENDo)))]', 'bar_colors',[.5 .5 .5; 0 0 1; 1 0 0]);
            ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            set(gcf,'Color','w');
            for statstat=1;
                r1=mean(mean(tmpeff(:,BEGINo:ENDo,redundantnumber+1),3),2)-mean(mean(tmpeff(:,BEGINo-9:BEGINo-1,redundantnumber+1),3),2);
                if redundantnumber==0;
                    r1=mean(mean(tmpeff(:,BEGINo:ENDo,2:9),3),2)-mean(mean(tmpeff(:,BEGINo-9:BEGINo-1,2:9),3),2);
                end
                c1=mean(mean(tmpeff(:,BEGINo:ENDo,1),3),2)-mean(mean(tmpeff(:,BEGINo-9:BEGINo-1,1),3),2);
                d1=mean(mean(tmpeff(:,BEGINo:ENDo,10),3),2)-mean(mean(tmpeff(:,BEGINo-9:BEGINo-1,10),3),2);

                [h,p1,ci,stats1]=ttest(r1,c1);
                [h,p2,ci,stats2]=ttest(d1,c1);
            end
            title(strcat('T_S_S_A','=',num2str(round(stats1.tstat*100)/100),', p=',num2str(round(p1*1000)/1000),'; T_D_D','=',num2str(round(stats2.tstat*100)/100),', p=',num2str(round(p2*1000)/1000)));
            title('pre trials');
        else
            figure; plot(x,y1,'b','LineWidth',3); hold on;plot(x,y2,'k','LineWidth',3); hold on;plot(x,y3,'r','LineWidth',3);yy=ylim;
            xlim([-.2 1]); title ('Average of cell responses: pre trials', 'FontSize', 16, 'FontWeight', 'bold');
        end

        for manyplotthing=1;
            figure;
            for cnd=1:10;
                y1=mean(tmpeff(:,:,cnd),1);
                y1=y1-min(y1(1,baseBEGIN:baseEND));
                if cnd==1; err1=std(a1,0,1)./sqrt(numcells);
                    err=std(a1,0,1)./sqrt(numcells);

                elseif cnd==10;
                    err=std(a3,0,1)./sqrt(numcells);
                else
                    err=std(a2,0,1)./sqrt(numcells);
                end
                subplot(1,10,cnd); shadedErrorBar(x,y1,err,'lineProps','k'); set(gcf,'Color','w');
                if cnd==1;
                    ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
                    title(titles{cnd}, 'FontSize', 18, 'FontWeight', 'bold');
                else
                    title(titles{cnd}, 'FontSize', 18, 'FontWeight', 'bold'); yticks([]);set(gca,'FontSize',16,'FontWeight','bold');
                end
            end
            for fixaxes=1

                ymin1=1; ymax1=-1;
                for cnd=1:10
                    subplot(1,10,cnd); yy=ylim;
                    if yy(1)<ymin1; ymin1=yy(1); end
                    if yy(2)>ymax1; ymax1=yy(2); end

                end
                for cnd=1:10
                    subplot(1,10,cnd); ylim([ymin1 ymax1]);
                end

            end
        end

        for testoverlapppingcelss=1;
            for pp=1:size(pind,1);
                aaa(pp)=newindices(pind(pp),1);
                bbb(pp)=newindices(pind(pp),2);
            end
            zerp=zeros(size(effectsall,1),2);
            for pp=1:size(aaa,2);
                zerp(aaa(pp),1)=1+zerp(aaa(pp));

                zerp(aaa(pp),2)=bbb(pp);
            end
            onestimDD=sum(zerp(:,1)==1);
            twostimDD=sum(zerp(:,1)==2);
            weird=zeros(size(zerp,1),1);ffllpp=[2 1];
            for ppp=1:size(zerp,1);
                if zerp(ppp,1)==1;
                    if effectsall(ppp,BEGINo:ENDo,1,ffllpp(zerp(ppp,2)))>include_cutoff;
                        weird(ppp)=1;
                    end
                end
            end
            figure; pie([onestimDD twostimDD]);   title('proportion of DDs to one stim vs both');
        end
    end

    for PLOTpost=1;

        tmpeff=[];
        for c=1:numcells;
            tmpeff(c,:,:)=effectsallTEST(newindices(pind(c),1),:,:,(newindices(pind(c),2)));
            for t=1:size(effectsall,2)
                if redundantnumber>0;
                    mn=mean(mean(effectsallTEST(newindices(pind(c),1),t,[redundantnumber+1 1 10],newindices(pind(c),2)),3),4);
                    a1(c,t)=mean(effectsallTEST(newindices(pind(c),1),t,redundantnumber+1,newindices(pind(c),2)),4)-mn;
                else
                    a1tmp=mean(mean(effectsallTEST(newindices(pind(c),1),t,2:9,newindices(pind(c),2)),3),4);
                    a2tmp=mean(effectsallTEST(newindices(pind(c),1),t,1,newindices(pind(c),2)),4);
                    a3tmp=mean(effectsallTEST(newindices(pind(c),1),t,10,newindices(pind(c),2)),4);
                    mn=(a1tmp+a2tmp+a3tmp)/3;
                    a1(c,t)=mean(mean(effectsallTEST(newindices(pind(c),1),t,2:9,newindices(pind(c),2)),3),4)-mn;
                end
                a2(c,t)=mean(effectsallTEST(newindices(pind(c),1),t,1,newindices(pind(c),2)),4)-mn;
                a3(c,t)=mean(effectsallTEST(newindices(pind(c),1),t,10,newindices(pind(c),2)),4)-mn;
            end
        end

        x=timeaxis;
        y1=mean(mean(mean(tmpeff(:,:,redundantnumber+1),3),4),1);

        if redundantnumber==0; y1=mean(mean(mean(tmpeff(:,:,2:9),3),4),1); end;
        y1a=y1-min(y1(1,1:BEGINo-1));
        err1=std(a1,0,1)./sqrt(numcells);

        y2=mean(mean(mean(tmpeff(:,:,1),3),4),1);y2a=y2-min(y2(1,1:BEGINo-1));
        err2=std(a2,0,1)./sqrt(numcells);

        y3=mean(mean(mean(tmpeff(:,:,10),3),4),1); y3a=y3-min(y3(1,1:BEGINo-1));
        err3=std(a3,0,1)./sqrt(numcells);


        if numcells>1;
            figure; shadedErrorBar(x,y1a,err1,'lineProps','b'); hold on;shadedErrorBar(x,y2a,err1,'lineProps','k');hold on;shadedErrorBar(x,y3a,err1,'lineProps','r');
            xlim([-.2 1]); title ('Post trials', 'FontSize', 16, 'FontWeight', 'bold');ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            set(gcf,'Color','w');

            yy=ylim;
            figure; errorbar_groups([mean(mean(y2a(:,BEGINo:ENDo))) mean(mean(y1a(:,BEGINo:ENDo))) mean(mean(y3a(:,BEGINo:ENDo)))]',[mean(mean(err2(:,BEGINo:ENDo))) mean(mean(err1(:,BEGINo:ENDo))) mean(mean(err3(:,BEGINo:ENDo)))]', 'bar_colors',[.5 .5 .5; 0 0 1; 1 0 0]);
            ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            set(gcf,'Color','w');
            for statstat=1;
                r1=mean(mean(tmpeff(:,BEGINo:ENDo,redundantnumber+1),3),2);
                if redundantnumber==0;
                    r1=mean(mean(tmpeff(:,BEGINo:ENDo,2:9),3),2)-mean(mean(tmpeff(:,BEGINo-9:BEGINo-1,2:9),3),2);
                end
                c1=mean(mean(tmpeff(:,BEGINo:ENDo,1),3),2);
                d1=mean(mean(tmpeff(:,BEGINo:ENDo,10),3),2);

                [h,p1,ci,stats1]=ttest(r1,c1);
                [h,p2,ci,stats2]=ttest(d1,c1);
                clear simpleeffectsTEST
                simpleeffectsTEST(:,1)=c1;simpleeffectsTEST(:,2)=r1;simpleeffectsTEST(:,3)=d1;
            end
            title(strcat('T_S_S_A','=',num2str(round(stats1.tstat*100)/100),', p=',num2str(round(p1*1000)/1000),'; T_D_D','=',num2str(round(stats2.tstat*100)/100),', p=',num2str(round(p2*1000)/1000)));

        else
            figure; plot(x,y1,'b','LineWidth',3); hold on;plot(x,y2,'k','LineWidth',3); hold on;plot(x,y3,'r','LineWidth',3);
            xlim([-.2 1]); title ('Mismatch Z Scores', 'FontSize', 16, 'FontWeight', 'bold');


        end
        tmpeff_post=tmpeff;
        for manyplotthing=1;
            figure;
            for cnd=1:10;
                y1=mean(tmpeff(:,:,cnd),1);
                y1=y1-min(y1(1,baseBEGIN:baseEND));
                if cnd==1; err1=std(a1,0,1)./sqrt(numcells);
                    err=std(a1,0,1)./sqrt(numcells);

                elseif cnd==10;
                    err=std(a3,0,1)./sqrt(numcells);
                else
                    err=std(a2,0,1)./sqrt(numcells);
                end
                subplot(1,10,cnd); shadedErrorBar(x,y1,err,'lineProps','k'); set(gcf,'Color','w');
                if cnd==1;
                    ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
                    title(titles{cnd}, 'FontSize', 18, 'FontWeight', 'bold');
                else
                    title(titles{cnd}, 'FontSize', 18, 'FontWeight', 'bold'); yticks([]);set(gca,'FontSize',16,'FontWeight','bold');
                end
            end
            for fixaxes=1

                ymin1=1; ymax1=-1;
                for cnd=1:10
                    subplot(1,10,cnd); yy=ylim;
                    if yy(1)<ymin1; ymin1=yy(1); end
                    if yy(2)>ymax1; ymax1=yy(2); end

                end
                for cnd=1:10
                    subplot(1,10,cnd); ylim([ymin1 ymax1]);
                end

            end
        end




    end

    %%%plot average of both

close all
    for plotbOLTH=1;
        effectsallBOTH=effectsall; %(effectsallBUILD+effectsallTEST)/2;
        tmpeff=[];
        for c=1:numcells;
            tmpeff(c,:,:)=effectsallBOTH(newindices(pind(c),1),:,:,(newindices(pind(c),2)));
            for t=1:size(effectsallBOTH,2)
                if redundantnumber>0;
                    mn=mean(mean(effectsallBOTH(newindices(pind(c),1),t,[redundantnumber+1 1 10],newindices(pind(c),2)),3),4);
                    a1(c,t)=mean(effectsallBOTH(newindices(pind(c),1),t,redundantnumber+1,newindices(pind(c),2)),4)-mn;
                else

                    a1tmp=mean(mean(effectsallBOTH(newindices(pind(c),1),t,2:9,newindices(pind(c),2)),3),4);
                    a2tmp=mean(effectsallBOTH(newindices(pind(c),1),t,1,newindices(pind(c),2)),4);
                    a3tmp=mean(effectsallBOTH(newindices(pind(c),1),t,10,newindices(pind(c),2)),4);
                    mn=(a1tmp+a2tmp+a3tmp)/3;
                    a1(c,t)=mean(mean(effectsallBOTH(newindices(pind(c),1),t,2:9,newindices(pind(c),2)),3),4)-mn;
                end
                a2(c,t)=mean(effectsallBOTH(newindices(pind(c),1),t,1,newindices(pind(c),2)),4)-mn;
                a3(c,t)=mean(effectsallBOTH(newindices(pind(c),1),t,10,newindices(pind(c),2)),4)-mn;
            end
        end
        ositmp=[]; difftmp=[];
        for c=1:numcells;
            ositmp(c,1)=osis(newindices(pind(c),1),(newindices(pind(c),2)));
            difftmp(c,1)=odiffs(newindices(pind(c),1),(newindices(pind(c),2)));
        end

        x=timeaxis;
        y1=mean(mean(mean(tmpeff(:,:,redundantnumber+1),3),4),1);
        if redundantnumber==0; y1=mean(mean(mean(tmpeff(:,:,2:9),3),4),1); end;
        y1a=y1-min(y1(1,BEGINo-9:BEGINo-1));
        err1=std(a1,0,1)./sqrt(numcells);

        y2=mean(mean(mean(tmpeff(:,:,1),3),4),1);y2a=y2-min(y2(1,BEGINo-9:BEGINo-1));
        err2=std(a2,0,1)./sqrt(numcells);

        y3=mean(mean(mean(tmpeff(:,:,10),3),4),1); y3a=y3-min(y3(1,BEGINo-9:BEGINo-1));
        err3=std(a3,0,1)./sqrt(numcells);

        mm=min(horzcat(y1a(1,1:BEGINo-1),y2a(1,1:BEGINo-1),y3a(1,1:BEGINo-1)));

        %y1a=y1a-mm;y2a=y2a-mm;y3a=y3a-mm;

        if numcells>1;
            figure; shadedErrorBar(x,y1a,err1,'lineProps','b'); hold on;shadedErrorBar(x,y2a,err2,'lineProps','k');hold on;shadedErrorBar(x,y3a,err3,'lineProps','r');
            xlim([-.2 1]); title ('Average of cell responses', 'FontSize', 16, 'FontWeight', 'bold');ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            set(gcf,'Color','w');

            yy=ylim;
            figure;  errorbar_groups([mean(mean(y2a(:,BEGINo:ENDo))) mean(mean(y1a(:,BEGINo:ENDo))) mean(mean(y3a(:,BEGINo:ENDo)))]',[mean(mean(err2(:,BEGINo:ENDo))) mean(mean(err1(:,BEGINo:ENDo))) mean(mean(err3(:,BEGINo:ENDo)))]', 'bar_colors',[.5 .5 .5; 0 0 1; 1 0 0]);
            ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            set(gcf,'Color','w');
            for statstat=1;
                r1=mean(mean(tmpeff(:,BEGINo:ENDo,redundantnumber+1),3),2)-min(mean(tmpeff(:,BEGINo-9:BEGINo-1,redundantnumber+1),3),[],2);
                if redundantnumber==0;
                    r1=mean(mean(tmpeff(:,BEGINo:ENDo,2:9),3),2)-mean(mean(tmpeff(:,BEGINo-9:BEGINo-1,2:9),3),2);
                end
                c1=mean(mean(tmpeff(:,BEGINo:ENDo,1),3),2)-min(mean(tmpeff(:,BEGINo-9:BEGINo-1,1),3),[],2);
                d1=mean(mean(tmpeff(:,BEGINo:ENDo,10),3),2)-min(mean(tmpeff(:,BEGINo-9:BEGINo-1,10),3),[],2);

                [h,p1,ci,stats1]=ttest(r1,c1);
                [h,p2,ci,stats2]=ttest(d1,c1);
            end
            title(strcat('T_S_S_A','=',num2str(round(stats1.tstat*100)/100),', p=',num2str(round(p1*1000)/1000),'; T_D_D','=',num2str(round(stats2.tstat*100)/100),', p=',num2str(round(p2*1000)/1000)));
        else
            figure; plot(x,y1,'b','LineWidth',3); hold on;plot(x,y2,'k','LineWidth',3); hold on;plot(x,y3,'r','LineWidth',3);yy=ylim;
            xlim([-.2 1]); title ('Average of cell responses', 'FontSize', 16, 'FontWeight', 'bold');
        end

        for manyplotthing=1;
            figure;
            for cnd=1:10;
                y1=mean(tmpeff(:,:,cnd),1);
                y1=y1-mean(y1(1,1:BEGINo-1));
                if cnd==1; err1=std(a2,0,1)./sqrt(numcells);
                    err=std(a2,0,1)./sqrt(numcells);

                elseif cnd==10;
                    err=std(a3,0,1)./sqrt(numcells);
                else
                    err=std(a1,0,1)./sqrt(numcells);
                end
                subplot(1,10,cnd); shadedErrorBar(x,y1,err,'lineProps','k'); set(gcf,'Color','w'); xlim([-.2 .8]);
                if cnd==1;
                    ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
                    title(titles{cnd}, 'FontSize', 18, 'FontWeight', 'bold');
                else
                    title(titles{cnd}, 'FontSize', 18, 'FontWeight', 'bold'); yticks([]);set(gca,'FontSize',16,'FontWeight','bold');
                end
            end
            for fixaxes=1

                ymin1=1; ymax1=-1;
                for cnd=1:10
                    subplot(1,10,cnd); yy=ylim;
                    if yy(1)<ymin1; ymin1=yy(1); end
                    if yy(2)>ymax1; ymax1=yy(2); end

                end
                for cnd=1:10
                    subplot(1,10,cnd); ylim([ymin1 ymax1]);
                end

            end
        end

        for manyplotthing=2;

            if redundantnumber==0;
                figure;
                ccnds={1;2:9;10};
                cccf=[1 5 10];

                colars={'k' 'b' 'r'};
                for cnd=1:3;
                    y1=mean(mean(tmpeff(:,:,ccnds{cnd}),3),1);
                    y1=y1-mean(y1(1,BEGINo-9:BEGINo-1));
                    if cnd==1; err1=std(a2,0,1)./sqrt(numcells/2);
                        err=std(a2,0,1)./sqrt(numcells/2);

                    elseif cnd==3;
                        err=std(a3,0,1)./sqrt(numcells/2);
                    else
                        err=std(a1,0,1)./sqrt(numcells/2);
                    end
                    subplot(1,3,cnd); shadedErrorBar(x,y1,err,'lineProps',colars{cnd}); set(gcf,'Color','w'); xlim([-.2 .8]);
                    if cnd==1;
                        ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
                        title(titles{cccf(cnd)}, 'FontSize', 18, 'FontWeight', 'bold');
                        cont1=y1./max(y1); cont1err=err; y11=y1;
                    else
                        if cnd==2;
                            title(titles{5}, 'FontSize', 18, 'FontWeight', 'bold'); yticks([]);set(gca,'FontSize',16,'FontWeight','bold');
                            rdnt1=y1./max(y11); rdnt1err=err;
                        else
                            title(titles{cccf(cnd)}, 'FontSize', 18, 'FontWeight', 'bold'); yticks([]);set(gca,'FontSize',16,'FontWeight','bold');
                            dev1=y1./max(y11); dev1err=err;

                        end
                    end
                end
                for fixaxes=1

                    ymin1=1; ymax1=-1;
                    for cnd=1:3
                        subplot(1,3,cnd); yy=ylim;
                        if yy(1)<ymin1; ymin1=yy(1); end
                        if yy(2)>ymax1; ymax1=yy(2); end

                    end
                    for cnd=1:3
                        subplot(1,3,cnd); ylim([ymin1 ymax1]);
                    end

                end
            else
                figure;
                ccnds=[1 redundantnumber+1 10];

                colars={'k' 'b' 'r'};
                for cnd=1:3;
                    y1=mean(mean(tmpeff(:,:,ccnds(cnd)),3),1);
                    y1=y1-mean(y1(1,BEGINo-9:BEGINo-1));
                    if cnd==1; err1=std(a1,0,1)./sqrt(numcells/2);
                        err=std(a1,0,1)./sqrt(numcells/2);

                    elseif cnd==3;
                        err=std(a3,0,1)./sqrt(numcells/2);
                    else
                        err=std(a2,0,1)./sqrt(numcells/2);
                    end
                    subplot(1,3,cnd); shadedErrorBar(x,y1,err,'lineProps',colars{cnd}); set(gcf,'Color','w'); xlim([-.2 .8]);
                    if cnd==1;
                        ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
                        title(titles{ccnds(cnd)}, 'FontSize', 18, 'FontWeight', 'bold');
                        cont1=y1./max(y1); cont1err=err; y11=y1;
                    else
                        if cnd==2;
                            title(titles{5}, 'FontSize', 18, 'FontWeight', 'bold'); yticks([]);set(gca,'FontSize',16,'FontWeight','bold');
                            rdnt1=y1./max(y11); rdnt1err=err;
                        else
                            title(titles{ccnds(cnd)}, 'FontSize', 18, 'FontWeight', 'bold'); yticks([]);set(gca,'FontSize',16,'FontWeight','bold');
                            dev1=y1./max(y11); dev1err=err;
                        end
                    end
                end
                for fixaxes=1

                    ymin1=1; ymax1=-1;
                    for cnd=1:3
                        subplot(1,3,cnd); yy=ylim;
                        if yy(1)<ymin1; ymin1=yy(1); end
                        if yy(2)>ymax1; ymax1=yy(2); end

                    end
                    for cnd=1:3
                        subplot(1,3,cnd); ylim([ymin1 ymax1]);
                    end

                end
            end
        end
        figure; shadedErrorBar(x,rdnt1,rdnt1err,'lineProps','b'); hold on;shadedErrorBar(x,cont1,cont1err,'lineProps','k');hold on;shadedErrorBar(x,dev1,dev1err,'lineProps','r');
        xlim([-.2 1]); title ('Average of cell responses; cont normalized', 'FontSize', 16, 'FontWeight', 'bold');ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('time (sec)','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
        set(gcf,'Color','w');



    end



    cols3=cols-cols;
    try
        DDs1=simpleeffectsPRE(:,3)-(mean(simpleeffectsPRE(:,1),2));DDs2=simpleeffectsPOST(:,3)-(mean(simpleeffectsPOST(:,1),2));
        DDs3=simpleeffectsPRE(:,3)-(mean(simpleeffectsPRE(:,2),2));DDs4=simpleeffectsPOST(:,3)-(mean(simpleeffectsPOST(:,2),2));
        sm=(DDs1>1.67)+(DDs2>1.67)+(DDs3>.1)+(DDs4>.1);%
        figure;
        ax = gca();
        pieData = [mean(sm==4) mean(sm~=4)];
        h = pie(ax, pieData);
        newColors = [...
            1,       0, 0;
            .5,       .5,       0.5];
        ax.Colormap = newColors;
        colDD=cols-cols;
        for c=1:size(sm,1)
            if sm(c,1)==4;
                colDD(c,1)=1;
            end
        end
        title(num2str(mean(sm~=4)));
        %     for determiningcombinedPvals=1;
        %         pp=1;
        %         for p1=.01:.01:1;
        %             pvals(pp)=chi2pdf((-2)*((log(p1)+log(p1))),4);pp=1+pp;
        %         end
        %         figure; plot(.01:.01:1,pvals);
        %     end
    end
    %axis(sctax)

    figure; scatter((.5*simpleeffects(:,1)+.5*simpleeffects(:,3))-simpleeffects(:,2),simpleeffects(:,3)-simpleeffects(:,1),50,colDD,'fill'); title ('Cell responses', 'FontSize', 20, 'FontWeight', 'bold');
    ylabel('Deviance Detection (Z)','FontSize',18,'FontWeight','bold'); xlabel('Stim specific adaptation (Z)','FontSize',18,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');set(gcf,'Color','w');
    SSAs=(.5*simpleeffects(:,1)+.5*simpleeffects(:,3))-simpleeffects(:,2);DDs=(simpleeffects(:,3)-simpleeffects(:,1));
    axis([-15 30 -15 30]);

    %
    figure; scatter((.5*simpleeffectsTEST(:,1)+.5*simpleeffectsTEST(:,3))-simpleeffectsTEST(:,2),simpleeffectsTEST(:,3)-simpleeffectsTEST(:,1),50,cols,'fill'); title ('Cell responses', 'FontSize', 20, 'FontWeight', 'bold');
    ylabel('Deviance Detection (Z)','FontSize',18,'FontWeight','bold'); xlabel('Stim specific adaptation (Z)','FontSize',18,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');set(gcf,'Color','w');
    figure; scatter((.5*simpleeffects(:,1)+.5*simpleeffects(:,3))-simpleeffects(:,2),simpleeffects(:,3)-simpleeffects(:,1),50,cols,'fill'); title ('Cell responses', 'FontSize', 20, 'FontWeight', 'bold');
    ylabel('Deviance Detection (Z)','FontSize',18,'FontWeight','bold'); xlabel('Stim specific adaptation (Z)','FontSize',18,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');set(gcf,'Color','w');

    %
    %
    difftmp1=difftmp;
    DDs=((simpleeffectsPRE(:,3)-simpleeffectsPRE(:,1))+(simpleeffectsPOST(:,3)-simpleeffectsPOST(:,1)))/2;%
    ocut=.2;
    difftmp=(difftmp1); cols2=cols3(ositmp>ocut,:); for c=1:3;  cols2(:,c)=cols2(:,c)+(1-(ositmp(ositmp>ocut))/max(ositmp)); end
    cols2=colDD(ositmp>ocut,:);
    figure; scatter(difftmp(ositmp>ocut).*28.6,DDs(ositmp>ocut),40,cols2,'fill'); ylabel('dev detection (Z)','FontSize',18,'FontWeight','bold'); xlabel('diff btw deviant and stim"s preferred angle (deg)','FontSize',18,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');set(gcf,'Color','w');


    hold on
    Fit = polyfit(difftmp(ositmp>ocut).*28.6,DDs(ositmp>ocut),10); % x = x data, y = y data, 1 = order of the polynomial i.e a straight line
    %plot(-90:90,polyval(Fit,-90:90));
    %r=corrcoef(difftmp(ositmp>ocut).*28.6,SSAs(ositmp>ocut)); title(strcat('r=',num2str(r(2,1))));
    a=difftmp(ositmp>ocut).*28; a1=a(abs(a)<15); b=DDs(ositmp>ocut); b1=b(abs(a)<15); a2=a(abs(a)>15);b2=b(abs(a)>15);
    [h,p,ci,stats]=ttest2(b1,b2)
    title(strcat('tval=',num2str(stats.tstat),'. pval=',num2str(p)))

    cols2=cols3(ositmp>0,:);
    figure; scatter(ositmp,DDs,40,colDD,'fill'); ylabel('Deviance Detection (Z)','FontSize',18,'FontWeight','bold'); xlabel('OSI','FontSize',18,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');set(gcf,'Color','w');
    corrcoef((ositmp),DDs)

    SSAs=((.5*simpleeffects(:,1)+.5*simpleeffects(:,1))-simpleeffects(:,2))./((.5*simpleeffects(:,1)+.5*simpleeffects(:,1)));DDs=(simpleeffects(:,3)-simpleeffects(:,1));
    SSAs=simpleeffectsPOST(:,2);
    SSAs=(simpleeffects(:,1)-simpleeffects(:,2));%./abs(simpleeffects(:,1)+simpleeffects(:,2));
    %SSAs=((.5*simpleeffects(:,1)+.5*simpleeffects(:,3))-simpleeffects(:,2));
    ocut=.2;
    difftmp=(difftmp1); cols2=cols3(ositmp>ocut,:); for c=1:3;  cols2(:,c)=cols2(:,c)+(1-(ositmp(ositmp>ocut))/max(ositmp)); end
    figure; scatter(difftmp(ositmp>ocut).*28.6,SSAs(ositmp>ocut),40,cols2,'fill'); ylabel('Stimulus specific adaptation (Z)','FontSize',18,'FontWeight','bold'); xlabel('diff btw redundant and stim"s preferred angle (deg)','FontSize',18,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');set(gcf,'Color','w');
    hold on
    %Fit = polyfit(difftmp(ositmp>ocut).*28.6,SSAs(ositmp>ocut),10); % x = x data, y = y data, 1 = order of the polynomial i.e a straight line
    %plot(-90:90,polyval(Fit,-90:90));
    %r=corrcoef(difftmp(ositmp>ocut).*28.6,SSAs(ositmp>ocut)); title(strcat('r=',num2str(r(2,1))));
    a=difftmp(ositmp>ocut).*28; a1=a(abs(a)<15); b=SSAs(ositmp>ocut); b1=b(abs(a)<15); a2=a(abs(a)>15);b2=b(abs(a)>15);
    [h,p,ci,stats]=ttest2(b1,b2)
    title(strcat('tval=',num2str(stats.tstat),'. pval=',num2str(p)))

    cols2=cols3(ositmp>0,:);
    figure; scatter((ositmp(ositmp>0)),SSAs(ositmp>0),40,cols2,'fill'); ylabel('stim specific adaptation (Z)','FontSize',18,'FontWeight','bold'); xlabel('OSI','FontSize',18,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');set(gcf,'Color','w');
    corrcoef((ositmp),SSAs)


   % sortby=[1 redundantnumber+1 10];
   % BOOSTscalefactor=88; %between 0 and 100. higher is brighter
    %figure;

    for rasters=1

%         effects1=tmpeff;
%         for c=1:size(tmpeff,1);
%             for cnd=1:10;
%                 m=mean(effects1(c,BEGINo-9:BEGINo-1,cnd));
%                 effects1(c,:,cnd)=effects1(c,:,cnd)-m;
%             end
%         end
%         ak1=mean(effects1(:,:,sortby),3); [ix,b]=sort(mean(ak1(:,BEGINo:ENDo),2),'descend');
%         effects2=effects1(b,:,:);
% 
%         for z1=1:size(effects2,3)
%             subplot(1,size(effects2,3),z1); imagesc(timeaxis,1:size(effects2,1),effects2(:,:,z1)); colormap('Gray'); set(gca,'FontSize',14,'FontWeight','bold');
%             xlim([-.2 1]);
%             title(titles{z1});
% 
%             if z1==1; xlabel('sec','FontSize',14,'FontWeight','bold');
%                 ylabel('neurons','FontSize',14,'FontWeight','bold');
%             end
%         end
%         for settingcolorscale=1;
%             scalefactor=(BOOSTscalefactor/100); if scalefactor==1; scalefactor=.99; end
%             tmpmax=0; tmpmin=0;
%             for z1=1:size(effects2,3)
%                 subplot(1,size(effects2,3),z1);
%                 cb=caxis; if cb(1)<tmpmin; tmpmin=cb(1); end
%                 if cb(2)>tmpmax; tmpmax=cb(2); end
%             end
%             for z1=1:size(effects2,3)
%                 subplot(1,size(effects2,3),z1);
%                 cb=[tmpmin tmpmax];
%                 caxis(cb*(1-scalefactor))
%             end
%         end
    end

    for limited_rasters=1;

        sortby=[1 redundantnumber+1 10];
        sortby2=[1 10];
        BOOSTscalefactor=77; %between 0 and 100. higher is brighter
        figure;
        titles2={'control';'rdnt';'deviant'};
        for rasters=1

            effects1=tmpeff;
            ak1=mean(effects1(:,:,sortby2),3); [ix,b]=sort(mean(ak1(:,BEGINo:ENDo),2),'descend');
            %ak1=mean(effects1(:,:,sortby(:)),3); [mmm,idx]=max(ak1(:,BEGINo:ENDo),[],2); [ix,b]=sort(idx);
            effects2=effects1(b,:,:);

            for z1=1:size(sortby,2)
                subplot(1,size(sortby,2),z1); imagesc(timeaxis,1:size(effects2,1),effects2(:,:,sortby(z1))); colormap('gray'); set(gca,'FontSize',14,'FontWeight','bold');
                xlim([-.2 1]);
                title(titles2{z1});

                if z1==1; xlabel('sec','FontSize',14,'FontWeight','bold');
                    ylabel('neurons','FontSize',14,'FontWeight','bold');
                end
            end
            for settingcolorscale=1;
                scalefactor=(BOOSTscalefactor/100); if scalefactor==1; scalefactor=.99; end
                tmpmax=0; tmpmin=0;
                for z1=1:size(sortby,2)
                    subplot(1,size(sortby,2),z1);
                    cb=caxis; if cb(1)<tmpmin; tmpmin=cb(1); end
                    if cb(2)>tmpmax; tmpmax=cb(2); end
                end
                for z1=1:size(sortby,2)
                    subplot(1,size(sortby,2),z1);
                    cb=[tmpmin tmpmax];
                    caxis(cb*(1-scalefactor))

                   % caxis([-.7 3]);
                end
            end
        end
    end

    make_eps_saveable
    
    fc=[579,394,1648,880 ]

    set(gcf,'Position',fc);
end

step1=.02; cmprb=[]; kc=1;
for t=0:step1:1;
    cmprb(kc,1)=sum(ositmp<t)/size(ositmp,1);
    kc=1+kc;
end
figure; plot(0:step1:1,cmprb,'Color','k','LineWidth',2);
ylabel('proportion of cells','FontSize',18,'FontWeight','bold'); xlabel('OSI','FontSize',18,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');set(gcf,'Color','w');
title('cumulative probability plot');  disp('mean osi')
mean(unique(ositmp))
std(unique(ositmp))
disp(strcat('prop. selective',num2str(mean(unique(ositmp)>.2))));

for plotting_by_preferred_orientation=1;
    ocut=.2;
    xval=difftmp(ositmp>ocut).*28.6;
    dat1=(simpleeffectsPRE(ositmp>ocut,1)+simpleeffectsPOST(ositmp>ocut,1))/2;
    dat2=(simpleeffectsPRE(ositmp>ocut,2)+simpleeffectsPOST(ositmp>ocut,2))/2;
    dat3=(simpleeffectsPRE(ositmp>ocut,3)+simpleeffectsPOST(ositmp>ocut,3))/2;
    bn=-90:36:90; clear bmean mns1 mns2 mns3 sts1 sts2 sts3
    for bin=1:5; bmean(bin)=mean(bn(bin:bin+1)); tmp1=dat1(and(xval(:)>bn(bin),xval(:)<bn(bin+1)),1); mns1(bin,1)=mean(tmp1);sts1(bin,1)=std(tmp1)./sqrt(size(tmp1,1)); end
    for bin=1:5; bmean(bin)=mean(bn(bin:bin+1)); tmp1=dat2(and(xval(:)>bn(bin),xval(:)<bn(bin+1)),1); mns2(bin,1)=mean(tmp1);sts2(bin,1)=std(tmp1)./sqrt(size(tmp1,1)); end
    for bin=1:5; bmean(bin)=mean(bn(bin:bin+1)); tmp1=dat3(and(xval(:)>bn(bin),xval(:)<bn(bin+1)),1); mns3(bin,1)=mean(tmp1);sts3(bin,1)=std(tmp1)./sqrt(size(tmp1,1)); end
    figure; errorbar(bmean,mns1,sts2,'k'); hold on;
    errorbar(bmean,mns2,sts2,'b'); hold on
    errorbar(bmean,mns3,sts2,'r');
    dat1=(simpleeffectsPRE(ositmp<ocut,1)+simpleeffectsPOST(ositmp<ocut,1))/2;
    dat2=(simpleeffectsPRE(ositmp<ocut,2)+simpleeffectsPOST(ositmp<ocut,2))/2;
    dat3=(simpleeffectsPRE(ositmp<ocut,3)+simpleeffectsPOST(ositmp<ocut,3))/2;
    figure;  errorbar_groups([mean(dat1) mean(dat2) mean(dat3)]',[std(dat1)./sqrt(size(dat1,1)) std(dat2)./sqrt(size(dat2,1)) std(dat3)./sqrt(size(dat3,1))]', 'bar_colors',[.5 .5 .5; 0 0 1; 1 0 0]);
    ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('stim context','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
    set(gcf,'Color','w');

    for dotimeplots=0;
        if dotimeplots==1;

            figure;
            xval=difftmp(ositmp>ocut).*28.6;
            dat1=(tmpeff(ositmp>ocut,:,1));
            dat2=(tmpeff(ositmp>ocut,:,redundantnumber+1));
            dat3=(tmpeff(ositmp>ocut,:,10));
            dat1a=dat1(and(xval(:)>-25,xval(:)<25),:); bs1=mean(mean(dat1a(:,baseEND-5:baseEND)));
            dat2a=dat2(and(xval(:)>-25,xval(:)<25),:); bs2=mean(mean(dat2a(:,baseEND-5:baseEND)));
            dat3a=dat3(and(xval(:)>-25,xval(:)<25),:); bs3=mean(mean(dat3a(:,baseEND-5:baseEND)));
            subplot(1,3,1); shadedErrorBar(x,mean(dat1a)-bs1,std(dat1a,0,1)./sqrt(size(dat1a,1)),'lineProps','k'); hold on;
            shadedErrorBar(x,mean(dat2a)-bs2,std(dat2a,0,1)./sqrt(size(dat2a,1)),'lineProps','b'); hold on;
            shadedErrorBar(x,mean(dat3a)-bs3,std(dat3a,0,1)./sqrt(size(dat3a,1)),'lineProps','r');
            xlim([-.2 1]); ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('preferred orientation','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            set(gcf,'Color','w');
            axlim1=axis
            dat1b=dat1(or(xval(:)<-25,xval(:)>25),:);bs1=mean(mean(dat1b(:,baseEND-5:baseEND)));
            dat2b=dat2(or(xval(:)<-25,xval(:)>25),:);bs2=mean(mean(dat2b(:,baseEND-5:baseEND)));
            dat3b=dat3(or(xval(:)<-25,xval(:)>25),:);bs3=mean(mean(dat3b(:,baseEND-5:baseEND)));
            subplot(1,3,2); shadedErrorBar(x,mean(dat1b)-bs1,std(dat1b,0,1)./sqrt(size(dat1b,1)),'lineProps','k'); hold on;
            shadedErrorBar(x,mean(dat2b)-bs2,std(dat2b,0,1)./sqrt(size(dat2b,1)),'lineProps','b'); hold on;
            shadedErrorBar(x,mean(dat3b)-bs3,std(dat3b,0,1)./sqrt(size(dat3b,1)),'lineProps','r');
            xlim([-.2 1]); ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('non-preferred orientation','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            set(gcf,'Color','w');

            dat1c=(tmpeff(ositmp<ocut,:,1)); bs1=mean(mean(dat1c(:,baseEND-5:baseEND)));
            dat2c=(tmpeff(ositmp<ocut,:,redundantnumber+1)); bs2=mean(mean(dat2c(:,baseEND-5:baseEND)));
            dat3c=(tmpeff(ositmp<ocut,:,10)); bs3=mean(mean(dat3c(:,baseEND-5:baseEND)));
            subplot(1,3,3); shadedErrorBar(x,mean(dat1c)-bs1,std(dat1c,0,1)./sqrt(size(dat1c,1)),'lineProps','k'); hold on;
            shadedErrorBar(x,mean(dat2c)-bs2,std(dat2c,0,1)./sqrt(size(dat2c,1)),'lineProps','b'); hold on;
            shadedErrorBar(x,mean(dat3c)-bs3,std(dat3c,0,1)./sqrt(size(dat3c,1)),'lineProps','r');
            xlim([-.2 1]); ylabel('z-scores','FontSize',16,'FontWeight','bold'); xlabel('non-selective','FontSize',16,'FontWeight','bold'); set(gca,'FontSize',16,'FontWeight','bold');
            set(gcf,'Color','w');
            for fixaxes=1

                ymin1=1; ymax1=-1;
                for cnd=1:3
                    subplot(1,3,cnd); yy=ylim;
                    if yy(1)<ymin1; ymin1=yy(1); end
                    if yy(2)>ymax1; ymax1=yy(2); end

                end
                for cnd=1:3
                    subplot(1,3,cnd); ylim([ymin1 ymax1]);
                end

            end
        else
            xval=difftmp(ositmp>ocut).*28.6;
            dat1=(tmpeff(ositmp>ocut,:,1));
            dat2=(tmpeff(ositmp>ocut,:,redundantnumber+1));
            dat3=(tmpeff(ositmp>ocut,:,10));
            dat1a=dat1(and(xval(:)>-20,xval(:)<20),:); bs1=mean(mean(dat1a(:,baseBEGIN:baseEND)));
            dat2a=dat2(and(xval(:)>-20,xval(:)<20),:); bs2=mean(mean(dat2a(:,baseBEGIN:baseEND)));
            dat3a=dat3(and(xval(:)>-20,xval(:)<20),:); bs3=mean(mean(dat3a(:,baseBEGIN:baseEND)));
            d1a=mean(dat1a(:,BEGINo:ENDo)-bs1,2); d2a=mean(dat2a(:,BEGINo:ENDo)-bs2,2); d3a=mean(dat3a(:,BEGINo:ENDo)-bs3,2);



            dat1b=dat1(or(xval(:)<-20,xval(:)>20),:);bs1=mean(mean(dat1b(:,baseBEGIN:baseEND)));
            dat2b=dat2(or(xval(:)<-20,xval(:)>20),:);bs2=mean(mean(dat2b(:,baseBEGIN:baseEND)));
            dat3b=dat3(or(xval(:)<-20,xval(:)>20),:);bs3=mean(mean(dat3b(:,baseBEGIN:baseEND)));
            d1b=mean(dat1b(:,BEGINo:ENDo)-bs1,2); d2b=mean(dat2b(:,BEGINo:ENDo)-bs2,2); d3b=mean(dat3b(:,BEGINo:ENDo)-bs3,2);


            dat1c=(tmpeff(ositmp<ocut,:,1)); bs1=mean(mean(dat1c(:,baseBEGIN:baseEND)));
            dat2c=(tmpeff(ositmp<ocut,:,redundantnumber+1)); bs2=mean(mean(dat2c(:,baseBEGIN:baseEND)));
            dat3c=(tmpeff(ositmp<ocut,:,10)); bs3=mean(mean(dat3c(:,baseBEGIN:baseEND)));
            d1c=mean(dat1c(:,BEGINo:ENDo)-bs1,2); d2c=mean(dat2c(:,BEGINo:ENDo)-bs2,2); d3c=mean(dat3c(:,BEGINo:ENDo)-bs3,2);


            figure;  errorbar_groups([mean(d1a) mean(d2a) mean(d3a); mean(d1b) mean(d2b) mean(d3b); mean(d1c) mean(d2c) mean(d3c)]',[std(d1a)./sqrt(size(d1a,1)) std(d2a)./sqrt(size(d2a,1)) std(d3a)./sqrt(size(d3a,1)); std(d1b)./sqrt(size(d1b,1)) std(d2b)./sqrt(size(d2b,1)) std(d3b)./sqrt(size(d3b,1)); std(d1c)./sqrt(size(d1c,1)) std(d2c)./sqrt(size(d2c,1)) std(d3c)./sqrt(size(d3c,1))]','bar_colors',[.5 .5 .5; 0 0 1; 1 0 0]);
            ylabel('z-scores','FontSize',16,'FontWeight','bold'); xticklabels({strcat('preferred. n=',num2str(size(d1a,1))),strcat('non-preferred. n=',num2str(size(d1b,1))),strcat('non-selective. n=',num2str(size(d1c,1)))})
            % anova_rm({horzcat(d1a,d2a,d3a) horzcat(d1b,d2b,d3b) horzcat(d1c,d2c,d3c)}) %orientation pref by contidion anova
            anova_rm({horzcat(d1a./mean(d1a),d3a./mean(d1a)) horzcat(d1b./mean(d1b),d3b./mean(d1b)) horzcat(d1c./mean(d1c),d3c./mean(d1c))}) %dd only by ori pref
            anova_rm({horzcat(d1a./mean(d1a),d2a) horzcat(d1b./mean(d1b),d2b) horzcat(d1c./mean(d1c),d2c)}) %ssa only by ori pref
            % anova_rm(horzcat(d1a,d3a))%dev detect for pref
            % anova_rm(horzcat(d1b,d3b))%dev detect for non-pref
            % anova_rm(horzcat(d1c,d3c))%dev detect for no osi
            [h,p,ci,stats]=ttest(d1a,d3a,'Tail','left'); disp(strcat('dev detect for preferred: t=',num2str(stats.tstat),'pval=',num2str(p)));
            [h,p,ci,stats]=ttest(d1b,d3b,'Tail','left'); disp(strcat('dev detect for non-preferred: t=',num2str(stats.tstat),'pval=',num2str(p)));
            [h,p,ci,stats]=ttest(d1c,d3c,'Tail','left'); disp(strcat('dev detect for non-selective: t=',num2str(stats.tstat),'pval=',num2str(p)));
            [h,p,ci,stats]=ttest(d1a,d2a,'Tail','right'); disp(strcat('SSA for preferred: t=',num2str(stats.tstat),'pval=',num2str(p)));
            [h,p,ci,stats]=ttest(d1b,d2b,'Tail','right'); disp(strcat('SSA for non-preferred: t=',num2str(stats.tstat),'pval=',num2str(p)));
            [h,p,ci,stats]=ttest(d1c,d2c,'Tail','right'); disp(strcat('SSA for non-selective: t=',num2str(stats.tstat),'pval=',num2str(p)));
            % [h,p,ci,stats]=ttest(d1c,d2c); dispa(dd
            for statsts=1;
                data=horzcat(vertcat(d1a,d1b,d1c),vertcat(d2a,d2b,d2c),vertcat(d3a,d3b,d3c)); %for inter cell anova
            end
        end

    end
end


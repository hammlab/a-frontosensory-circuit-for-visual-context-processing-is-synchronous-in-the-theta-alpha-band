
clcl
rednumber=4; %redundant to analyse. i used 4 for main fig; 
limtrials=10; %limit trials in all conditions to this. if 0, it wont do it;
lim2dev=0; %see line 75ish

for fileload=1;
    matchy=0; 

    files1={''}
    oris=[5 6; 5 6; 1 2; 5 6; 1 2; 5 6;  5 6;  5 6; 5 6;  3 4; 5 6;  3 4; 3 4; 5 6];
     files2={'';};%;
end
 
%% do control
for makematrices=1;
    erspsV1=[];
    itcsV1=[];
    erspsPFC=[];
    itcsPFC=[];
    cohers=[];
    phases=[];
    lagalls=[];
    coherTIME_cntrl=zeros(100,350,size(files1,1));
    coherTIME_cntrl_smooth=zeros(100,350,size(files1,1));
    time3=79;
    basetime1a=23; time4=87;
end
%% do control
for makematrices=1;
    erspsV1_control=[];
    itcsV1_control=[];
    erspsPFC_control=[];
    itcsPFC_control=[];
    ERP_V1_control=[];
    ERP_PFC_control=[];
    powers_control=[];
    cohers_control=[];
    phases=[];
    lagalls=[];
    coherTIME_cntrl=zeros(100,350,size(files1,1));
    coherTIME_cntrl_smooth=zeros(100,350,size(files1,1));
    time3=79;
    phaseampsCNTpfc=zeros(100,14,8,size(files1,1));
    phaseampsCNTv1=zeros(100,14,8,size(files1,1));
end
%control
for fila=1:size(files1,1); 
    if lim2dev==1; load(files2{fila},'trials1','rej');trials1=trials1(:,rej==0); limtrials=sum(trials1==2); clear trials1 rej; end
    
%     if rem(fila,2)==0
%         ori1=1; ori2=2;
%     else
%         ori1=5; ori2=6;
%     end
    ori1=oris(fila,1); ori2=oris(fila,2);

        load(files1{fila}); 
    if size(trials1,2)>250;
    %   trials1(1,251:end)=zeros(1,size(trials1,2)-250);
    end
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0); ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
     trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%
     
     powtmp=zeros(1,100);
     for tr=1:size(erp1,1)
         han=hanning(1000)';
         a=fft(erp1(tr,175:1174).*han);
         powtmp=powtmp+abs(a(1,1:100));
     end
     powers_control(:,fila,1)=powtmp./tr;
      
     powtmp=zeros(1,100);
     for tr=1:size(erp2,1);
         han=hanning(1000)';
         a=fft(erp2(tr,175:1174).*han);
         powtmp=powtmp+abs(a(1,1:100));
     end
     powers_control(:,fila,2)=powtmp./tr;    
     
        
    %for limiting trials by manual definitiion
    if limtrials>0;
        counta=0;
        for t=0:(size(trials1,2)-1);
            if (counta>(limtrials))
                trials1(1,size(trials1,2)-t)=0;
            elseif or(trials1(1,size(trials1,2)-t)==ori1,trials1(1,size(trials1,2)-t)==ori2)
                counta=1+counta;
            end
        end
    end
    
    tr=size(trials1,2); cut=round(tr/2);
    % trials1(1,1:50)=zeros(1,50);%
    %trials1(1,1:125)=zeros(1,125); %exclude first half of trials


    aa=ersp1(:,:,or(trials1==ori1,trials1==ori2));
    erspsV1_control(:,:,fila)=mean(aa(:,:,:),3);

    ERP_V1_control(:,fila)=mean(erp1(or(trials1==ori1,trials1==ori2),:),1);

%     for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end    
    aa=ersp2(:,:,or(trials1==ori1,trials1==ori2));
    erspsPFC_control(:,:,fila)=mean(aa(:,:,:),3);

    ERP_PFC_control(:,fila)=mean(erp2(or(trials1==ori1,trials1==ori2),:),1);
           
    tr=sum(or(trials1==ori1,trials1==ori2));
    tfs=tfs1(:,:,or(trials1==ori1,trials1==ori2));
    itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
    itcsV1_control(:,:,fila)=itc;
    
    tfs=tfs2(:,:,or(trials1==ori1,trials1==ori2));
    itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
    itcsPFC_control(:,:,fila)=itc;
    
   
        tfs1a=tfs1;tfs2a=tfs2; 
    tfs1=tfs1(:,:,and(trials1>0,trials1<9));
    tfs2=tfs2(:,:,and(trials1>0,trials1<9));
    clear coher
    for calculatecoherencebetweens=1;
        
        for f=1:size(f_sG1,2);
            for t=1:100;
                lags=0; cnt=0;
                for tr=1:size(tfs1,3);
                    l1=tfs1(f,t,tr)./abs(tfs1(f,t,tr));
                    l2=tfs2(f,t,tr)./abs(tfs2(f,t,tr));
                    lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                    cnt=cnt+1;
                end
                coher(f,t)=abs(lags/cnt)-(sqrt(-(1/cnt)*log(.5)));
            end
        end
    end
    cohers_control(:,:,fila)=coher-cohersham;

    tfs1=tfs1a(:,:,or(trials1==ori1,trials1==ori2));
    tfs2=tfs2a(:,:,or(trials1==ori1,trials1==ori2));
    %coherence across all timepoints
    clear coher
    for calculatecoherencebetweens=1        
        for f=1:size(f_sG1,2)
            lags=0; cnt=0;
            for t=time1:time4
                for tr=1:size(tfs1,3)
                    l1=tfs1(f,t,tr)./abs(tfs1(f,t,tr));
                    l2=tfs2(f,t,tr)./abs(tfs2(f,t,tr));
                    lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                    cnt=cnt+1;
                end
            end
            coher(f)=abs(lags/cnt);%-(sqrt(-(1/cnt)*log(.5)));
        end
    end
    cohers_control_alltimes(:,fila)=coher;%-cohersham;
    
    clear coher
    for calculatecoherencebetweens=1;
        tfs1=tfs1a; tfs2=tfs2a;
        for f=1:size(f_sG1,2);
            for t=1:100;
                lags=0; cnt=0;
                for tr=1:size(tfs1,3)
                    l1=tfs1(f,t,tr)./abs(tfs1(f,t,tr));
                    l2=tfs2(f,t,tr)./abs(tfs2(f,t,tr));
                    lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                    cnt=cnt+1;
                end
                coher(f,t)=abs(lags/cnt)-(sqrt(-(1/cnt)*log(.5)));
            end
        end
    end
    cohers_allC(:,:,fila)=coher-cohersham;
    cohershams(:,:,fila)=cohersham;
    
    %coherence in pre and post baseline
    for pre_and_post_baseline_coher=1;
        load(strcat(files1{fila},'_withbase'),'tfs1_prebase','tfs1_postbase','tfs2_prebase','tfs2_postbase');
        tfs1base1=tfs1_prebase; tfs1base2=tfs1_postbase;
        tfs2base1=tfs2_prebase; tfs2base2=tfs2_postbase;
        load(strcat(files2{fila},'_withbase'),'tfs1_prebase','tfs1_postbase','tfs2_prebase','tfs2_postbase');
        tfs1base3=tfs1_prebase; tfs1base4=tfs1_postbase;
        tfs2base3=tfs2_prebase; tfs2base4=tfs2_postbase;
        
        clear coher
        for calculatecoherencebetweens=1;
            for f=1:size(f_sG1,2);
                lags=0; cnt=0;
                for t=1:100;
                    for tr=1:size(tfs1base1,3)
                         l1=tfs1base1(f,t,tr)./abs(tfs1base1(f,t,tr));
                         l2=tfs2base1(f,t,tr)./abs(tfs2base1(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                         cnt=cnt+1;
                    end
%                     for tr=1:size(tfs1base2,3);
%                        l1=tfs1base2(f,t,tr)./abs(tfs1base2(f,t,tr));
%                        l2=tfs2base2(f,t,tr)./abs(tfs2base2(f,t,tr));
%                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
%                        cnt=cnt+1;
%                     end
                    for tr=1:size(tfs1base3,3)
                       l1=tfs1base3(f,t,tr)./abs(tfs1base3(f,t,tr));
                       l2=tfs2base3(f,t,tr)./abs(tfs2base3(f,t,tr));
                       lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                       cnt=cnt+1;
                    end
%                     for tr=1:size(tfs1base4,3);
%                        l1=tfs1base4(f,t,tr)./abs(tfs1base4(f,t,tr));
%                        l2=tfs2base4(f,t,tr)./abs(tfs2base4(f,t,tr));
%                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
%                        cnt=cnt+1;
%                     end
                end
                coher(f)=abs(lags/cnt)-(sqrt(-(1/cnt)*log(.5)));
            end
        end
        cohers_nostim_alltimes(:,fila)=coher;%-cohersham;
    end
  
    
    %basecorrect
    for f=1:size(cohers_control,1);
%         bb=mean(cohers(f,basetime1:basetime2,fila));
%         for t=1:size(cohers,2);
%             cohers(f,t,fila)=cohers(f,t,fila)-bb;
%         end
    end
 
    for calculate_phase_amp_couplin=1;
%          
%          tfs1x=tfs1a(:,:,1:200); tfs2x=tfs2a(:,:,1:200); 
%          phaseamp1=zeros(100,14,8,size(tfs1x,3));
%          phaseamp2=zeros(100,14,8,size(tfs1x,3));
% %          for laga=0:4
%              for phzfreq=1:14
%                  for tr=1:size(tfs1x,3)
%                      for f=25:100
%                          amp1=0;  cnt=0; lags1=0; lags2=0;
%                           for t=time1:100
%                               l1=angle(tfs2x(phzfreq,t-laga,tr));
%                               l2=abs(tfs1x(f,t,tr));                            
%                               lags2=lags2+(l2*exp(1i*l1));
%                               if laga==0
%                                 l0=angle(tfs1x(phzfreq,t-laga,tr));
%                                 lags1=lags1+(l2*exp(1i*l0));  
%                               end
%                               amp1=amp1+l2;
%                               cnt=cnt+1;
%                           end                          
%                           if laga==0
%                               phaseamp1(f,phzfreq,laga+1,tr)=abs(lags1/cnt)/(amp1/cnt);
%                           end
%                           phaseamp2(f,phzfreq,laga+1,tr)=abs(lags2/cnt)/(amp1/cnt);
%                      end
%                  end
%              end
%          end              
    end
  %  phaseampsCNTpfc(:,:,:,fila)=mean(phaseamp2,4);
  %  phaseampsCNTv1(:,:,:,fila)=mean(phaseamp1,4);
   


     trnums(1,fila)=sum(trials1(or(trials1==ori1,trials1==ori2))>0);
    trnums2(1,fila)=sum(trials1(and(trials1>0,trials1<9))>0);
   
    tfs1=tfs1b(:,:,and(trials2>0,trials2<100));
    tfs2=tfs2b(:,:,and(trials2>0,trials2<100));
    
    for calculatecoherenceTIMElines=1;
        
        for f=1:size(f_sG1,2);
            for tr=1:size(tfs1,3);
                cnt=0; lags=0;
                for t=2:99;   
                    l1=tfs1(f,t,tr)./abs(tfs1(f,t,tr));
                    l2=tfs2(f,t,tr)./abs(tfs2(f,t,tr));
                    lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                    cnt=cnt+1;
                end
                coherTIME_cntrl(f,tr,fila)=abs(lags/cnt)-(sqrt(-(1/cnt)*log(.5)));
            end
            %coherTIME_cntrl(f,:,fila)=coherTIME_cntrl(f,:,fila)-mean(coherTIME_cntrl(f,1:5,fila));
            coherTIME_cntrl_smooth(f,:,fila)=smoothdata(coherTIME_cntrl(f,:,fila),10)';
           % coherTIME_cntrl_smooth(f,:,fila)=coherTIME_cntrl_smooth(f,:,fila)-mean(coherTIME_cntrl_smooth(f,1,fila));
            
        end
    end
end

for clumping=1; 
    cnt=1; clear tmp
    for t=1:2:size(cohers_control_alltimes,2);
        tmp(:,cnt)=(cohers_control_alltimes(:,t)+cohers_control_alltimes(:,t+1))/2; cnt=1+cnt;
    end
    cohers_control_alltimes=tmp;

    cnt=1; clear tmp
    for t=1:2:size(ERP_V1_control,2);
        tmp(:,cnt)=(ERP_V1_control(:,t)+ERP_V1_control(:,t+1))/2; cnt=1+cnt;
    end
    ERP_V1_control=tmp;

    cnt=1; clear tmp
    for t=1:2:size(powers_control,2);
        tmp(:,cnt,:)=(powers_control(:,t,:)+powers_control(:,t+1,:))/2; cnt=1+cnt;
    end
    powers_control=tmp;

    cnt=1; clear tmp
    for t=1:2:size(cohers_control,3);
        tmp(:,:,cnt)=(cohers_control(:,:,t)+cohers_control(:,:,t+1))/2; cnt=1+cnt;
    end
    cohers_control=tmp;

    cnt=1; clear tmp
    for t=1:2:size(cohers_allC,3);
        tmp(:,:,cnt)=(cohers_allC(:,:,t)+cohers_allC(:,:,t+1))/2; cnt=1+cnt;
    end
    cohers_allC=tmp;

    cnt=1; clear tmp
    for t=1:2:size(itcsV1_control,3);
        tmp(:,:,cnt)=(itcsV1_control(:,:,t)+itcsV1_control(:,:,t+1))/2; cnt=1+cnt;
    end
    itcsV1_control=tmp;

    cnt=1; clear tmp
    for t=1:2:size(itcsPFC_control,3);
        tmp(:,:,cnt)=(itcsPFC_control(:,:,t)+itcsPFC_control(:,:,t+1))/2; cnt=1+cnt;
    end
    itcsPFC_control=tmp;

    cnt=1; clear tmp
    for t=1:2:size(erspsV1_control,3);
        tmp(:,:,cnt)=(erspsV1_control(:,:,t)+erspsV1_control(:,:,t+1))/2; cnt=1+cnt;
    end
    erspsV1_control=tmp;

    cnt=1; clear tmp
    for t=1:2:size(erspsPFC_control,3);
        tmp(:,:,cnt)=(erspsPFC_control(:,:,t)+erspsPFC_control(:,:,t+1))/2; cnt=1+cnt;
    end
    erspsPFC_control=tmp;

    cnt=1; clear tmp
    for t=1:2:size(cohers_nostim_alltimes,2);
        tmp(:,cnt)=(cohers_nostim_alltimes(:,t)+cohers_nostim_alltimes(:,t+1))/2; cnt=1+cnt;
    end
    cohers_nostim_alltimes=tmp;
end

powers{1,1}=powers_control;
coherences{1,1}=cohers_control;
coherences{4,1}=cohers_allC;
itcsV1{1,1}=itcsV1_control;
itcsPFC{1,1}=itcsPFC_control;
erspsV1{1,1}=erspsV1_control;
erspsPFC{1,1}=erspsPFC_control;

%% do mismatch
for makematrices=1;
    erspsV1_redundant=[];
    itcsV1_redundant=[];
    erspsPFC_redundant=[];
    itcsPFC_redundant=[];
    cohers_redundant=[];
    ERP_V1_redundant=[];
    ERP_PFC_redundant=[];
    ERP_V1_deviant=[];
    ERP_PFC_deviant=[];
    powers_odball=[];
    erspsV1_deviant=[];
    itcsV1_deviant=[];
    erspsPFC_deviant=[];
    itcsPFC_deviant=[];
    cohers_deviant=[];
    coherTIME_ob=zeros(100,350,size(files2,1));
    coherTIME_ob_smooth=zeros(100,350,size(files2,1));
    lagalls=[];
    time3=79;
   
    phaseampsOBpfc=zeros(100,14,8,size(files2,1));
    phaseampsOBv1=zeros(100,14,8,size(files2,1));
end

for fila=1:size(files2,1)
    load(files2{fila}); 
    trials_numrdnt=trials1; cnt=0; 
    for t=1:size(trials1,2);
        if trials1(t)==1;
            trials_numrdnt(1,t)=trials1(t)+cnt;
            cnt=1+cnt;            
        else 
            cnt=0;
            trials_numrdnt(1,t)=99;
        end
    end
    
    trials1=trials1(:,rej==0); erp1=erp1(rej==0,:);erp2=erp2(rej==0,:);tfs1=tfs1(:,:,rej==0);tfs2=tfs2(:,:,rej==0);ersp1=ersp1(:,:,rej==0); ersp2=ersp2(:,:,rej==0);
    trials_numrdnt2=trials_numrdnt(:,rej==0);
     trials2=trials1;tfs1b=tfs1; tfs2b=tfs2;%
     
     powtmp=zeros(1,100);
     for tr=1:size(erp1,1);
         han=hanning(1000)';
         a=fft(erp1(tr,175:1174).*han);
         powtmp=powtmp+abs(a(1,1:100));
     end
     powers_oddball(:,fila,1)=powtmp./tr;
     
     powtmp=zeros(1,100);
     for tr=1:size(erp2,1);
         han=hanning(1000)';
         a=fft(erp2(tr,175:1174).*han);
         powtmp=powtmp+abs(a(1,1:100));
     end
     powers_oddball(:,fila,2)=powtmp./tr;
     
    %for limiting trials by manual definitiion
    if limtrials>0; %deviant
        counta=0;
        for t=1:size(trials1,2);
            if (counta>(limtrials))
                trials1(1,t)=0;
            elseif trials1(1,t)==2
                counta=1+counta;
            end
        end
    end
    
     %for limiting trials by manual definitiion
    if limtrials>0; %rdnt
        counta=0;
        for t=1:size(trials_numrdnt2,2);
            if (counta>(limtrials))
                trials_numrdnt2(1,t)=0;
            elseif trials_numrdnt2(1,t)==rednumber
                counta=1+counta;
            end
        end
    end
        
    %ersp=mean(abs(tfs1(:,:,trials1>1)).^2,3);
    %for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end    
    erspsV1_deviant(:,:,fila)=mean(ersp1(:,:,trials1>1),3);
    
    ERP_V1_deviant(:,fila)=mean(erp1(trials1>1,:),1);

    %ersp=mean(abs(tfs2(:,:,trials1>1)).^2,3);
    %for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end    
    erspsPFC_deviant(:,:,fila)=mean(ersp2(:,:,trials1>1),3);

    ERP_PFC_deviant(:,fila)=mean(erp2(trials1>1,:),1);
    
    tr=sum(trials1>1);
    tfs=tfs1(:,:,trials1>1);
    itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
    itcsV1_deviant(:,:,fila)=itc;
    
    tfs=tfs2(:,:,trials1>1);
    itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
    itcsPFC_deviant(:,:,fila)=itc;
        
    %ersp=mean(abs(tfs1(:,:,trials1==1)).^2,3);
    %for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end   
    aa=ersp1(:,:,trials_numrdnt2==rednumber);
    if rednumber==98;
        aa=ersp1(:,:,and(trials_numrdnt2>0,trials_numrdnt2<7));
    end
    erspsV1_redundant(:,:,fila)=mean(aa(:,:,:),3);

    ERP_V1_redundant(:,fila)=mean(erp1(trials_numrdnt2==rednumber,:),1);
    
    %ersp=mean(abs(tfs2(:,:,trials1==1)).^2,3);
   % for f=1:size(ersp,1); ersp(f,:)=log(ersp(f,:)./mean(ersp(f,basetime1:basetime2))); end    
   aa=ersp2(:,:,trials_numrdnt2==rednumber); 
   if rednumber==98
       aa=ersp2(:,:,and(trials_numrdnt2>0,trials_numrdnt2<7));
   end
   erspsPFC_redundant(:,:,fila)=mean(aa(:,:,:),3);

    ERP_PFC_redundant(:,fila)=mean(erp2(trials_numrdnt2==rednumber,:),1);
        
   tr=sum(trials_numrdnt2==rednumber);
   tfs=tfs1(:,:,trials_numrdnt2==rednumber);
   if rednumber==98;
       tr=sum(and(trials_numrdnt2>0,trials_numrdnt2<7));
       tfs=tfs1(:,:,and(trials_numrdnt2>0,trials_numrdnt2<7));
   end
   itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
   itcsV1_redundant(:,:,fila)=itc;
    
   tfs=tfs2(:,:,trials_numrdnt2==rednumber);
   if rednumber==98;
       tfs=tfs2(:,:,and(trials_numrdnt2>0,trials_numrdnt2<7));
   end
   itc=tfs./abs(tfs); itc=abs(mean(itc,3))-sqrt(-(1/tr)*log(.5));
   itcsPFC_redundant(:,:,fila)=itc;
    
   for deviant_coher=1;
        tfs1a=tfs1(:,:,trials1>1);
        tfs2a=tfs2(:,:,trials1>1);
        
        clear coher
        for calculatecoherencebetweens=1;
            
            for f=1:size(f_sG1,2);
                for t=1:100;
                    lags=0; cnt=0;
                    for tr=1:size(tfs1a,3);
                        l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                        l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                    coher(f,t)=abs(lags/cnt)-(sqrt(-(1/cnt)*log(.5)));
                end
            end
        end
        cohers_deviant(:,:,fila)=coher;%-cohersham;
        cohershams(:,:,fila)=cohersham;
        %basecorrect
        for f=1:size(cohers_deviant,1);
            %         bb=mean(cohers(f,basetime1:basetime2,fila));
            %         for t=1:size(cohers,2);
            %             cohers(f,t,fila)=cohers(f,t,fila)-bb;
            %         end
        end
        
           %coherence across all timepoints
        clear coher
        for calculatecoherencebetweens=1
            for f=1:size(f_sG1,2)
                lags=0; cnt=0;
                for t=time1:time4
                    for tr=1:size(tfs1a,3)
                        l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                        l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                end
                coher(f)=abs(lags/cnt);%-(sqrt(-(1/cnt)*log(.5)));
            end
        end
        cohers_deviant_alltimes(:,fila)=coher;%-cohersham;
    end
    
   for redndt_coher=1;
        tfs1a=tfs1(:,:,and(trials_numrdnt2>0,trials_numrdnt2<10));
        tfs2a=tfs2(:,:,and(trials_numrdnt2>0,trials_numrdnt2<10));
       % tfs1a=tfs1(:,:,trials_numrdnt2==rednumber);
       % tfs2a=tfs2(:,:,trials_numrdnt2==rednumber);
        
        clear coher
        for calculatecoherencebetweens=1;
            
            for f=1:size(f_sG1,2);
                for t=1:100;
                    lags=0; cnt=0;
                    for tr=1:size(tfs1a,3);
                        l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                        l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                    coher(f,t)=abs(lags/cnt)-(sqrt(-(1/cnt)*log(.5)));
                end
            end
        end
        cohers_redundant(:,:,fila)=coher;%-cohersham;
        cohershams(:,:,fila)=cohersham;
        %basecorrect
        for f=1:size(cohers_redundant,1);
            %         bb=mean(cohers(f,basetime1:basetime2,fila));
            %         for t=1:size(cohers,2);
            %             cohers(f,t,fila)=cohers(f,t,fila)-bb;
            %         end
        end
        
        
        %coherence across all timepoints

         tfs1a=tfs1(:,:,trials_numrdnt2==rednumber);
        tfs2a=tfs2(:,:,trials_numrdnt2==rednumber);
         if rednumber==98;
            tfs1a=tfs1(:,:,and(trials_numrdnt2>0,trials_numrdnt2<9));
            tfs2a=tfs2(:,:,and(trials_numrdnt2>0,trials_numrdnt2<9));
        end

        clear coher
        for calculatecoherencebetweens=1
            for f=1:size(f_sG1,2)
                lags=0; cnt=0;
                for t=time1:time4
                    for tr=1:size(tfs1a,3)
                        l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                        l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                end
                coher(f)=abs(lags/cnt);%-(sqrt(-(1/cnt)*log(.5)));
            end
        end
        cohers_redundant_alltimes(:,fila)=coher;%-cohersham;


        for calculatephaselags=1;
            freqq=8;
            lags=0; cnt=1;
            if fila==6;
            for t=time2:time3;
                for tr=1:size(tfs1,3);
                    l1=tfs1(freqq,t,tr)./abs(tfs1(freqq,t,tr));
                    l2=tfs2(freqq,t,tr)./abs(tfs2(freqq,t,tr));
                    lags(cnt)=(1*exp(1i*((angle(l2)-angle(l1)))));
                    cnt=cnt+1;
                end
            end
            lagalls=vertcat(lagalls,lags');
            end
        end
    end
   trnums(1,fila)=sum(trials1==1);
   
   for OB_coher=1;
        tfs1a=tfs1;
        tfs2a=tfs2;
        
        clear coher
        for calculatecoherencebetweens=1;
            
            for f=1:size(f_sG1,2);
                for t=1:100;
                    lags=0; cnt=0;
                    for tr=1:size(tfs1a,3);
                        l1=tfs1a(f,t,tr)./abs(tfs1a(f,t,tr));
                        l2=tfs2a(f,t,tr)./abs(tfs2a(f,t,tr));
                        lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                        cnt=cnt+1;
                    end
                    coher(f,t)=abs(lags/cnt)-(sqrt(-(1/cnt)*log(.5)));
                end
            end
        end
        cohers_OB(:,:,fila)=coher-cohersham;
        cohershams(:,:,fila)=cohersham;
        %basecorrect
        for f=1:size(cohers_redundant,1);
            %         bb=mean(cohers(f,basetime1:basetime2,fila));
            %         for t=1:size(cohers,2);
            %             cohers(f,t,fila)=cohers(f,t,fila)-bb;
            %         end
        end
     
        
    end
    
   
   for calculatecoherenceTIMElines=1;
        
        for f=1:size(f_sG1,2);
            for tr=1:size(tfs1,3);
                cnt=0; lags=0;
                for t=2:99;   
                    l1=tfs1(f,t,tr)./abs(tfs1(f,t,tr));
                    l2=tfs2(f,t,tr)./abs(tfs2(f,t,tr));
                    lags=lags+(1*exp(1i*((angle(l1)-angle(l2)))));
                    cnt=cnt+1;
                end
                coherTIME_ob(f,tr,fila)=abs(lags/cnt)-(sqrt(-(1/cnt)*log(.5)));
            end
            %coherTIME_ob(f,:,fila)=coherTIME_ob(f,:,fila)-mean(coherTIME_ob(f,1:5,fila));
            coherTIME_ob_smooth(f,:,fila)=smoothdata(coherTIME_ob(f,:,fila),10)';
            %coherTIME_ob_smooth(f,:,fila)=coherTIME_ob_smooth(f,:,fila)-mean(coherTIME_ob_smooth(f,1,fila));
            
        end
    end
end

for clumping=1; 
    cnt=1; clear tmp
    for t=1:2:size(ERP_V1_redundant,2);
        tmp(:,cnt)=(ERP_V1_redundant(:,t)+ERP_V1_redundant(:,t+1))/2; cnt=1+cnt;
    end
    ERP_V1_redundant=tmp;

    cnt=1; clear tmp
    for t=1:2:size(cohers_redundant_alltimes,2);
        tmp(:,cnt)=(cohers_redundant_alltimes(:,t)+cohers_redundant_alltimes(:,t+1))/2; cnt=1+cnt;
    end
    cohers_redundant_alltimes=tmp;

    cnt=1; clear tmp
    for t=1:2:size(powers_oddball,2);
        tmp(:,cnt,:)=(powers_oddball(:,t,:)+powers_oddball(:,t+1,:))/2; cnt=1+cnt;
    end
    powers_oddball=tmp;

    cnt=1; clear tmp
    for t=1:2:size(cohers_redundant,3);
        tmp(:,:,cnt)=(cohers_redundant(:,:,t)+cohers_redundant(:,:,t+1))/2; cnt=1+cnt;
    end
    cohers_redundant=tmp;

    cnt=1; clear tmp
    for t=1:2:size(cohers_OB,3);
        tmp(:,:,cnt)=(cohers_OB(:,:,t)+cohers_OB(:,:,t+1))/2; cnt=1+cnt;
    end
    cohers_OB=tmp;

    cnt=1; clear tmp
    for t=1:2:size(itcsV1_redundant,3);
        tmp(:,:,cnt)=(itcsV1_redundant(:,:,t)+itcsV1_redundant(:,:,t+1))/2; cnt=1+cnt;
    end
    itcsV1_redundant=tmp;

    cnt=1; clear tmp
    for t=1:2:size(itcsPFC_redundant,3);
        tmp(:,:,cnt)=(itcsPFC_redundant(:,:,t)+itcsPFC_redundant(:,:,t+1))/2; cnt=1+cnt;
    end
    itcsPFC_redundant=tmp;

    cnt=1; clear tmp
    for t=1:2:size(erspsV1_redundant,3);
        tmp(:,:,cnt)=(erspsV1_redundant(:,:,t)+erspsV1_redundant(:,:,t+1))/2; cnt=1+cnt;
    end
    erspsV1_redundant=tmp;

    cnt=1; clear tmp
    for t=1:2:size(erspsPFC_redundant,3);
        tmp(:,:,cnt)=(erspsPFC_redundant(:,:,t)+erspsPFC_redundant(:,:,t+1))/2; cnt=1+cnt;
    end
    erspsPFC_redundant=tmp;
end

powers{2,1}=powers_oddball;
coherences{2,1}=cohers_redundant;
coherences{5,1}=cohers_OB;
itcsV1{2,1}=itcsV1_redundant;
itcsPFC{2,1}=itcsPFC_redundant;
erspsV1{2,1}=erspsV1_redundant;
erspsPFC{2,1}=erspsPFC_redundant;

for clumping=1; 
     cnt=1; clear tmp
    for t=1:2:size(ERP_V1_deviant,2);
        tmp(:,cnt)=(ERP_V1_deviant(:,t)+ERP_V1_deviant(:,t+1))/2; cnt=1+cnt;
    end
    ERP_V1_deviant=tmp;

    cnt=1; clear tmp
    for t=1:2:size(cohers_deviant_alltimes,2);
        tmp(:,cnt)=(cohers_deviant_alltimes(:,t)+cohers_deviant_alltimes(:,t+1))/2; cnt=1+cnt;
    end
    cohers_deviant_alltimes=tmp;

    cnt=1; clear tmp
    for t=1:2:size(cohers_deviant,3);
        tmp(:,:,cnt)=(cohers_deviant(:,:,t)+cohers_deviant(:,:,t+1))/2; cnt=1+cnt;
    end
    cohers_deviant=tmp;

    cnt=1; clear tmp
    for t=1:2:size(itcsV1_deviant,3);
        tmp(:,:,cnt)=(itcsV1_deviant(:,:,t)+itcsV1_deviant(:,:,t+1))/2; cnt=1+cnt;
    end
    itcsV1_deviant=tmp;

    cnt=1; clear tmp
    for t=1:2:size(itcsPFC_deviant,3);
        tmp(:,:,cnt)=(itcsPFC_deviant(:,:,t)+itcsPFC_deviant(:,:,t+1))/2; cnt=1+cnt;
    end
    itcsPFC_deviant=tmp;

    cnt=1; clear tmp
    for t=1:2:size(erspsV1_deviant,3);
        tmp(:,:,cnt)=(erspsV1_deviant(:,:,t)+erspsV1_deviant(:,:,t+1))/2; cnt=1+cnt;
    end
    erspsV1_deviant=tmp;

    cnt=1; clear tmp
    for t=1:2:size(erspsPFC_deviant,3);
        tmp(:,:,cnt)=(erspsPFC_deviant(:,:,t)+erspsPFC_deviant(:,:,t+1))/2; cnt=1+cnt;
    end
    erspsPFC_deviant=tmp;
end

powers{3,1}=powers_oddball;
coherences{3,1}=cohers_deviant;
itcsV1{3,1}=itcsV1_deviant;
itcsPFC{3,1}=itcsPFC_deviant;
erspsV1{3,1}=erspsV1_deviant;
erspsPFC{3,1}=erspsPFC_deviant;

%% stimtype coherence,calculated across all timepoints with the stimulus in it
timeA=time1;
timeB=time4; clear obs
timeAp=basetime1;
timeBp=basetime2;
figure;
for coherences_across_all_timepoints_per_condition=1;   
    
    
    base=mean(cohers_nostim_alltimes,2);
    %bbMN=mean(cohers_nostim_alltimes,2);
        
    tmp1=cohers_control_alltimes;%-cohers_nostim_alltimes;
    tmp2=cohers_redundant_alltimes;%-cohers_nostim_alltimes;
    tmp3=cohers_deviant_alltimes;%-cohers_nostim_alltimes;
   % bb1=cohers_nostim_alltimes;
   for s=1:size(tmp1,2);
        tmp1(:,s)=tmp1(:,s);%-base;
        tmp2(:,s)=tmp2(:,s);%-base;        
        tmp3(:,s)=tmp3(:,s);%-base;
   end
        

     ctMN=mean(tmp1,2);
    rdMN=mean(tmp2,2);
    dvMN=mean(tmp3,2);
        
    tmpX=(tmp1+tmp2+tmp3)/3; tmpx=0;
    ctST=std(tmp1-tmpX,0,2)/sqrt(size(tmp1,2));
    rdST=std(tmp2-tmpX,0,2)/sqrt(size(tmp2,2));
    dvST=std(tmp3-tmpX,0,2)/sqrt(size(tmp3,2));    
   % bbST=std(bb1-tmpX,0,2)/sqrt(size(bb1,2));
   
    figure;
    shadedErrorBar(f_sG1,dvMN,dvST,'lineProps','r'); xlim([1 55]);hold on
    shadedErrorBar(f_sG1,ctMN,ctST,'lineProps','k');  xlim([1 55]);hold on
    shadedErrorBar(f_sG1,rdMN,rdST,'lineProps','b');  xlim([1 55]);hold on
  %  shadedErrorBar(f_sG1,bbMN,bbST,'lineProps','g');  xlim([1 55]);hold on
    title('V1 phase-locking to PFC: all timepoints, trial types');
    xlabel('frequency (Hz)')
    yy(1,:)=ylim;
    yyy(1)=min(yy(:)); yyy(2)=max(yy(:));
    legend('deviant','control','redundant')

    freq1=9; freq2=19; 
    errorbar_groups([ctMN(freq1) rdMN(freq1) dvMN(freq1); ctMN(freq2) rdMN(freq2) dvMN(freq2)]',[ctST(freq1) rdST(freq1) dvST(freq1); ctST(freq2) rdST(freq2) dvST(freq2)]', 'bar_colors',[ .5 .5 .5; .5 .5 1; 1 0 0],'bar_names',{'theta';'beta'}); title('phase coherence'); hold on
    tmpa=vertcat(tmp1(freq1,:),tmp2(freq1,:),tmp3(freq1,:),tmp1(freq2,:),tmp2(freq2,:),tmp3(freq2,:));
    tmpb=tmpa-tmpa; sz=size(tmpb,2); tmpb(1,:)=zeros(1,sz)+1.1;tmpb(2,:)=zeros(1,sz)+2;tmpb(3,:)=zeros(1,sz)+2.9;tmpb(4,:)=zeros(1,sz)+4.1;tmpb(5,:)=zeros(1,sz)+5;tmpb(6,:)=zeros(1,sz)+5.9;
    scatter(tmpb,tmpa,'fill');
    line(tmpb(1:2,:),tmpa(1:2,:));line(tmpb(2:3,:),tmpa(2:3,:));line(tmpb(4:5,:),tmpa(4:5,:));line(tmpb(5:6,:),tmpa(5:6,:));
    anova_rm(tmpa(4:6,:)'); title('beta diffs');
    anova_rm(tmpa(1:3,:)'); title('theta diffs');
end

%% stimlocked coherences
timeA=time1;
timeB=time3; clear obs
timeAp=basetime1;
timeBp=basetime2;
figure;
for cond=1;
    tmp1=coherences{4,cond};%./cohers_nostim_alltimes;
    tmp2=coherences{5,cond};%./cohers_nostim_alltimes;
    tmp3=(tmp1+tmp2); 
    base=mean(cohers_nostim_alltimes,2);
    for sub=1:size(tmp1,3);
        for t=1:size(tmp1,2);
            tmp1(:,t,sub)=(tmp1(:,t,sub)-cohers_nostim_alltimes(:,sub));%-base);
            tmp2(:,t,sub)=(tmp2(:,t,sub)-cohers_nostim_alltimes(:,sub));%-base);
           tmp3(:,t,sub)=(tmp3(:,t,sub));%-(cohers_nostim_alltimes(:,sub)));%-base);
            % tmp3(:,t,sub)=cohers_nostim_alltimes(:,sub);%-(cohers_nostim_alltimes(:,sub)));%-base);


           % tmp1(:,t,sub)=(tmp1(:,t,sub)-base)./(base);%-cohers_nostim_alltimes(:,sub));%./((tmp1(:,t,sub)+cohers_nostim_alltimes(:,sub))/2);
           % tmp2(:,t,sub)=(tmp2(:,t,sub)-base)./(base);%-cohers_nostim_alltimes(:,sub));%./((tmp2(:,t,sub)+cohers_nostim_alltimes(:,sub))/2);
           % tmp3(:,t,sub)=(tmp3(:,t,sub)-base)./(base);%-cohers_nostim_alltimes(:,sub));%./((tmp2(:,t,sub)+cohers_nostim_alltimes(:,sub))/2);
        end
    end
    ctMN=mean(tmp1(:,:,:),3);
    rdMN=mean(tmp2(:,:,:),3);
    allMN=mean(tmp3(:,:,:),3);
    
    
    
    tmpX=(tmp1+tmp2)/2;
    ctST=std(tmp1-tmpX,0,3)/sqrt(size(tmp1,3));
    rdST=std(tmp2-tmpX,0,3)/sqrt(size(tmp2,3));
    allST=std(tmp3,0,3)/sqrt(size(tmp3,3));
    

    a=timeA;
    b=timeB;
     %shadedErrorBar(f_sG1,mean(rdMN(:,a:b),2)-mean(rdMN(:,c:d),2),mean((rdST(:,a:b)),2),'lineProps','b'); xlim([1 55]);hold on
     %shadedErrorBar(f_sG1,mean(ctMN(:,a:b),2)-mean(ctMN(:,c:d),2),mean((ctST(:,a:b)),2),'lineProps','k');  xlim([1 55]);
         %title('V1 phase-locking to PFC: all timempoints, basecorrected');
    %xlabel('frequency (Hz)')
    figure;
    shadedErrorBar(f_sG1,mean(allMN(:,a:b),2),mean((allST(:,a:b)),2),'lineProps','b'); xlim([1 55]);hold on
   % shadedErrorBar(f_sG1,mean(ctMN(:,a:b),2),mean((ctST(:,a:b)),2),'lineProps','k');  xlim([1 100]);
    title('V1 phase-locking to PFC: all timempoints');
    xlabel('frequency (Hz)')
     %legend('oddball run','control run')
   
end

for stimlocked_coherences_between_v1_and_pfc_sep=1;
    tmp1=coherences{1,1};%./cohers_nostim_alltimes;
    tmp2=coherences{2,1};%./cohers_nostim_alltimes;
    tmp3=coherences{3,1};%./cohers_nostim_alltimes;
    for t=1:size(tmp1,1);
        for f=1:size(tmp1,2);
            for sub=1:size(tmp1,3);
                b=mean(tmp1(f,timeAp:timeBp,sub),2); b=0;
                tmp1(f,:,sub)=tmp1(f,:,sub);
                b=mean(tmp2(f,timeAp:timeBp,sub),2); b=0;
                tmp2(f,:,sub)=tmp2(f,:,sub);
                b=mean(tmp3(f,timeAp:timeBp,sub),2); b=0;
                tmp3(f,:,sub)=tmp3(f,:,sub);
            end
        end
    end
   
figure;

    subplot(1,3,1); imagesc(t_sG1,f_sG1,mean(tmp1,3)); axis xy; title('control'); colormap jet;  axis([-300 700  1 55]);
    subplot(1,3,2);imagesc(t_sG1,f_sG1,mean(tmp2,3)); axis xy; title('redundant'); colormap jet;   axis([-300 700  1 55]);
    subplot(1,3,3);imagesc(t_sG1,f_sG1,mean(tmp3,3)); axis xy; title('oddball'); colormap jet;   axis([-300 700  1 55]);
end
  
for stimlocked_coherences_between_v1_and_pfc=1;
%     tmp1=coherences{4,1};
%     tmp2=coherences{5,1};
%     tmp1=mean(tmp1,3);
%     tmp2=mean(tmp2,3);
%     for t=1:size(tmp1,1);
%         for f=1:size(tmp1,2);
%             for sub=1:size(tmp1,3);
%                 b1=mean(tmp1(f,timeAp:timeBp,sub),2); %b=0;
%                 b2=mean(tmp2(f,timeAp:timeBp,sub),2);%b=0;
%                 tmp2(f,:,sub)=tmp2(f,:,sub);%-((b1+b2)/2);
%                 tmp1(f,:,sub)=tmp1(f,:,sub);%-((b1+b2)/2);
%             end
%         end
%     end
%    
% %figure;
% % contourf(t_sG1,f_sG1,mean((tmp1+tmp2)/2,3),10); axis xy;  colormap jet;  axis([-300 700  1 55]);
%  figure;
%  imagesc(t_sG1,f_sG1,mean((tmp1+tmp2)/2,3)); axis xy; title('control'); colormap jet; axis([-200 700  2 55]); caxis([.01 .2]);
end
 
for phase_locking_to_stimV1=1;
    tmp1=itcsV1{1,1};
    tmp2=itcsV1{2,1};
    tmp3=itcsV1{3,1};
    for t=1:size(tmp1,1);
        for f=1:size(tmp1,2);
            for sub=1:size(tmp1,3);
                b=mean(tmp1(f,timeAp:timeBp,sub),2);
                tmp1(f,:,sub)=tmp1(f,:,sub)-b;
                b=mean(tmp2(f,timeAp:timeBp,sub),2);
                tmp2(f,:,sub)=tmp2(f,:,sub)-b;
                b=mean(tmp3(f,timeAp:timeBp,sub),2);
                tmp3(f,:,sub)=tmp3(f,:,sub)-b;
            end
        end
    end
   
    
    grpSig=mean(tmp1,3)-mean(tmp1,3);
    for f=1:50;
        for t=time2:time3;
            a=squeeze(tmp1(f,t,:));b=squeeze(tmp2(f,t,:));c=squeeze(tmp3(f,t,:));            
            [p,table] = anova_rm(horzcat(a,b,c),'off');
            grpSig(f,t)=1-table{2,6};
        end
    end
    figure;

    subplot(1,3,1); imagesc(t_sG1,f_sG1,mean(tmp1,3)); axis xy; title('control'); colormap hot;  axis([-50 550 2 55]);cc=caxis; cc=[.05 .3]; ylabel('pre CNO');caxis(cc);
    subplot(1,3,2);imagesc(t_sG1,f_sG1,mean(tmp2,3)); axis xy; title('redundant'); colormap hot;   axis([-50 550 2 55]);caxis(cc);
    subplot(1,3,3);imagesc(t_sG1,f_sG1,mean(tmp3,3)); axis xy; title('deviant'); colormap hot;  axis([-50 550 2 55]);caxis(cc); 
    figure;
    map = [0 0 0;    1 1 1];
    subplot(1,2,1);imagesc(t_sG1,f_sG1,grpSig); axis xy; title('pvalues-group effect'); colormap(map); caxis([.945 1]); axis([-50 550 1 55]); 

          
end
  
for inducedphaselockin_to_stimV1_for_given_freq=1;
    b1=3; b2=9; %frequencies for analysis
   % b1=20; b2=40;
    tmp1=itcsV1{1,1};
    tmp2=itcsV1{2,1};
    tmp3=itcsV1{3,1};
    %tmp4=itcsV1{4,1};
    
    grpSig=zeros(1,100);
    cpre=zeros(size(tmp1,3),100); rpre=zeros(size(tmp1,3),100); dpre=zeros(size(tmp1,3),100);
    for f=1;
        for t=basetime1:92;
            a=squeeze(mean(tmp1(b1:b2,t,:),1));b=squeeze(mean(tmp2(b1:b2,t,:),1));c=squeeze(mean(tmp3(b1:b2,t,:),1));
            pm1=mean(mean(mean(tmp1(b1:b2,basetime1:basetime2,:))));
            pm2=mean(mean(mean(tmp2(b1:b2,basetime1:basetime2,:))));
            pm3=mean(mean(mean(tmp3(b1:b2,basetime1:basetime2,:))));
            [p,table] = anova_rm({horzcat(a,b,c)},'off');
            grpSig(f,t)=table{2,6};
            cpre(:,t)=a-pm1; rpre(:,t)=b-pm2; dpre(:,t)=c-pm3;  
        end
    end
    premean=(cpre+rpre+dpre)/2;
    figure;
    
    shadedErrorBar(t_sG1,mean(cpre,1),std((cpre-premean),0,1)/sqrt(size(cpre,1)),'lineProps','k'); hold on
    shadedErrorBar(t_sG1,mean(rpre,1),std((rpre-premean),0,1)/sqrt(size(rpre,1)),'lineProps','b'); hold on
    shadedErrorBar(t_sG1,mean(dpre,1),std((dpre-premean),0,1)/sqrt(size(dpre,1)),'lineProps','r');xlim([-100 600]);
        
    figure; plot(t_sG1,grpSig); title('pvalues');xlim([-100 600]);

    t1=time1; t2=time2; 
            a=squeeze(mean(mean(tmp1(b1:b2,t1:t2,:),1),2));b=squeeze(mean(mean(tmp2(b1:b2,t1:t2,:),1),2));c=squeeze(mean(mean(tmp3(b1:b2,t1:t2,:),1),2));
            pm1=mean(mean(mean(tmp1(b1:b2,basetime1:basetime2,:))));
            pm2=mean(mean(mean(tmp2(b1:b2,basetime1:basetime2,:))));
            pm3=mean(mean(mean(tmp3(b1:b2,basetime1:basetime2,:))));
            [p,table] = anova_rm({horzcat(a-pm1,b-pm2,c-pm3)},'on');
            grpSig(f,t)=table{2,6};
            cpre(:,t)=a-pm1; rpre(:,t)=b-pm2; dpre(:,t)=c-pm3;  
            a=a-pm1; b=b-pm2; c=c-pm3; x=(a+b+c)/3;
            errorbar_groups([mean(a) mean(b) mean(c)]',[std(a-x)/sqrt(size(a,1)) std(b-x)/sqrt(size(b,1)) std(c-x)/sqrt(size(c,1))]', 'bar_colors',[ .5 .5 .5; .5 .5 1; 1 0 0],'bar_names',{'control';'redundants';'deviants';}); title('ITC'); hold on
            tmpa=vertcat(a,b,c);
            tmpb=tmpa-tmpa; sz=size(a,1); tmpb=horzcat(zeros(1,sz)+1.1,zeros(1,sz)+2,zeros(1,sz)+2.9);
                scatter(tmpb,tmpa,'fill');
                disp(strcat('cohens D for dev vs cntrl',num2str(mean((a-pm1)-(c-pm3))./std(((a-pm1)+(c-pm3))/2))));
 end

for inducedAMPLTUDE_to_stimV1=1;
    tmp1=erspsV1{1,1};
    tmp2=erspsV1{2,1};
    tmp3=erspsV1{3,1};
     for t=1:size(tmp1,1);
        for f=1:size(tmp1,2);
            for sub=1:size(tmp1,3);
                b=mean(tmp1(f,timeAp:timeBp,sub),2);
                tmp1(f,:,sub)=tmp1(f,:,sub)-b;
                b=mean(tmp2(f,timeAp:timeBp,sub),2);
                tmp2(f,:,sub)=tmp2(f,:,sub)-b;
                b=mean(tmp3(f,timeAp:timeBp,sub),2);
                tmp3(f,:,sub)=tmp3(f,:,sub)-b;
            end
        end
    end
   
    
    grpSig=mean(tmp1,3)-mean(tmp1,3);
    for f=1:50;
        for t=time2:time3;
            a=squeeze(tmp1(f,t,:));b=squeeze(tmp2(f,t,:));c=squeeze(tmp3(f,t,:));            
            [p,table] = anova_rm(horzcat(a,b,c),'off');
            grpSig(f,t)=1-table{2,6};
        end
    end
    figure;

    subplot(1,3,1); imagesc(t_sG1,f_sG1,mean(tmp1,3)); axis xy; title('control'); colormap jet;  axis([-150 600 1 55]);cc=caxis; ylabel('pre CNO'); cc(1)=.02;caxis(cc);
    subplot(1,3,2);imagesc(t_sG1,f_sG1,mean(tmp2,3)); axis xy; title('redundant'); colormap jet;   axis([-150 600 1 55]);caxis(cc);
    subplot(1,3,3);imagesc(t_sG1,f_sG1,mean(tmp3,3)); axis xy; title('deviant'); colormap jet;  axis([-150 600 1 55]);caxis(cc); 
    figure;
    map = [0 0 0;    1 1 1];
    subplot(1,2,1);imagesc(t_sG1,f_sG1,grpSig); axis xy; title('pvalues-group effect'); colormap(map); caxis([.945 1]); axis([-150 600 1 55]); 
end
    
for inducedAMPLTUDE_to_stimV1_for_given_freq=1;
   % b1=5; b2=11; %frequencies for analysis
    b1=3; b2=9;
    tmp1=erspsV1{1,1};
    tmp2=erspsV1{2,1};
    tmp3=erspsV1{3,1};
    %tmp4=itcsV1{4,1};
    
     grpSig=zeros(1,100);
    cpre=zeros(size(tmp1,3),100); rpre=zeros(size(tmp1,3),100); dpre=zeros(size(tmp1,3),100);
    for f=1;
        for t=basetime1:92;
            a=squeeze(mean(tmp1(b1:b2,t,:),1));b=squeeze(mean(tmp2(b1:b2,t,:),1));c=squeeze(mean(tmp3(b1:b2,t,:),1));
            pm1=mean(mean(mean(tmp1(b1:b2,basetime1:basetime2,:))));
            pm2=mean(mean(mean(tmp2(b1:b2,basetime1:basetime2,:))));
            pm3=mean(mean(mean(tmp3(b1:b2,basetime1:basetime2,:))));
            [p,table] = anova_rm({horzcat(a-pm1,b-pm2,c-pm3)},'off');
            grpSig(f,t)=table{2,6};
            cpre(:,t)=a-pm1; rpre(:,t)=b-pm2; dpre(:,t)=c-pm3;  
        end
    end
    premean=(cpre+rpre+dpre)/2;
    figure;

         shadedErrorBar(t_sG1,mean(cpre,1),std((cpre-premean),0,1)/sqrt(size(cpre,1)),'lineProps','k'); hold on
       shadedErrorBar(t_sG1,mean(rpre,1),std((rpre-premean),0,1)/sqrt(size(rpre,1)),'lineProps','b'); hold on
       shadedErrorBar(t_sG1,mean(dpre,1),std((dpre-premean),0,1)/sqrt(size(dpre,1)),'lineProps','r');xlim([-100 600]);
        
        figure; plot(t_sG1,grpSig); title('pvalues');xlim([-100 600]);
          time15=47;  time25=65;
         t1=time2+5; t2=time3; 
            a=squeeze(mean(mean(tmp1(b1:b2,t1:t2,:),1),2));b=squeeze(mean(mean(tmp2(b1:b2,t1:t2,:),1),2));c=squeeze(mean(mean(tmp3(b1:b2,t1:t2,:),1),2));
            pm1=mean(mean(mean(tmp1(b1:b2,basetime1:basetime2,:))));
            pm2=mean(mean(mean(tmp2(b1:b2,basetime1:basetime2,:))));
            pm3=mean(mean(mean(tmp3(b1:b2,basetime1:basetime2,:))));
            [p,table] = anova_rm({horzcat(a-pm1,c-pm3)},'on');
            grpSig(f,t)=table{2,6};
            cpre(:,t)=a-pm1; rpre(:,t)=b-pm2; dpre(:,t)=c-pm3;  
        a=a-pm1; b=b-pm2; c=c-pm3; x=(a+b+c)/3;
            errorbar_groups([mean(a) mean(b) mean(c)]',[std(a-x)/sqrt(size(a,1)) std(b-x)/sqrt(size(b,1)) std(c-x)/sqrt(size(c,1))]', 'bar_colors',[ .5 .5 .5; .5 .5 1; 1 0 0],'bar_names',{'control';'redundants';'deviants';}); title('stp'); hold on
            tmpa=vertcat(a,b,c);
            tmpb=tmpa-tmpa; sz=size(a,1); tmpb=horzcat(zeros(1,sz)+1.1,zeros(1,sz)+2,zeros(1,sz)+2.9);
                scatter(tmpb,tmpa,'fill');
 end

 for erp_plot=1;

     for basesub=1;
         if basesub==1;
             b1=300; b2=500;
             for sub=1:size(ERP_V1_control,2);
                 bs=mean(ERP_V1_control(b1:b2,sub));
                 for t=1:size(ERP_V1_control,1);
                     ERP_V1_control(t,sub)=ERP_V1_control(t,sub)-bs;
                 end
                 bs=mean(ERP_V1_redundant(b1:b2,sub));
                 for t=1:size(ERP_V1_redundant,1);
                     ERP_V1_redundant(t,sub)=ERP_V1_redundant(t,sub)-bs;
                 end
                 bs=mean(ERP_V1_deviant(b1:b2,sub));
                 for t=1:size(ERP_V1_deviant,1);
                     ERP_V1_deviant(t,sub)=ERP_V1_deviant(t,sub)-bs;
                 end
                 bs=mean(ERP_PFC_control(b1:b2,sub));
                 for t=1:size(ERP_PFC_control,1);
                     ERP_PFC_control(t,sub)=ERP_PFC_control(t,sub)-bs;
                 end
                 bs=mean(ERP_PFC_redundant(b1:b2,sub));
                 for t=1:size(ERP_PFC_redundant,1);
                     ERP_PFC_redundant(t,sub)=ERP_PFC_redundant(t,sub)-bs;
                 end
                 bs=mean(ERP_PFC_deviant(b1:b2,sub));
                 for t=1:size(ERP_PFC_deviant,1);
                     ERP_PFC_deviant(t,sub)=ERP_PFC_deviant(t,sub)-bs;
                 end
             end
         end
     end
     figure; plot(-500:849,mean(ERP_V1_control(:,:),2),'k'); hold on;
     plot(-500:849,mean(ERP_V1_redundant(:,:),2),'b'); hold on;
     plot(-500:849,mean(ERP_V1_deviant(:,:),2),'r'); hold on;
     xlim([-200 850]);
     figure; plot(-500:849,mean(ERP_PFC_control(:,:),2),'k'); hold on;
     plot(-500:849,mean(ERP_PFC_redundant(:,:),2),'b'); hold on;
     plot(-500:849,mean(ERP_PFC_deviant(:,:),2),'r'); hold on;
     xlim([-200 850]);

 end

 for evoked_amplitudeV1=1;
     cnt=[]; rdnt=[]; dev=[];
     for sub=1:size(ERP_V1_control(:,:),2);
         [erspa,itc,powbase,t_sG1,f_sG1,boot1,boot2,tfdata]=newtimef(ERP_V1_control(:,sub),1350,[-476 875],1000,[.5 25],'nfreqs',100,'freqs',[2 101],'timesout',100,'plotitc','off','plotersp','off','baseline',[-150 0],'verbose','off');
         cnt(:,:,sub)=erspa;
         [erspa,itc,powbase,t_sG1,f_sG1,boot1,boot2,tfdata]=newtimef(ERP_V1_redundant(:,sub),1350,[-476 875],1000,[.5 25],'nfreqs',100,'freqs',[2 101],'timesout',100,'plotitc','off','plotersp','off','baseline',[-150 0],'verbose','off');
         rdnt(:,:,sub)=erspa;
         [erspa,itc,powbase,t_sG1,f_sG1,boot1,boot2,tfdata]=newtimef(ERP_V1_deviant(:,sub),1350,[-476 875],1000,[.5 25],'nfreqs',100,'freqs',[2 101],'timesout',100,'plotitc','off','plotersp','off','baseline',[-150 0],'verbose','off');
         dev(:,:,sub)=erspa;
     end
     tmp1=cnt;
    tmp2=rdnt;
    tmp3=dev;
     for t=1:size(tmp1,1);
        for f=1:size(tmp1,2);
            for sub=1:size(tmp1,3);
                b=mean(tmp1(f,timeAp:timeBp,sub),2);
                tmp1(f,:,sub)=tmp1(f,:,sub)-b;
                b=mean(tmp2(f,timeAp:timeBp,sub),2);
                tmp2(f,:,sub)=tmp2(f,:,sub)-b;
                b=mean(tmp3(f,timeAp:timeBp,sub),2);
                tmp3(f,:,sub)=tmp3(f,:,sub)-b;
            end
        end
    end
   
    
    grpSig=mean(tmp1,3)-mean(tmp1,3);
    for f=1:50;
        for t=time2:time3;
            a=squeeze(tmp1(f,t,:));b=squeeze(tmp2(f,t,:));c=squeeze(tmp3(f,t,:));            
            [p,table] = anova_rm(horzcat(a,b),'off');
            grpSig(f,t)=1-table{2,6};
        end
    end
    figure;

    subplot(1,3,1); imagesc(t_sG1,f_sG1,mean(tmp1,3)); axis xy; title('control'); colormap jet;  axis([-150 600 1 55]);cc=caxis; ylabel('pre CNO'); cc(1)=.02;caxis(cc);
    subplot(1,3,2);imagesc(t_sG1,f_sG1,mean(tmp2,3)); axis xy; title('redundant'); colormap jet;   axis([-150 600 1 55]);caxis(cc);
    subplot(1,3,3);imagesc(t_sG1,f_sG1,mean(tmp3,3)); axis xy; title('deviant'); colormap jet;  axis([-150 600 1 55]);caxis(cc); 
    figure;
    map = [0 0 0;    1 1 1];
    subplot(1,2,1);imagesc(t_sG1,f_sG1,grpSig); axis xy; title('pvalues-group effect'); colormap(map); caxis([.945 1]); axis([-150 600 1 55]); 
 end
  
for evokedAMPLTUDE_to_stimV1_for_given_freq=1;
    b1=7; b2=9; %frequencies for analysis
   % b1=20; b2=40;
    tmp1=cnt;
    tmp2=rdnt;
    tmp3=dev;
    %tmp4=itcsV1{4,1};
    
     grpSig=zeros(1,100);
    cpre=zeros(size(tmp1,3),100); rpre=zeros(size(tmp1,3),100); dpre=zeros(size(tmp1,3),100);
    for f=1;
        for t=basetime1:92;
            a=squeeze(mean(tmp1(b1:b2,t,:),1));b=squeeze(mean(tmp2(b1:b2,t,:),1));c=squeeze(mean(tmp3(b1:b2,t,:),1));
            pm1=mean(mean(mean(tmp1(b1:b2,basetime1:basetime2,:))));
            pm2=mean(mean(mean(tmp2(b1:b2,basetime1:basetime2,:))));
            pm3=mean(mean(mean(tmp3(b1:b2,basetime1:basetime2,:))));
            [p,table] = anova_rm({horzcat(a,b,c)},'off');
            grpSig(f,t)=table{2,6};
            cpre(:,t)=a-pm1; rpre(:,t)=b-pm2; dpre(:,t)=c-pm3;  
        end
    end
    premean=(cpre+rpre+dpre)/2;
    figure;

         shadedErrorBar(t_sG1,mean(cpre,1),std((cpre-premean),0,1)/sqrt(size(cpre,1)),'lineProps','k'); hold on
       shadedErrorBar(t_sG1,mean(rpre,1),std((rpre-premean),0,1)/sqrt(size(rpre,1)),'lineProps','b'); hold on
       shadedErrorBar(t_sG1,mean(dpre,1),std((dpre-premean),0,1)/sqrt(size(dpre,1)),'lineProps','r');xlim([-100 600]);
        
        figure; plot(t_sG1,grpSig); title('pvalues');xlim([-100 600]);
 end

